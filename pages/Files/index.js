import {
  CircularProgress,
  Divider,
  IconButton,
  Stack,
  Typography,
} from "@mui/material";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import AdverItem from "../../src/component/index/AdverItem";
import MyHeader from "../../src/component/public/MyHeader";
import { COLOR_PRIMARY } from "../../src/theme/theme";
import { GET_MY_FILES } from "../../src/utils/Api";
import { MdArrowBack } from "react-icons/md";
import { FONT_NAME_BOLD } from "../../src/utils/Constant";

const MyFiles = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [moreLoading, setMoreLoading] = useState(false);
  const router = useRouter();
  console.log("router", router);
  useEffect(() => {
    getData();
  }, []);
  useEffect(() => {
    if (page > 1) {
      getData(false);
    }
  }, [page]);
  return (
    <Stack>
      {router.query && router.query.fromAdd ? (
        <div style={styles.header1}>
          <div style={{ width: "0%" }}></div>
          <span style={styles.titleBold}>فایل های من</span>
          <div style={{ width: "0%" }}>
            <IconButton onClick={() => router.replace("/")}>
              <MdArrowBack color={"#111"} size={21} />
            </IconButton>
          </div>
        </div>
      ) : (
        <MyHeader back title="فایل های من" />
      )}
      {loading ? (
        <Stack className="loading">
          <CircularProgress color="primary" />
        </Stack>
      ) : (
        <Stack>
          {data.length > 0 ? (
            <Stack>
              {data.map((item) => {
                return (
                  <>
                    <AdverItem item={item} edit />
                    <Divider sx={{ backgroundColor: "#eee" }} />
                  </>
                );
              })}
            </Stack>
          ) : (
            <Stack className="loading">
              <Typography variant="h1" color={COLOR_PRIMARY}>
                فایل های شما خالی است
              </Typography>
            </Stack>
          )}
        </Stack>
      )}
    </Stack>
  );

  function getData() {
    setLoading(true);

    axios
      .get(GET_MY_FILES + `?page=${page}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      })
      .then((res) => {
        setLoading(false);
        if (res.data.isSuccess) {
          setData((prevState) => prevState.concat(res.data.data.files));
          setCount(res.data.data.totalItems);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  }
};
const styles = {
  title: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 18,
    color: "#111",
    textAlign: "center",
    marginRight: 8,
  },
  titleBold: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 18,
    color: "#111",
    textAlign: "center",
  },
  header1: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    width: "100%",
    backgroundColor: "white",
    padding: 15,
    position: "sticky",
    zIndex: 99,
    top: 0,
    boxShadow: "0px 1px 2px #ccc",
    right: 0,
  },
  header2: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    backgroundColor: "white",
    padding: 17,
    position: "sticky",
    zIndex: 99,
    top: 0,
    boxShadow: "0px 1px 2px #ccc",
    right: 0,
  },
};
export default MyFiles;
