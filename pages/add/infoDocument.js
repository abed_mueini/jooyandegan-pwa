/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import {
  Button,
  CircularProgress,
  Divider,
  Stack,
  Typography,
} from "@mui/material";
import MyHeader from "../../src/component/public/MyHeader";
import { HiOutlineChevronLeft } from "react-icons/hi";
import {
  saveNumberSpec,
  setBottomSheet,
} from "../../src/redux/actions/AddFileActions";
import { persianNumber } from "../../src/utils/Persian";
import axios from "axios";
import { GET_CATEGORIES_SPECIFICATIONS } from "../../src/utils/Api";
import { FONT_NAME, FONT_NAME_BOLD } from "../../src/utils/Constant";
import { useDispatch, useSelector } from "react-redux";
import ListSpicificationSheet from "../../src/component/bottomSheet/ListSpicificationSheet";
import NumberPickerModal from "../../src/modals/NumberPickerModal";
import Btn from "../../src/component/addFile/Button";
import router from "next/router";
import MySnackBar from "../../src/component/public/MySnackBar";

export default function InfoDocument() {
  const [modalOpen, setModalOpen] = useState({ open: false, data: null });
  const [openSheet, setOpenSheet] = useState(false);
  const [loading2, setLoading2] = useState(false);
  const [listData, setListData] = useState([]);
  const [numberData, setNumberData] = useState([]);
  const [titlePlaceholder, setTitlePlaceholder] = useState(null);
  const dispatch = useDispatch();
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const files = useSelector((state) => state.files);

  useEffect(() => {
    if (files.category !== null) {
      getFeatures();
    } else {
      setListData([]);
      setNumberData([]);
    }
  }, [files.category]);

  return (
    <Stack>
      <MyHeader back title="انتخاب دسته بندی" />
      <Stack m={2} className="rowAC">
        <img src="/images/information.png" style={styles.image} alt="" />
        <Typography mt={1} mr={1} fontSize={17} variant="h1">
          اطلاعات تکمیلی
        </Typography>
      </Stack>
      <Divider />
      {loading2 ? (
        <Stack className="rowACJC">
          <CircularProgress color="primary" size={20} sx={{ marginTop: 1 }} />
        </Stack>
      ) : (
        <Stack className={"colJCAC"}>
          {listData.map((item) => {
            return (
              <Button
                key={item.id}
                style={styles.itemList}
                onClick={() => {
                  dispatch(
                    setBottomSheet({
                      title: item.title,
                      id: item.id,
                      data: item.listSpecificationValues,
                    })
                  );
                  setOpenSheet(true);
                }}
              >
                <Typography variant="body1" style={styles.itemTitle}>{`${
                  item.isRequired ? "*" : ""
                } ${item.title}`}</Typography>
                <Stack
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    display: "flex",
                  }}
                >
                  <Typography variant="body1" style={styles.itemTitleResult}>
                    {getListTitle(item)}
                  </Typography>
                  <HiOutlineChevronLeft
                    style={{ marginTop: 3 }}
                    color="#444"
                    size={15}
                  />
                </Stack>
              </Button>
            );
          })}
          {numberData.map((item) => {
            return (
              <Button
                key={item.id}
                style={styles.itemList}
                onClick={() =>
                  setModalOpen({
                    open: true,
                    data: {
                      title: item.title,
                      id: item.id,
                      isPrice: item.isPrice,
                    },
                  })
                }
              >
                <Typography variant="body1" style={styles.itemTitle}>{`${
                  item.isRequired ? "*" : ""
                } ${item.title}`}</Typography>
                <Stack className="rowAC">
                  <Typography variant="body1" style={styles.itemTitleResult}>
                    {getNumberTitle(item, item.isPrice)}
                  </Typography>
                  <HiOutlineChevronLeft
                    style={{ marginTop: 3 }}
                    color="#444"
                    size={15}
                  />
                </Stack>
              </Button>
            );
          })}
        </Stack>
      )}
      <Stack style={{ height: 150 }} />
      <Btn text="ثبت و مرحله بعد" onClick={() => Navigate()} />
      <ListSpicificationSheet
        open={openSheet}
        onClose={() => setOpenSheet(false)}
      />
      {modalOpen && (
        <NumberPickerModal
          open={modalOpen.open}
          data={modalOpen.data}
          onClose={() => setModalOpen({ open: false, data: null })}
        />
      )}
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  function getFeatures() {
    console.log("start");
    setLoading2(true);
    setListData([]);
    setNumberData([]);
    axios
      .get(GET_CATEGORIES_SPECIFICATIONS + "/" + files.category.id)
      .then((res) => {
        setListData(res.data.data.listSpecifications);
        setNumberData(res.data.data.numberSpecifications);
        setTitlePlaceholder(res.data.data.placeholderFileFullTitle);
        setLoading2(false);
      })
      .catch((err) => {
        setLoading2(false);
        console.log("err", err);
      });
  }
  function getListTitle(item) {
    let result = "انتخاب  کنید";
    item.listSpecificationValues.map((lsv) => {
      files.listSpecs.map((ls) => {
        if (ls.value === lsv.id) {
          result = lsv.value;
        }
      });
    });
    return result;
  }
  function getNumberTitle(item, price) {
    let result = "انتخاب کنید";
    files.numberSpecs.map((ls) => {
      if (ls.id === item.id) {
        if (ls.value !== 0 && price) {
          result = persianNumber(ls.value, true);
        } else {
          result = ls.value;
        }
      }
    });
    return result;
  }
  function Navigate() {
    let temp = getRequiredNumberDataIds();
    if (temp.length > 0) {
      files.numberSpecs.map((it) => {
        const idx = temp.indexOf(it.id);
        if (idx > -1) {
          temp.splice(idx, 1);
        }
      });
    }
    let temp2 = getRequiredLisDataIds();
    if (temp2.length > 0) {
      files.listSpecs.map((it2) => {
        const idx2 = temp2.indexOf(it2.id);
        if (idx2 > -1) {
          temp2.splice(idx2, 1);
        }
      });
    }
    let pricesNumberData = getPricesNumberData();
    pricesNumberData.forEach((pnd) => {
      if (files.numberSpecs.length === 0) {
        dispatch(
          saveNumberSpec({
            id: pnd,
            value: 0,
          })
        );
      } else if (files.numberSpecs.filter((ns) => ns.id === pnd).length === 0) {
        dispatch(
          saveNumberSpec({
            id: pnd,
            value: 0,
          })
        );
      }
    });
    if (temp.length === 0 && temp2.length === 0) {
      router.push({
        pathname: "otherDocument",
        query: {
          placeholder: titlePlaceholder,
        },
      });
    } else {
      setSnackBar({
        open: true,
        message: "لطفا همه‌ی ویژگی‌ها ستاره دار را پر کنید",
      });
    }
  }

  function getPricesNumberData() {
    let value = [];
    numberData.forEach((item) => {
      if (item.isPrice) {
        value.push(item.id);
      }
    });
    return value;
  }
  function getRequiredNumberDataIds() {
    let dt = [];
    numberData.forEach((item) => {
      if (item.isRequired && !item.isPrice) {
        dt.push(item.id);
      }
    });
    return dt;
  }
  function getRequiredLisDataIds() {
    let dt = [];
    listData.forEach((item) => {
      if (item.isRequired) {
        dt.push(item.id);
      }
    });
    return dt;
  }
}
const styles = {
  image: {
    width: 25,
    height: 25,
    objectFit: "contain",
  },
  itemList: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 15,
    width: "97%",
    alignSelf: "center",
  },
  itemTitle: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 15,
    color: "#111",
  },
  itemTitleResult: {
    fontSize: 15,
    fontFamily: FONT_NAME,
    color: "#333",
  },
};
