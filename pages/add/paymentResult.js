import { Button, Stack, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";
import {
  changeLevel,
  clearHistory,
  saveCategory,
} from "../../src/redux/actions/AddFileActions";
import { persianNumber } from "../../src/utils/Persian";

export default function PaymentResult() {
  const router = useRouter();
  const walletCredit = useSelector((state) => state.splash.wallet);
  const dispatch = useDispatch();
  return (
    <Stack>
      <MyHeader title="نتیجه پرداخت" />
      <Stack mt={3} className="rowACJC">
        <Stack
          className="rowACJC"
          style={Object.assign(
            {
              border:
                router.query.status !== "true"
                  ? "1px solid red"
                  : "1px solid  green",
            },
            styles.resultPaymnet
          )}
        >
          <Typography variant="h1">
            {router.query.status === "true"
              ? " پرداخت شما موفق بود"
              : "پرداخت شما ناموفق بود"}
          </Typography>
        </Stack>
      </Stack>
      <Stack style={styles.resultContainer} className="rowACJC">
        <Stack style={styles.rowContainer} mt={4} className="rowACJSB">
          <Typography variant="h1">اعتبار کیف پول شما‌ :</Typography>
          <Typography variant="h1">
            {`${persianNumber(walletCredit, true)} تومان `}
          </Typography>
        </Stack>
      </Stack>
      <Stack style={styles.resultContainer} className="rowACJC">
        <Stack style={styles.rowContainer} mt={4} className="rowACJSB">
          <Typography variant="h1">وضعیت پرداخت :</Typography>
          <Typography
            color={router.query.status === "true" ? "green" : "red"}
            variant="h1"
          >
            {router.query.status === "true" ? "موفق" : "ناموفق"}
          </Typography>
        </Stack>
      </Stack>
      <Stack mt={12} className="rowACJC">
        <Button
          className="rowACJC"
          style={styles.submitBtn}
          variant="contained"
          onClick={() => {
            if (router.query.status === "true") {
              dispatch(
                saveCategory({
                  id: router.query.categoryId,
                  title: router.query.title,
                })
              );
              dispatch(changeLevel(null));
              dispatch(clearHistory());
              router.replace("/add");
            } else {
              router.replace("/add");
              dispatch(changeLevel(null));
              dispatch(clearHistory());
            }
          }}
        >
          <Typography variant="h1" color={"#ffff"}>
            تایید و بازگشت
          </Typography>
        </Button>
      </Stack>
    </Stack>
  );
}
const styles = {
  resultPaymnet: {
    width: "60%",
    padding: 10,
    borderRadius: 5,
  },
  resultContainer: {
    width: "100%",
  },
  rowContainer: {
    width: "90%",
  },
  submitBtn: {
    width: "60%",

    padding: 10,
    borderRadius: 5,
  },
};
