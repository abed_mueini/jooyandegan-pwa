import { Stack, TextField, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";
import {
  saveAdverTiserPhone,
  saveCity,
  saveDesc,
  saveTitle,
} from "../../src/redux/actions/AddFileActions";
import { isEmptyObject } from "../../src/utils/Tools";
import Select from "react-select";
import Btn from "../../src/component/addFile/Button";
import MySnackBar from "../../src/component/public/MySnackBar";
export default function Other() {
  const files = useSelector((state) => state.files);
  const cities = useSelector((st) => st.splash.data.cities);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const dispatch = useDispatch();
  const router = useRouter();
  return (
    <Stack>
      <MyHeader title="ثبت آگهی" back />
      <Stack m={2} className="colAC">
        <Typography variant="h1">محدوده آگهی</Typography>
        <Select
          className="Select"
          placeholder={"شهر"}
          value={
            cities.length > 0 &&
            cities.filter((ct) => ct.value === files.cityId)
          }
          onChange={(selectedOption) =>
            dispatch(saveCity(selectedOption.value))
          }
          options={cities}
        />
      </Stack>
      <Stack m={2} className="colAC">
        <Typography mt={-0.5} variant="h1">
          عنوان آگهی
        </Typography>
        <TextField
          variant="outlined"
          value={files.title}
          onChange={(e) => dispatch(saveTitle(e.target.value))}
          placeholder={
            !isEmptyObject(router.query.placeholder)
              ? `${router.query.placeholder}`
              : "عنوان فایل"
          }
          sx={{ marginTop: 1.5 }}
        />
      </Stack>
      <Stack m={2} className="colAC">
        <Typography mt={-0.5} variant="h1">
          تلفن همراه
        </Typography>
        <TextField
          variant="outlined"
          value={files.advertiserPhoneNumber}
          onChange={(e) => dispatch(saveAdverTiserPhone(e.target.value))}
          placeholder={"شماره همراه خود را وارد نمایید"}
          sx={{ marginTop: 1.5 }}
          type="number"
        />
      </Stack>
      <Stack m={2} className="colAC">
        <Typography mt={-0.5} variant="h1">
          توضیحات
        </Typography>
        <TextField
          variant="outlined"
          value={files.desc}
          onChange={(e) => dispatch(saveDesc(e.target.value))}
          placeholder={"در این قسمت توضیحات بیشتر برای آگهی بنویسید..."}
          sx={{ marginTop: 1.5 }}
          multiline
        />
      </Stack>
      <Stack style={{ height: 150 }} />

      <Btn
        text="ثبت و مرحله بعد"
        onClick={() => {
          if (!isEmptyObject(files.title)) {
            if (!isEmptyObject(files.advertiserPhoneNumber)) {
              if (
                !isEmptyObject(files.advertiserPhoneNumber) &&
                validatePhone(files.advertiserPhoneNumber)
              ) {
                router.push("descriptionDocument");
              } else {
                setSnackBar({
                  open: true,
                  message: "فرمت شماره تماس اشتباه است",
                });
              }
            } else {
              setSnackBar({
                open: true,
                message: "شماره تماس نباید خالی باشد!",
              });
            }
          } else {
            setSnackBar({ open: true, message: "عنوان آگهی نباید خالی باشد!" });
          }
        }}
      />
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  function validatePhone(phone) {
    const re = /^(^(09|9)[1][1-9]\d{7}$)|(^(09|9)[3][12456]\d{7}$)$/;
    return re.test(String(phone).toLowerCase());
  }
}
