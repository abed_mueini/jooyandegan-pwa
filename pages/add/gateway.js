import { Button, CircularProgress, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";
import { COLOR_PRIMARY } from "../../src/theme/theme";
import RadioButtonUncheckedIcon from "@mui/icons-material/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@mui/icons-material/RadioButtonChecked";
import CheckIcon from "@mui/icons-material/Check";
import { useRouter } from "next/router";
import { persianNumber } from "../../src/utils/Persian";
import MySnackBar from "../../src/component/public/MySnackBar";
import { PAYMENT_GATEWAY } from "../../src/utils/Api";
import Axios from "axios";
import { isEmptyObject } from "../../src/utils/Tools";
export default function Gateway() {
  const [payment, setPayment] = useState(null);
  const data = useSelector((st) => st.splash.data.gateways);
  const imageBaseUrl = useSelector((st) => st.splash.data.imageBaseUrl);
  const walletCredit = useSelector((state) => state.splash.wallet);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });

  const router = useRouter();
  return (
    <Stack>
      <MyHeader title="پرداخت هزینه آکهی" back />
      <Stack className="rowACJC">
        <Typography
          margin={3}
          textAlign={"center"}
          color={"#aaa"}
          variant="body1"
        >
          این دسته بندی دارای تعرفه پرداخت می باشد لذا لازم است ابتدا هزینه آگهی
          را پرداخت نمایید
        </Typography>
      </Stack>
      <Stack sx={styles.rowContainer} className="rowACJSB">
        <Typography variant="h1">اعتبار شما:</Typography>
        <Typography color={COLOR_PRIMARY}>{`${persianNumber(
          walletCredit,
          true
        )} تومان`}</Typography>
      </Stack>
      <Stack mt={2} sx={styles.rowContainer} className="rowACJSB">
        <Typography variant="h1">تعرفه قابل پرداخت این آگهی :</Typography>
        <Typography>{`${persianNumber(
          router.query.price,
          true
        )} تومان`}</Typography>
      </Stack>
      <Typography fontSize={17} mt={4} mr={2} variant="h1">
        درگاه مورد نظر را انتخاب کنید :
      </Typography>
      <Stack className="rowACJC" mt={3}>
        {data.map((item) => {
          return (
            <Button
              onClick={() => setPayment(item)}
              className="rowACJSB"
              style={styles.itemPayment}
              key={item.id}
              variant="text"
            >
              <Stack className="rowACJC">
                <img
                  src={imageBaseUrl + item.icon}
                  alt=""
                  style={styles.logoPayment}
                />
                <Typography>{item.title}</Typography>
              </Stack>
              {payment ? (
                item.id === payment.id ? (
                  <RadioButtonCheckedIcon fontSize="small" />
                ) : (
                  <RadioButtonUncheckedIcon fontSize="small" />
                )
              ) : (
                <RadioButtonUncheckedIcon fontSize="small" />
              )}
            </Button>
          );
        })}
      </Stack>
      <Stack className="rowACJC">
        <Button
          className="rowACJC"
          style={styles.submitBtn}
          variant="contained"
          onClick={() => paymentSubmit()}
        >
          {isLoading ? (
            <CircularProgress size={22} style={{ color: "white" }} />
          ) : (
            <Stack className="rowACJC">
              <Typography variant="h1" color={"#ffff"}>
                پرداخت
              </Typography>
              <CheckIcon style={styles.icon} fontSize="medium" />
            </Stack>
          )}
        </Button>
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  async function paymentSubmit() {
    setIsLoading(true);
    if (!isEmptyObject(payment)) {
      let token = "";
      try {
        const value = await localStorage.getItem("token");
        token = value;
      } catch (e) {
        console.log("err.token", e);
      }
      let url = `?categoryId=${router.query.id}&gatewayId=${payment.id}&requestReference=20`;
      await Axios.get(PAYMENT_GATEWAY + url, {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
        .then((res) => {
          setIsLoading(false);
          console.log("res=>", res.data);
          if (res.data.data.status) {
            setSnackBar({ open: true, message: res.data.data.message });
            router.push(`${res.data.data.bankGateway}`);
          } else {
            setSnackBar({ open: true, message: res.data.data.message });
            console.log("here!");
            router.push({
              pathname: "paymentresult",
              query: {
                status: "true",
                categoryId: router.query.id,
                title: router.query.title,
              },
            });
          }
        })
        .catch((err) => {
          setIsLoading(false);
          console.log("err=>", err.response);
          if (err.response.status === 401) {
            setSnackBar({
              open: true,
              message: "ابتدا وارد حساب کاربری خود شوید",
            });
            router.push("auth");
          }
        });
    } else {
      setSnackBar({ open: true, message: "درگاه مورد نظر را انتخاب کنید" });
      setIsLoading(false);
    }
  }
}
const styles = {
  rowContainer: {
    marginInline: 2,
  },
  itemPayment: {
    width: "95%",
    border: "1px solid #eee",
    padding: 10,
    boxShadow: "0 0 7px #ccc",
    borderRadius: 10,
  },
  logoPayment: {
    width: 80,
    height: 50,
    marginLeft: -10,
  },
  submitBtn: {
    width: "100%",
    position: "fixed",
    bottom: 0,
    padding: 15,
    borderRadius: 0,
  },
  icon: {
    color: "white",
    marginTop: -3,
  },
};
