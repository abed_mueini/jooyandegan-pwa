import { Divider, Stack, Typography } from "@mui/material";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Btn from "../../src/component/addFile/Button";
import MyHeader from "../../src/component/public/MyHeader";
import MySnackBar from "../../src/component/public/MySnackBar";
import { clearFileInfo } from "../../src/redux/actions/AddFileActions";
import { SUBMIT_FILE } from "../../src/utils/Api";
import { persianNumber } from "../../src/utils/Persian";
import { isEmptyObject } from "../../src/utils/Tools";

export default function Description() {
  const files = useSelector((state) => state.files);
  const [isLoading, setIsLoading] = useState(false);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const dispatch = useDispatch();
  const router = useRouter();
  return (
    <Stack>
      <MyHeader back title="ثبت آگهی" />
      <Stack mt={2} className="rowACJC">
        <Typography variant="h1" color="#aaa">
          {`ثبت آگهی در این دسته بندی ${
            isEmptyObject(files.priceAdver) ? "رایگان" : "دارای تعرفه"
          } می باشد`}
        </Typography>
      </Stack>
      <Divider style={{ marginTop: 18 }} />
      <Typography m={2} variant="h1">
        توضیحات بیشتر
      </Typography>
      <Typography style={styles.desc} variant="body1">
        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
        از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و
        سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
        متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه
        درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با
        نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان
        خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید
        داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان
        رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات
        پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد
      </Typography>
      <Stack mt={2} className="rowACJC">
        <Stack style={styles.box} className="rowACJC" width={"90%"}>
          <Typography variant="h1">
            {`تعرفه قابل پرداخت : ${
              !isEmptyObject(files.priceAdver)
                ? `${persianNumber(files.priceAdver, true)} تومان`
                : "رایگان"
            } `}
          </Typography>
        </Stack>
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
      <Stack style={{ height: 150 }} />

      <Btn text="ثبت آگهی" isLoading={isLoading} onClick={() => submitFile()} />
    </Stack>
  );
  function getImages(files) {
    let result = [];
    if (files.image1 !== null) {
      result.push(files.image1);
    }
    if (files.image2 !== null) {
      result.push(files.image2);
    }
    if (files.image3 !== null) {
      result.push(files.image3);
    }
    if (files.image4 !== null) {
      result.push(files.image4);
    }
    if (files.image5 !== null) {
      result.push(files.image5);
    }
    if (files.image6 !== null) {
      result.push(files.image6);
    }
    return result;
  }
  async function submitFile() {
    setIsLoading(true);
    if (!isEmptyObject(localStorage.getItem("token"))) {
      files.listSpecs = files.listSpecs.map((fl) => {
        return fl.value;
      });
      let formData = new FormData();
      formData.append("Title", files.title);
      formData.append("CategoryId", files.category.id);
      formData.append("AdvertiserPhoneNumber", files.advertiserPhoneNumber);
      formData.append("cityId", files.cityId);
      formData.append("GiftAmount", files.gift);
      if (files.desc !== "") {
        formData.append("Description", files.desc);
      }
      if (getImages(files).length > 0) {
        getImages(files).forEach((img) => {
          formData.append("Images", img);
        });
      }
      if (files.numberSpecs.length > 0) {
        files.numberSpecs.forEach((ns) => {
          formData.append("NumberSpecifications[]", JSON.stringify(ns));
        });
      }
      if (files.listSpecs.length > 0) {
        files.listSpecs.forEach((ls) => {
          formData.append("ListSpecifications[]", JSON.stringify(ls));
        });
      }
      let token = "";
      try {
        const value = await localStorage.getItem("token");
        token = value;
      } catch (e) {
        console.log("zze", e);
      }
      setTimeout(() => {
        axios
          .post(SUBMIT_FILE, formData, {
            headers: {
              Authorization: "Bearer " + token,
              "Content-Type": "multipart/form-data",
            },
          })
          .then((res) => {
            setIsLoading(false);
            if (res.data.isSuccess) {
              setSnackBar({ open: true, message: res.data.message });
              dispatch(clearFileInfo());
              router.replace({
                pathname: "/Files",
                query: {
                  fromAdd: true,
                },
              });
            }
          })
          .catch((err) => {
            setIsLoading(false);
            console.log(`THREAD: got errrrr ${err}`);
          });
      }, 2000);
    } else {
      setSnackBar({ open: true, message: "ابتدا باید وارد حساب کاربری شوید" });
      router.push("/auth");
    }
  }
}
const styles = {
  desc: {
    marginInline: 15,
    lineHeight: 2,
  },
  box: {
    border: "1px solid #aaa",
    padding: 10,
    borderRadius: 10,
  },
};
