import { Button, Divider, Stack, TextField, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import TabNavigation from "../../src/component/navigation/TabNavigation";
import MyHeader from "../../src/component/public/MyHeader";
import { COLOR_PRIMARY } from "../../src/theme/theme";
import { BsChevronLeft } from "react-icons/bs";
import router from "next/router";
import ImagePickerContainer from "../../src/component/addFile/ImagePickerContainer";
import Num2persian from "../../src/utils/NumToPersian";
import { saveCity, saveGiftFile } from "../../src/redux/actions/AddFileActions";

import { useDispatch, useSelector } from "react-redux";
import MySnackBar from "../../src/component/public/MySnackBar";
const AddFile = () => {
  const [amountGift, setAmountGift] = useState("");
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const dispatch = useDispatch();
  const files = useSelector((state) => state.files);
  const city = useSelector((st) => st.filter?.city);

  return (
    <Stack>
      <TabNavigation name="AddFile" />
      <MyHeader title="ثبت آگهی" />
      <Button
        style={styles.btnCategories}
        variant="outlined"
        className="rowACJSB"
        onClick={() => router.push("add/categories")}
      >
        <Typography color={COLOR_PRIMARY} variant="h1">
          {files.category !== null ? "تغییر دسته بندی" : "دسته بندی"}
        </Typography>
        <Stack className="rowAC">
          <Typography ml={1} variant="body1">
            {files.category !== null ? files.category.title : "انتخاب کنید"}
          </Typography>
          <BsChevronLeft color="#000" size={15} />
        </Stack>
      </Button>
      <Typography m={2} variant="h1">
        انتخاب عکس آگهی
      </Typography>
      <Typography color={"#aaa"} mr={1.5} mt={-1} variant="body1">
        عکس های خودرا برای این آگهی اضافه کنید
      </Typography>
      <ImagePickerContainer />
      <Divider style={styles.divider} />
      <Typography m={2} variant="h1">
        تعیین مبلغ مژدگانی
      </Typography>
      <Typography color={"#aaa"} mr={1.5} mt={-1} variant="body1">
        مبلغ به تومان وارد می شود برای مثال ۱۰۰۰۰ تومان
      </Typography>
      <Stack className="rowACJC">
        <TextField
          variant="outlined"
          value={amountGift}
          onChange={(e) => setAmountGift(e.target.value)}
          style={styles.input}
          placeholder="مبلغ مژدگانی را وارد کنید"
          type={"number"}
        />
      </Stack>
      {amountGift.length > 0 && (
        <Typography
          mt={1}
          color={"#aaa"}
          mr={1.5}
          variant="body1"
        >{`${Num2persian(amountGift)} تومان`}</Typography>
      )}
      {files.category !== null && (
        <Stack className="rowACJC">
          <Button
            onClick={() => Navigate()}
            style={styles.btnNext}
            variant="contained"
            color="primary"
          >
            <Typography variant="h1" color="white">
              ثبت و مرحله بعد
            </Typography>
          </Button>
        </Stack>
      )}
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
      <Stack style={{ height: 150 }} />
    </Stack>
  );

  function Navigate() {
    if (files.category === null) {
      setSnackBar({ open: true, message: "دسته بندی را انتخاب کنید" });
    } else if (amountGift.length === 0) {
      dispatch(saveGiftFile(0));
      router.push("add/infoDocument");
    } else {
      dispatch(saveGiftFile(parseInt(amountGift.split(",").join(""))));
      router.push("add/infoDocument");
    }
  }
};
const styles = {
  btnCategories: {
    padding: 16,
    border: "none",
    borderBottom: "1px solid #eee",
  },
  divider: {
    marginTop: 30,
  },
  input: {
    width: "90%",
    marginTop: 10,
  },
  btnNext: {
    position: "fixed",
    bottom: 70,
    padding: 15,
    width: "100%",
    zIndex: 9,
  },
};
export default AddFile;
