import { Button, Stack, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";
import { HiOutlineChevronLeft } from "react-icons/hi";
import { GET_CATEGORIES } from "../../src/utils/Api";
import {
  fetchCategoies,
  saveCategory,
} from "../../src/redux/actions/AddFileActions";
import axios from "axios";
import { FONT_NAME_BOLD } from "../../src/utils/Constant";
import { COLOR_PRIMARY } from "../../src/theme/theme";

export default function Categories() {
  const files = useSelector((state) => state.files);
  const categories = files.categories;
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const router = useRouter();
  useEffect(() => {
    if (categories.length === 0) {
      getCategories();
    }
  }, []);
  useEffect(() => {
    getRelatedCategories();
  }, [router.query]);
  return (
    <Stack>
      <MyHeader back title="انتخاب دسته بندی" />
      {data.map((item, index) => {
        return (
          !item.isAllCategory && (
            <Button
              onClick={() => {
                if (item.isNotFree) {
                  router.push({
                    pathname: "gateway",
                    query: {
                      id: item.id,
                      price: item.costPerFile,
                      title: item.title,
                    },
                  });
                } else {
                  router.push({
                    pathname: "categories",
                    query: { parentId: item.id },
                  });
                }
              }}
              style={styles.btn}
              key={index}
            >
              <Stack className="rowACJC">
                <Typography variant="h1">{item.title}</Typography>
                {item.isNotFree && (
                  <Typography style={styles.notFree}>
                    {"(غیر رایگان)"}
                  </Typography>
                )}
              </Stack>
              <HiOutlineChevronLeft />
            </Button>
          )
        );
      })}
    </Stack>
  );
  function getCategories() {
    axios
      .get(GET_CATEGORIES)
      .then((res) => {
        dispatch(fetchCategoies(res.data.data));
        getRelatedCategories2(res.data.data);
      })
      .catch((err) => {
        console.log("err", err);
      });
  }
  function getRelatedCategories() {
    console.log("here");
    const parentId =
      router.query.parentId !== undefined
        ? parseInt(router.query.parentId)
        : null;
    let temp = categories.filter(
      (cat) => cat.parentId === parentId && !cat.IsAllCategory
    );
    if (temp.length === 0 && parentId !== null) {
      let lastCategory = categories.filter((fil) => fil.id === parentId)[0];
      dispatch(
        saveCategory({ id: lastCategory.id, title: lastCategory.title })
      );
      router.replace("/add");
    } else {
      setData(temp);
    }
    console.log("end");
  }

  function getRelatedCategories2(data) {
    let temp = data.filter((cat) => cat.parentId === null);
    setData(temp);
  }
}

const styles = {
  btn: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 20,
  },
  notFree: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 14,
    color: COLOR_PRIMARY,
    marginRight: 10,
  },
};
