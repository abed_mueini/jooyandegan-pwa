import React, { useState } from "react";
import {
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { MdPhoneEnabled } from "react-icons/md";
import { Fa2En } from "../src/utils/Persian";
import MyButton from "../src/component/public/MyButton";
import MySnackBar from "../src/component/public/MySnackBar";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { storeProfile } from "../src/redux/actions/SplashActions";
import { AUTH_VERIFY } from "../src/utils/Api";
import { isEmptyObject } from "../src/utils/Tools";
import { MdArrowBack } from "react-icons/md";

import axios from "axios";
import { COLOR_PRIMARY, COLOR_SECOUNDRY } from "../src/theme/theme";
export default function Auth() {
  const [code, setCode] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const router = useRouter();
  const dispatch = useDispatch();
  return (
    <Stack className="col" style={styles.container}>
      <Stack className="rowACJC" style={styles.header}>
        <img src="/images/logo.png" alt="" style={styles.image} />
      </Stack>
      <Stack className="rowAC" style={styles.backContainer}>
        <IconButton>
          <MdArrowBack color={"#111"} size={21} />
        </IconButton>
      </Stack>
      <Stack style={styles.inputContainer} className="colJCAC">
        <Typography color="#ccc" variant="body1">
          {`یک کد برای شماره ${router.query.Phone} ارسال خواهد شد`}
        </Typography>
        <TextField
          value={code}
          onChange={(e) => setCode(Fa2En(e.target.value))}
          style={{ width: "90%", marginTop: 15, backgroundColor: "white" }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <MdPhoneEnabled
                  style={{ fontSize: 25, color: COLOR_SECOUNDRY }}
                />
              </InputAdornment>
            ),
          }}
          placeholder="کد ارسالی را وارد کنید"
        />
      </Stack>
      <Stack className="rowACJC">
        <MyButton
          onClick={(e) => requestCode()}
          isLoading={isLoading}
          style={{ padding: 10 }}
          title="تایید"
        />
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  function requestCode() {
    if (!isEmptyObject(code)) {
      setIsLoading(true);
      let data = {
        PhoneNumber: router.query.Phone,
        code: code,
      };
      axios
        .post(AUTH_VERIFY, data)
        .then((res) => {
          setIsLoading(false);
          if (res.data.isSuccess) {
            let token = res.data.data.token;
            delete res.data.data.token;
            dispatch(storeProfile(res.data.data));
            localStorage.setItem("token", token);
            localStorage.setItem("user", JSON.stringify(res.data.data));
            router.back();
            router.back();
          } else {
            setSnackBar({ open: true, message: res.data.data });
          }
        })
        .catch((err) => {
          setIsLoading(false);
          if (err.response.status === 401) {
            setSnackBar({ open: true, message: "کد وارد شده صحیح نیست" });
          } else {
            setSnackBar({ open: true, message: "مشکلی به وجود آمده است" });
          }
          console.log("error", err);
        });
    } else {
      setIsLoading(false);
      setSnackBar({ open: true, message: "کد نباید خالی باشد" });
    }
  }
}
const styles = {
  container: {
    display: "flex",
    width: "100%",
    height: "100vh",
    backgroundColor: "#fafafa",
  },
  image: {
    width: 100,
    height: 100,
    objectFit: "contain",
  },
  header: {
    height: 180,
    backgroundColor: "white",
  },
  inputContainer: {
    padding: 25,
  },
  backContainer: {
    width: "100%",
    backgroundColor: "white",
    borderBottom: "1px solid #eee",
    justifyContent: "flex-end",
  },
};
