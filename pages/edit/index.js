import React, { useEffect, useRef, useState } from "react";
import {
  Button,
  CircularProgress,
  Divider,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import MyHeader from "../../src/component/public/MyHeader";
import {
  EDIT,
  GET_CATEGORIES,
  GET_CATEGORIES_SPECIFICATIONS,
} from "../../src/utils/Api";
import axios from "axios";
import {
  clearFileInfo,
  fetchCategoies,
  saveAdverTiserPhoneNumberEf,
  saveCategory,
  saveCity,
  saveDesc,
  saveGiftFileEf,
  saveId,
  saveImages,
  saveListSpec,
  saveNumberSpecEf,
  saveTitle,
  setBottomSheetEf,
} from "../../src/redux/actions/EditFileActions";
import { isEmptyObject } from "../../src/utils/Tools";
import { HiOutlineChevronLeft } from "react-icons/hi";
import { BsChevronLeft } from "react-icons/bs";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import ImagePickerContainer from "../../src/component/editFIle/ImagePickerContainer";
import { COLOR_PRIMARY } from "../../src/theme/theme";
import Select from "react-select";
import { FONT_NAME, FONT_NAME_BOLD } from "../../src/utils/Constant";
import NumberPickerModal from "../../src/modals/NumberPickerModal";
import MySnackBar from "../../src/component/public/MySnackBar";
import ListSpecificationSheetEf from "../../src/component/bottomSheet/ListSpecificationSheetEf";
import Num2persian from "../../src/utils/NumToPersian";
import Btn from "../../src/component/addFile/Button";

function EditFile() {
  const router = useRouter();
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const files = useSelector((state) => state.editFile);
  const cities = useSelector((st) => st.splash.data.cities);
  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(false);
  const [listData, setListData] = useState([]);
  const [numberData, setNumberData] = useState([]);
  const [openSheet, setOpenSheet] = useState(false);
  const [modalOpen, setModalOpen] = useState({ open: false, data: null });
  const [loading3, setLoading3] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    fetchFileInfo();
  }, []);
  useEffect(() => {
    if (files.category !== null) {
      getFeatures();
    }
  }, [files.category]);
  return (
    <Stack>
      <MyHeader back title="ویرایش فایل" />
      {loading ? (
        <Stack className="rowACJC">
          <CircularProgress size={35} sx={{ marginTop: 2 }} />
        </Stack>
      ) : (
        <Stack className="col">
          <Button
            style={styles.btnCategories}
            variant="outlined"
            className="rowACJSB"
            onClick={() => router.push("edit/Categories")}
          >
            <Typography variant="h1">دسته بندی</Typography>

            <Stack className="rowAC">
              <Typography ml={1} color={COLOR_PRIMARY} variant="body1">
                {files.category !== null ? files.category.title : "انتخاب کنید"}
              </Typography>
              <BsChevronLeft color="#000" size={15} />
            </Stack>
          </Button>
          <Stack m={2} className="colAC">
            <Typography variant="h1">عنوان آگهی</Typography>
            <Select
              className="Select"
              placeholder={"شهر"}
              value={cities.filter((ct) => ct.value === files.cityId)}
              onChange={(selectedOption) =>
                dispatch(saveCity(selectedOption.value))
              }
              options={cities}
            />
          </Stack>
          <Typography margin={2} variant="h1">
            ویرایش تصاویر
          </Typography>
          <ImagePickerContainer />
          <Divider style={{ marginTop: 20 }} />
          <Typography margin={2} variant="h1">
            ویژگی ها
          </Typography>
          <Stack>
            {listData.map((item) => {
              return (
                <Button
                  key={item.id}
                  style={styles.itemList}
                  onClick={() => {
                    dispatch(
                      setBottomSheetEf({
                        title: item.title,
                        id: item.id,
                        data: item.listSpecificationValues,
                      })
                    );
                    setOpenSheet(true);
                  }}
                >
                  <Typography variant="body1" style={styles.itemTitle}>{`${
                    item.isRequired ? "*" : ""
                  } ${item.title}`}</Typography>
                  <Stack
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      display: "flex",
                    }}
                  >
                    <Typography variant="body1" style={styles.itemTitleResult}>
                      {getListTitle(item)}
                    </Typography>
                    <HiOutlineChevronLeft
                      style={{ marginTop: 3 }}
                      color="#444"
                      size={15}
                    />
                  </Stack>
                </Button>
              );
            })}
          </Stack>
          <Stack>
            {numberData.map((item) => {
              return (
                <Button
                  key={item.id}
                  style={styles.itemList}
                  onClick={() =>
                    setModalOpen({
                      open: true,
                      data: {
                        title: item.title,
                        id: item.id,
                        isPrice: item.isPrice,
                      },
                    })
                  }
                >
                  <Typography variant="body1" style={styles.itemTitle}>{`${
                    item.isRequired ? "*" : ""
                  } ${item.title}`}</Typography>
                  <Stack className="rowAC">
                    <Typography variant="body1" style={styles.itemTitleResult}>
                      {getNumberTitle(item, item.isPrice)}
                    </Typography>
                    <HiOutlineChevronLeft
                      style={{ marginTop: 3 }}
                      color="#444"
                      size={15}
                    />
                  </Stack>
                </Button>
              );
            })}
          </Stack>
          <Stack m={2} className="colAC">
            <Typography mb={2} variant="h1">
              عنوان فایل
            </Typography>
            <TextField
              variant="outlined"
              value={files.title}
              onChange={(e) => dispatch(saveTitle(e.target.value))}
              placeholder={
                !isEmptyObject(router.query.placeholder)
                  ? `${router.query.placeholder}`
                  : "عنوان فایل"
              }
            />
          </Stack>
          <Stack m={2} className="colAC">
            <Typography mb={2} variant="h1">
              مقدار مژدگانی
            </Typography>
            <TextField
              variant="outlined"
              value={files.gift}
              onChange={(e) => dispatch(saveGiftFileEf(e.target.value))}
              placeholder="مبلغ مژدگانی را وارد کنید"
              type={"number"}
            />
          </Stack>
          {files.gift.length > 0 && (
            <Typography color={"#aaa"} mr={2} variant="body1">{`${Num2persian(
              files.gift
            )} تومان`}</Typography>
          )}
          <Stack m={2} className="colAC">
            <Typography mt={-0.5} variant="h1">
              تلفن همراه
            </Typography>
            <TextField
              variant="outlined"
              value={files.adverTiserPhoneNumber}
              onChange={(e) =>
                dispatch(saveAdverTiserPhoneNumberEf(e.target.value))
              }
              placeholder={"شماره همراه خود را وارد نمایید"}
              sx={{ marginTop: 1.5 }}
              type="number"
            />
          </Stack>
          <Stack m={2} className="colAC">
            <Typography mt={-0.5} variant="h1">
              توضیحات
            </Typography>
            <TextField
              variant="outlined"
              value={files.desc !== "null" ? files.desc : ""}
              onChange={(e) => dispatch(saveDesc(e.target.value))}
              placeholder={"در این قسمت توضیحات بیشتر برای آگهی بنویسید..."}
              sx={{ marginTop: 1.5 }}
              multiline
            />
          </Stack>
        </Stack>
      )}
      <Stack style={{ height: 100 }} />
      <Btn
        text="تایید و ثبت"
        onClick={async () => {
          if (files.cityId !== null) {
            let temp = getRequiredNumberDataIds();
            if (temp.length > 0) {
              files.numberSpecs.map((it) => {
                const idx = temp.indexOf(it.id);
                if (idx > -1) {
                  temp.splice(idx, 1);
                }
              });
            }
            let temp2 = getRequiredLisDataIds();
            if (temp2.length > 0) {
              files.listSpecs.map((it2) => {
                const idx2 = temp2.indexOf(it2.id);
                if (idx2 > -1) {
                  temp2.splice(idx2, 1);
                }
              });
            }
            if (temp.length === 0 && temp2.length === 0) {
              if (!isEmptyObject(files.title)) {
                files.listSpecs = files.listSpecs.map((fl) => {
                  return fl.value;
                });
                if (
                  !isEmptyObject(files.adverTiserPhoneNumber) &&
                  validatePhone(files.adverTiserPhoneNumber)
                ) {
                  submit();
                } else {
                  setSnackBar({
                    open: true,
                    message: "فرمت شماره تماس اشتباه است",
                  });
                }
              } else {
                setSnackBar({
                  open: true,
                  message: "عنوان نمی تواند خالی بماند",
                });
              }
            } else {
              setSnackBar({
                open: true,
                message: "لطفا همه‌ی ویژگی‌ها ستاره دار راپر کنید",
              });
            }
          } else {
            setSnackBar({
              open: true,
              message: "لطفا محدوده فایل را انتخاب کنید",
            });
          }
        }}
        isLoading={loading3}
      />
      <ListSpecificationSheetEf
        open={openSheet}
        onClose={() => setOpenSheet(false)}
      />
      {modalOpen && (
        <NumberPickerModal
          open={modalOpen.open}
          data={modalOpen.data}
          onClose={() => setModalOpen({ open: false, data: null })}
          edit
        />
      )}
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  async function fetchFileInfo() {
    setLoading(true);
    dispatch(clearFileInfo());
    let token = "";
    try {
      const value = await localStorage.getItem("token");
      token = value;
    } catch (e) {
      console.log("zze", e);
    }
    axios
      .get(EDIT + `/${router.query.id}`, {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
      .then((res) => {
        console.log("res.data", res.data);
        dispatch(saveId(res.data.data.id));
        dispatch(saveCity(res.data.data.cityId));
        dispatch(
          saveCategory({
            id: res.data.data.categoryId,
            title: res.data.data.categoryName,
          })
        );
        dispatch(saveTitle(res.data.data.title));
        dispatch(
          saveAdverTiserPhoneNumberEf(res.data.data.advertiserPhoneNumber)
        );
        dispatch(saveGiftFileEf(res.data.data.giftAmount));
        dispatch(saveDesc(res.data.data.description));
        let images = ["", "", "", "", "", ""];
        images.forEach((item, ind) => {
          if (!isEmptyObject(res.data.data.images[ind])) {
            images[ind] = res.data.data.images[ind];
          }
        });
        dispatch(saveImages(images));
        res.data.data.selectedNumberSpecifications.map((ns) => {
          dispatch(saveNumberSpecEf({ id: ns.id, value: ns.value }));
        });
        res.data.data.selectedListSpecifications.map((ls, index) => {
          dispatch(saveListSpec({ value: ls.valueId, id: ls.id }));
        });
        getCategories();
      })
      .catch((err) => {
        setLoading(false);
        console.log("err", err);
      });
  }
  async function submit() {
    setLoading3(true);
    let token = "";
    try {
      const value = await localStorage.getItem("token");
      token = value;
    } catch (e) {
      console.log("zze", e);
    }
    let formData = new FormData();
    formData.append("Title", files.title);
    formData.append("Id", files.id);
    formData.append("CategoryId", files.category.id);
    formData.append("cityId", files.cityId);
    formData.append("GiftAmount", files.gift);
    formData.append("AdvertiserPhoneNumber", files.adverTiserPhoneNumber);
    formData.append("Description", files.desc);

    if (files.addImages.length > 0) {
      files.addImages.forEach((img) => {
        formData.append("AddImages", img);
      });
    }
    if (files.editFileImages.length > 0) {
      files.editFileImages.forEach((img) => {
        formData.append("EditFileImage", img);
      });
    }
    if (files.editFileImagesId.length > 0) {
      files.editFileImagesId.forEach((pt) => {
        formData.append("EditFileImageIds[]", pt);
      });
    }
    if (files.deleteImages.length > 0) {
      files.deleteImages.forEach((pt) => {
        formData.append("DeleteFileImageIds[]", pt);
      });
    }
    if (files.numberSpecs.length > 0) {
      files.numberSpecs.forEach((ns) => {
        formData.append("NumberSpecifications[]", JSON.stringify(ns));
      });
    }
    if (files.listSpecs.length > 0) {
      files.listSpecs.forEach((ls) => {
        formData.append("ListSpecifications[]", ls);
      });
    }
    axios
      .post(EDIT, formData, {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        setLoading3(false);
        console.log("res", res.data);
        setSnackBar({
          open: true,
          message: res.data.message + " و پس از تایید منتشر خواهد شد.",
        });
        dispatch(clearFileInfo());
        router.back();
      })
      .catch((err) => {
        setLoading3(false);
        console.log("err", err);
      });
  }
  function validatePhone(phone) {
    const re = /^(^(09|9)[1][1-9]\d{7}$)|(^(09|9)[3][12456]\d{7}$)$/;
    return re.test(String(phone).toLowerCase());
  }

  function getRequiredNumberDataIds() {
    let dt = [];
    numberData.forEach((item) => {
      if (item.isRequired) {
        dt.push(item.id);
      }
    });
    return dt;
  }
  function getRequiredLisDataIds() {
    let dt = [];
    listData.forEach((item) => {
      if (item.isRequired) {
        dt.push(item.id);
      }
    });
    return dt;
  }

  function getCategories() {
    axios
      .get(GET_CATEGORIES)
      .then((res) => {
        setLoading(false);
        dispatch(fetchCategoies(res.data.data));
      })
      .catch((err) => {
        setLoading(false);
        console.log("err", err.response);
      });
  }
  async function getFeatures() {
    setLoading2(true);
    let token = "";
    try {
      const value = await localStorage.getItem("token");
      token = value;
    } catch (e) {
      console.log("zze", e);
    }
    setListData([]);
    setNumberData([]);
    axios
      .get(GET_CATEGORIES_SPECIFICATIONS + "/" + files.category.id, {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
      .then((res) => {
        setLoading2(false);
        setListData(res.data.data.listSpecifications);
        setNumberData(res.data.data.numberSpecifications);
      })
      .catch((err) => {
        setLoading2(false);
        console.log("err", err.response);
      });
  }
  function getListTitle(item) {
    let result = "انتخاب  کنید";
    item.listSpecificationValues.map((lsv) => {
      files.listSpecs.map((ls) => {
        if (ls.value === lsv.id) {
          result = lsv.value;
        }
      });
    });
    return result;
  }
  function getNumberTitle(item) {
    let result = "انتخاب  کنید";
    files.numberSpecs.map((ls) => {
      if (ls.id === item.id) {
        result = ls.value;
      }
    });
    return result;
  }
}
export default EditFile;
const styles = {
  btnCategories: {
    padding: 16,
    border: "none",
  },
  itemList: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 15,
    width: "97%",
    alignSelf: "center",
  },
  itemTitle: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 15,
    color: "#111",
  },
  itemTitleResult: {
    fontSize: 15,
    fontFamily: FONT_NAME,
    color: "#333",
  },
};
