/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { HiOutlineChevronLeft } from "react-icons/hi";
import MyHeader from "../../src/component/public/MyHeader";
import {
  Button,
  CircularProgress,
  Divider,
  Stack,
  Typography,
} from "@mui/material";
import MySnackBar from "../../src/component/public/MySnackBar";
import {
  changeLevel,
  clearHistory,
  clearListSpecEf,
  clearNumberSpecEf,
  pushNavigationHistory,
  saveCategory,
} from "../../src/redux/actions/EditFileActions";

function Categories(props) {
  const files = useSelector((state) => state.editFile);
  const categories = files.categories;
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });

  useEffect(() => {
    getRelatedCategories();
  }, [files]);

  return loading ? (
    <div className={"loadingContainer"}>
      <CircularProgress color="primary" />
    </div>
  ) : (
    <Stack>
      <MyHeader back title="انتخاب دسته بندی" />
      <Divider />
      {data.map((item, index) => {
        return (
          <Button
            onClick={() => {
              dispatch(pushNavigationHistory(item.id));
              dispatch(changeLevel(item.id));
            }}
            style={styles.btn}
            key={index}
          >
            <Typography variant="h1" style={styles.itemTitle}>
              {item.title}
            </Typography>
            <HiOutlineChevronLeft />
          </Button>
        );
      })}
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  function getRelatedCategories() {
    let temp = categories.filter((cat) => cat.parentId === files.level);
    if (temp.length === 0 && files.history.length !== 0) {
      let lastCategory = categories.filter((fil) => fil.id === files.level)[0];
      if (files.category.id === lastCategory.id) {
        setSnackBar({
          open: true,
          message: "دسته بندی فعلی شما نمی تواند انتخاب شود",
        });
      } else {
        dispatch(
          saveCategory({ id: lastCategory.id, title: lastCategory.title })
        );
        dispatch(changeLevel(null));
        dispatch(clearNumberSpecEf());
        dispatch(clearListSpecEf());
        dispatch(clearHistory());
        router.back();
      }
    } else {
      setData(temp);
    }
  }
}
export default Categories;
const styles = {
  body: {
    display: "flex",
    flex: 1,
    height: "100vh",
    overflow: "hidden",
    justifyContent: "center",
  },
  container: {
    backgroundColor: "white",
    border: "1px #aaa solid",
    borderTop: "5px #286fbb solid",
    overflow: "auto",
    marginTop: 70,
  },
  title: {
    fontSize: 16,
    color: "black",
    padding: 10,
  },
  itemTitle: {
    fontSize: 14,
    padding: 10,
    color: "#000",
  },

  btn: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
};
