/* eslint-disable @next/next/no-img-element */
import {
  Button,
  CircularProgress,
  Divider,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { color } from "@mui/system";
import axios from "axios";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MyButton from "../../src/component/public/MyButton";
import MyHeader from "../../src/component/public/MyHeader";
import MySnackBar from "../../src/component/public/MySnackBar";
import VerifyEditProfileModal from "../../src/modals/VerifyEditProfile";
import { storeProfile } from "../../src/redux/actions/SplashActions";
import { EDIT_PROFILE } from "../../src/utils/Api";
import { isEmptyObject } from "../../src/utils/Tools";

export default function EditFile() {
  const userinfo =
    typeof window !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : null;
  const [name, setName] = useState("");
  const dispatch = useDispatch();
  const [isloading, setIsLoading] = useState();
  const [modalShow, setModalShow] = useState(false);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  return (
    <Stack>
      <MyHeader back title="ویرایش فایل" />
      <Stack padding={2} className={"colJCAC"}>
        <img src="/images/user.png" style={styles.userIcon} alt="" />
        {userinfo !== null && userinfo.fullName !== null && (
          <Typography marginTop={1} varinat="body1">
            {userinfo.fullName}
          </Typography>
        )}
        <Stack className="rowACJC" style={styles.userPhone}>
          <Typography variant="h1">
            {userinfo !== null && userinfo.phoneNumber}
          </Typography>
        </Stack>
        <Typography textAlign="center" color="#aaa" marginTop={2}>
          کاربر گرامی ویرایش شماره همراه ممکن نیست ، برای ویرایش از حساب کاربری
          خارج شوید ، سپس وارد شوید
        </Typography>
      </Stack>
      <Divider />
      <Typography margin={1.5} variant="h1">
        نام و نام خانوادگی :
      </Typography>
      <Stack m={1} className="rowACJC">
        <TextField
          value={name}
          onChange={(e) => setName(e.target.value)}
          variant="outlined"
          placeholder="نام و نام خانوادگی"
          sx={styles.input}
        />
      </Stack>
      <Stack
        mb={2}
        width={"100%"}
        className="rowACJC"
        position="fixed"
        bottom={0}
      >
        <Button
          onClick={() => submit()}
          sx={styles.btn}
          variant="contained"
          color="primary"
        >
          {isloading ? (
            <CircularProgress size={22} style={{ color: "white" }} />
          ) : (
            <Typography color={"white"} variant="h1">
              ثبت
            </Typography>
          )}
        </Button>
      </Stack>
      {modalShow && (
        <VerifyEditProfileModal
          open={modalShow}
          onClose={() => setModalShow(false)}
        />
      )}
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  async function submit() {
    setIsLoading(true);
    if (!isEmptyObject(name)) {
      let token = "";
      try {
        const value = await localStorage.getItem("token");
        token = value;
      } catch (e) {
        console.log("zze", e);
      }
      let data = {};
      data.FullName = name;
      axios
        .post(
          EDIT_PROFILE,
          { FullName: name },
          {
            headers: {
              Authorization: "Bearer " + token,
            },
          }
        )
        .then((res) => {
          setIsLoading(false);
          console.log("res", res.data.data);
          if (res.data.isSuccess) {
            setModalShow(true);
            setName("");
            dispatch(storeProfile(res.data.data));
            localStorage.setItem("user", JSON.stringify(res.data.data));
          } else {
            setSnackBar({ open: true, message: res.data.message });
          }
        })
        .catch((err) => {
          setIsLoading(false);
          console.log("err", err);
        });
    } else {
      setIsLoading(false);
      setSnackBar({
        open: true,
        message: "نام و نام خانوادگی نباید خالی باشد",
      });
    }
  }
}
const styles = {
  userIcon: {
    width: 70,
    height: 70,
    objectFit: "contain",
  },
  userPhone: {
    width: "80%",
    padding: 9,
    marginTop: 5,
    backgroundColor: "#ccc",
    borderRadius: 10,
  },
  input: {
    width: "90%",
    color: "#000",
  },
  btn: {
    width: "90%",
    padding: 1.5,
  },
};
