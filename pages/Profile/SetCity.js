import {
  Button,
  Container,
  Divider,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { BsChevronLeft, BsArrowRightShort, BsSearch } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { setCity } from "../../src/redux/actions/FilterActions";
import { isEmptyObject } from "../../src/utils/Tools";

function SetCity() {
  const [search, setSearch] = useState("");
  const city = useSelector((st) => st.splash.data.cities);
  const dispatch = useDispatch();
  const router = useRouter();

  return (
    <Stack>
      <Stack style={styles.header} className="rowACJSB" width={"100%"}>
        <IconButton onClick={(e) => router.back()}>
          <BsArrowRightShort color={"#000"} size={35} />
        </IconButton>
        <TextField
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          style={styles.inputSearch}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <BsSearch style={{ fontSize: 25 }} />
              </InputAdornment>
            ),
          }}
          placeholder="شهر مورد نظر خودرا جستجو کنید..."
        />
      </Stack>
      <Divider sx={{ marginTop: 1 }} />
      <Stack className="rowACJC">
        <Typography marginTop={1.5} textAlign={"center"} color="#aaa">
          شهر مورد نظر خود را انتخاب کنید در صورت عدم وجود شهر نزدیک ترین شهر را
          انتخاب کنید
        </Typography>
      </Stack>
      {!isEmptyObject(search) ? (
        <Stack>
          {city
            .filter((x) => x.label.toLowerCase().includes(search.toLowerCase()))
            .map((item, index) => {
              return (
                <Stack key={index}>
                  <Button
                    onClick={() => {
                      dispatch(setCity(item));
                      localStorage.setItem("city", JSON.stringify(item));
                    }}
                    style={styles.itemListCity}
                    className="rowACJSB"
                    variant="text"
                  >
                    <Typography variant="h1">{item.label}</Typography>
                    <BsChevronLeft color={"#000"} />
                  </Button>
                  <Divider />
                </Stack>
              );
            })}
        </Stack>
      ) : (
        <Stack>
          {city.map((item, index) => {
            return (
              <Stack key={index}>
                <Button
                  onClick={() => {
                    dispatch(setCity(item));
                    localStorage.setItem("city", JSON.stringify(item));
                    router.replace("/");
                  }}
                  style={styles.itemListCity}
                  className="rowACJSB"
                  variant="text"
                >
                  <Typography variant="h1">{item.label}</Typography>
                  <BsChevronLeft color={"#000"} />
                </Button>
                <Divider />
              </Stack>
            );
          })}
        </Stack>
      )}
    </Stack>
  );
}
const styles = {
  inputSearch: {
    width: "85%",
    marginTop: 10,
    marginLeft: 10,
  },
  itemListCity: {
    padding: 20,
  },
  header: {
    position: "sticky",
    top: 0,
    zIndex: 999,
    backgroundColor: "white",
  },
};
export default SetCity;
