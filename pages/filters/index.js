import { Button, IconButton, Stack, Typography } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Axios from "axios";
import { GET_FILTER_SPECIFICATIONS } from "../../src/utils/Api";
import { MdArrowBack } from "react-icons/md";
import { useRouter } from "next/router";
import Select from "react-select";
import Link from "next/link";
import { addFilter, clearFilter } from "../../src/redux/actions/FilterActions";
import Btn from "../../src/component/addFile/Button";

export default function Filters() {
  const bottomRef = useRef(null);
  const filterData = useSelector((cat) => cat.filter);
  const filterParams = filterData.params;
  const [bottomSheet, setBottomSheet] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loading2, setLoading2] = useState(true);
  const [modalOpen, setModalOpen] = useState({ open: false, data: null });
  const [hasImageState, setHasImageState] = useState(filterData.hasImage);
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const router = useRouter();
  useEffect(() => {
    if (filterData.catId.id !== -1) {
      getFilters();
    } else {
      if (filterData.catId.id !== -1) {
        router.replace({
          pathname: "/filters/categories",
          query: { parentId: filterData.catId.id },
        });
      }
    }
  }, [filterData]);
  return (
    <Stack>
      <Stack style={styles.header} className="row">
        {hasImageState ||
          (filterParams.length > 0 && (
            <IconButton
              onClick={() => {
                dispatch(clearFilter());
                setHasImageState(false);
              }}
            >
              <Typography variant="h1">حذف همه</Typography>
            </IconButton>
          ))}
        <Typography variant="h1">
          {`فیلتر ${filterData.category && filterData.category.title}`}
        </Typography>

        <IconButton onClick={() => router.back()}>
          <MdArrowBack color={"#111"} size={21} />
        </IconButton>
      </Stack>
      <Stack>
        <Link href="/filters/categories" prefetch={false}>
          <Button
            variant="outlined"
            sx={{
              border: "none",
              padding: 2,
              borderBottom: "1px solid #ccc",
              borderRadius: 0,
            }}
          >
            <Typography variant="h1">
              {filterData.catId !== -1 ? "تغییر دسته بندی" : "انتخاب دسته بندی"}
            </Typography>
            {filterData.catId !== -1 && (
              <Typography
                variant="h1"
                style={{ marginRight: 5 }}
              >{`(${filterData.catId.title})`}</Typography>
            )}
          </Button>
        </Link>

        <Stack className="col">
          {data.map((item, index) => {
            return item.type === "range" ? (
              <Stack className="rowACJC">
                <div
                  className={"col"}
                  style={{ marginTop: index !== 0 ? 10 : 0, width: "95%" }}
                >
                  <Typography variant="h1">{item.title}</Typography>
                  <div className={"rowACJSB"}>
                    <Select
                      className="SelectF11"
                      onChange={(e) => {
                        console.log("e", e);
                        dispatch(
                          addFilter({
                            id: item.filterName,
                            title: e.label,
                            value: e.value,
                            type: item.type,
                            _id: item.id,
                            rangeType: "min",
                          })
                        );
                      }}
                      value={filterParams
                        .filter(
                          (fp) =>
                            fp.id === item.filterName && fp.rangeType === "min"
                        )
                        .map((fpg) => {
                          return { label: fpg.title, value: fpg.value };
                        })}
                      placeholder={"از"}
                      options={item.numberSpecificationFilters.map((it) => {
                        return { label: it.title, value: it.value };
                      })}
                    />
                    <Select
                      className="SelectF1"
                      onChange={(e) => {
                        console.log("e2", e);
                        dispatch(
                          addFilter({
                            id: item.filterName,
                            title: e.label,
                            value: e.value,
                            type: item.type,
                            _id: item.id,
                            rangeType: "max",
                          })
                        );
                      }}
                      value={filterParams
                        .filter(
                          (fp) =>
                            fp.id === item.filterName && fp.rangeType === "max"
                        )
                        .map((fpg) => {
                          return { label: fpg.title, value: fpg.value };
                        })}
                      placeholder={"تا"}
                      options={item.numberSpecificationFilters.map((it) => {
                        return { label: it.title, value: it.value };
                      })}
                    />
                  </div>
                </div>
              </Stack>
            ) : (
              <Stack className="rowACJC">
                <div className={"col"} style={{ marginTop: 10, width: "95%" }}>
                  <Typography variant="h1">{item.title}</Typography>
                  <Select
                    className="Select"
                    onChange={(e) => {
                      dispatch(
                        addFilter({
                          id: item.filterName,
                          title: e.label,
                          value: e.value,
                          type: item.type,
                        })
                      );
                    }}
                    value={filterParams
                      .filter((fp) => fp.id === item.filterName)
                      .map((fpg) => {
                        return { label: fpg.title, value: fpg.value };
                      })}
                    placeholder={"انتخاب کنید"}
                    options={item.listSpecificationFilters.map((it) => {
                      return { label: it.title, value: it.id };
                    })}
                  />
                </div>
              </Stack>
            );
          })}
        </Stack>
      </Stack>
      <Btn text="اعمال فیلتر" onClick={() => router.push("/")} />
    </Stack>
  );
  function getFilters() {
    setLoading(true);
    Axios.get(GET_FILTER_SPECIFICATIONS + `/${filterData.catId.id}`)
      .then((res) => {
        setLoading(false);
        console.log("njhk", res.data);
        let filters = [];
        if (res.data.data.listSpecifications.length > 0) {
          res.data.data.listSpecifications.map((item1) => {
            filters.push({ type: "list", ...item1 });
          });
        }
        if (res.data.data.numberSpecifications.length > 0) {
          res.data.data.numberSpecifications.map((item2) => {
            filters.push({ type: "range", ...item2 });
          });
        }
        setData(filters);
      })
      .catch((err) => {
        setLoading(false);
        console.log("err", err.response);
      });
  }
  function prepareFilter() {
    setLoading(true);
    let listSpecs = [];
    let numberSpecsResult = [];
    filterParams.forEach((item) => {
      if (item.type === "list") {
        listSpecs.push(item.value);
      } else {
        let values = Array(2).fill(0);
        if (item.rangeType === "min") {
          values[0] = item.value;
        }
        if (item.rangeType === "max") {
          values[1] = item.title;
        }
        if (numberSpecsResult.findIndex((ite) => ite.id === item._id) === -1) {
          numberSpecsResult.push({ id: item._id, value: values });
        } else {
          let tmp = numberSpecsResult.filter((ite) => ite.id === item._id)[0];
          tmp.value[tmp.value.indexOf(0)] = item.value;
        }
      }
    });
    let result = {};
    if (listSpecs.length > 0) {
      result.ListSpecifications = listSpecs;
    }
    if (numberSpecsResult.length > 0) {
      result.NumberSpecifications = numberSpecsResult;
    }
    dispatch(prepareFilterForData(result));
    dispatch(needRefresh(true));
    dispatch(setHasImage(hasImageState));
    setLoading(false);
    navigation.navigate("Home");
  }
}
const styles = {
  header: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "space-evanly",
    padding: 17,
    position: "sticky",
    zIndex: 99,
    top: 0,
    boxShadow: "0px 1px 2px #ccc",
    right: 0,
    justifyContent: "space-between",
  },
};
