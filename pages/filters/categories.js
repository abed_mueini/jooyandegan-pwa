import {
  Button,
  CircularProgress,
  Divider,
  Stack,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";
import { HiOutlineChevronLeft } from "react-icons/hi";
import { useRouter } from "next/router";
import { GET_CATEGORIES } from "../../src/utils/Api";
import axios from "axios";
import { fetchCategoies } from "../../src/redux/actions/AddFileActions";
import { setCatId } from "../../src/redux/actions/FilterActions";

export default function Categories() {
  const files = useSelector((state) => state.files);
  const categories = files.categories;
  const [data, setData] = useState([]);
  const categories2 = useSelector((cat) => cat.filter.data);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();
  useEffect(() => {
    if (categories.length === 0) {
      getCategories();
    }
  }, []);
  useEffect(() => {
    if (categories.length !== 0) {
      getRelatedCategories();
    }
  }, [router.query, categories]);
  return (
    <Stack>
      <MyHeader
        title={
          router.query.parentId
            ? categories2.filter(
                (po) => po.id === parseInt(router.query.parentId)
              )[0].title
            : "انتخاب دسته بندی"
        }
        back
      />
      {loading ? (
        <Stack className="loading">
          <CircularProgress />
        </Stack>
      ) : (
        <Stack>
          {data.map((item, index) => {
            return (
              <Stack                   key={index}
              >
                <Button
                  onClick={() => {
                    if (item.isAllCategory) {
                      dispatch(setCatId({ id: item.id, title: item.title }));
                      router.replace("/filters");
                    } else {
                      router.push({
                        pathname: "categories",
                        query: { parentId: item.id },
                      });
                    }
                  }}
                  style={styles.btn}
                >
                  <Typography variant="h1" style={styles.itemTitle}>
                    {item.title}
                  </Typography>
                  <HiOutlineChevronLeft />
                </Button>
                <Divider />
              </Stack>
            );
          })}
        </Stack>
      )}
    </Stack>
  );
  function getCategories() {
    setLoading(true);

    axios
      .get(GET_CATEGORIES)
      .then((res) => {
        setLoading(false);
        dispatch(fetchCategoies(res.data.data));
      })
      .catch((err) => {
        setLoading(false);
        console.log("err add", err);
      });
  }

  function getRelatedCategories() {
    const parentId =
      router.query.parentId !== undefined
        ? parseInt(router.query.parentId)
        : null;

    let temp = categories.filter((cat) => cat.parentId === parentId);
    if (temp.length === 0 && parentId !== null) {
      let lastCategory = categories.filter((fil) => fil.id === parentId)[0];
      dispatch(setCatId({ id: lastCategory.id, title: lastCategory.title }));
      router.replace("/filters");
    } else {
      setData(temp);
    }
  }
}
const styles = {
  body: {
    display: "flex",
    flex: 1,
    height: "100vh",
    overflow: "hidden",
    justifyContent: "center",
  },
  container: {
    backgroundColor: "white",
    border: "1px #aaa solid",
    borderTop: "5px #286fbb solid",
    overflow: "auto",
    marginTop: 70,
  },
  title: {
    fontSize: 16,
    color: "black",
    padding: 10,
  },
  itemTitle: {
    fontSize: 14,
    padding: 10,
    color: "#000",
  },
  infoText: {
    fontSize: 14,
    marginTop: 7,
    color: "#555",
  },
  btn: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
  },
};
