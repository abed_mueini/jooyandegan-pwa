import Head from "next/head";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider } from "@emotion/react";
import "/styles/globals.css";
import createEmotionCache from "../src/utils/createEmotionCache";
import theme from "../src/theme/theme";
import { Provider } from "react-redux";
import { store } from "../src/redux/store";
import Layout from "../src/component/Layout";

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

export default function MyApp(props) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <Provider store={store}>
      <Layout>
        <CacheProvider value={emotionCache}>
          <Head>
            <title>جویندگان</title>
            <meta
              name="viewport"
              content="initial-scale=1, width=device-width"
            />
          </Head>
          <ThemeProvider theme={theme}>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            <Component {...pageProps} />
          </ThemeProvider>
        </CacheProvider>
      </Layout>
    </Provider>
  );
}
