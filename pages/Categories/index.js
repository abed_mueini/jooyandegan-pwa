import { Stack, Typography, Button, Divider } from "@mui/material";
import React, { useEffect, useState } from "react";
import TabNavigation from "../../src/component/navigation/TabNavigation";
import MyHeader from "../../src/component/public/MyHeader";
import { BsChevronLeft } from "react-icons/bs";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { GET_CATEGORIES } from "../../src/utils/Api";
import axios from "axios";
import { useRouter } from "next/router";
import { fetchCategoies } from "../../src/redux/actions/AddFileActions";
import { setCatId } from "../../src/redux/actions/FilterActions";
const Home = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const files = useSelector((state) => state.files);
  const categories = files.categories;
  const [data, setData] = useState([]);
  const router = useRouter();
  useEffect(() => {
    if (categories.length === 0) {
      getCategories();
    }
  }, []);
  useEffect(() => {
    if (categories.length !== 0) {
      getRelatedCategories();
    }
  }, [router.query, categories]);
  return (
    <Stack>
      <TabNavigation name={"categories"} />
      <MyHeader title="دسته بندی" />
      <Stack>
        {data.map((item, index) => {
          return (
            <>
              <Button
              
                onClick={() => {
                  if (item.isAllCategory) {
                    router.push({
                      pathname: "/",
                      query: {
                        id: item.id,
                      },
                    });
                  } else {
                    router.push({
                      pathname: "Categories",
                      query: { parentId: item.id },
                    });
                  }
                }}
                key={index}
                style={styles.rowACJSB}
              >
                <Typography variant="h1">{item.title}</Typography>
                <BsChevronLeft color={"#000"} />
              </Button>
              <Divider />
            </>
          );
        })}
      </Stack>
    </Stack>
  );
  // function getCategories(flag) {
  //   if (categoriesObject.mainCategories.length === 0) {
  //     setLoading(true);
  //     axios
  //       .get(GET_CATEGORIES)
  //       .then((res) => {
  //         setLoading(false);
  //         console.log("res.data", res.data);
  //         dispatch(saveHomeMainCategories(res.data.data));
  //         if (flag && router.query && parseInt(router.query.item)) {
  //           let temp = res.data.data.filter(
  //             (cat) => cat.parentId === parseInt(router.query.item)
  //           );
  //           setData(temp);
  //           // dispatch(setHomeCategoryList(temp));
  //         } else {
  //           setData(res.data.data.filter((cat) => cat.parentId === null));
  //         }
  //       })
  //       .catch((err) => {
  //         setLoading(false);
  //         console.log("erro cat", err);
  //       });
  //   } else {
  //     if (flag && router.query && parseInt(router.query.item)) {
  //       let temp = categoriesObject.mainCategories.filter(
  //         (cat) => cat.parentId === parseInt(router.query.item)
  //       );
  //       setData(temp);
  //       // dispatch(setHomeCategoryList(temp));
  //     } else {
  //       setData(
  //         categoriesObject.mainCategories.filter((cat) => cat.parentId === null)
  //       );
  //     }
  //   }
  // }
  function getCategories() {
    setLoading(true);

    axios
      .get(GET_CATEGORIES)
      .then((res) => {
        setLoading(false);
        dispatch(fetchCategoies(res.data.data));
      })
      .catch((err) => {
        setLoading(false);
        console.log("err add", err);
      });
  }

  function getRelatedCategories() {
    const parentId =
      router.query.parentId !== undefined
        ? parseInt(router.query.parentId)
        : null;

    let temp = categories.filter((cat) => cat.parentId === parentId);
    if (temp.length === 0 && parentId !== null) {
      var lastCategory = categories.filter((fil) => fil.id === parentId)[0];
      router.replace({
        pathname: "/",
        query: {
          id: lastCategory.id,
        },
      });
    } else {
      setData(temp);
    }
  }
};
const styles = {
  rowACJSB: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    padding: 20,
  },
};
export default Home;
