/* eslint-disable @next/next/no-img-element */
import {
  Stack,
  CircularProgress,
  Typography,
  IconButton,
  Divider,
  Button,
} from "@mui/material";
import Axios from "axios";
import { useRouter } from "next/dist/client/router";
import React, { useEffect, useState } from "react";
import ImageSliderFile from "../../src/component/infoFiles/SliderImage/imageSliderFile";
import { FAVORITE, GET_FILES } from "../../src/utils/Api";
import { RiBookmarkFill, RiBookmarkLine } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";
import {
  addToFavorites,
  removeFromFavorites,
} from "../../src/redux/actions/FavoritesAction";
import { COLOR_PRIMARY } from "../../src/theme/theme";
import moment from "moment-jalaali";
import { BsChevronRight } from "react-icons/bs";
import { persianNumber } from "../../src/utils/Persian";
import InfoContactBottomSheet from "../../src/component/infoFiles/BottomSheet/InfoContactBottomSheet";
import Btn from "../../src/component/addFile/Button";
import { isEmptyObject } from "../../src/utils/Tools";
export default function Details() {
  const [isloading, setIsLoading] = useState(false);
  const [data, setData] = useState({});
  const favorites = useSelector((state) => state.favorites.data);
  const dispatch = useDispatch();
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  useEffect(() => {
    getDetails(router.query.id);
  }, []);
  console.log("data", data);
  return (
    <Stack className="colAC">
      {isloading ? (
        <Stack className="loading">
          <CircularProgress color="primary" />
        </Stack>
      ) : (
        <Stack>
          <Stack style={{ width: "100%" }} className="rowACJC">
            <Stack className="back" style={styles.header}>
              <IconButton
                onClick={() => {
                  if (favorites.includes(data.id)) {
                    favoriteToggle("remove");
                  } else {
                    favoriteToggle("add");
                  }
                }}
              >
                {favorites.includes(data.id) ? (
                  <RiBookmarkFill
                    style={{ margin: 10 }}
                    size={25}
                    color={COLOR_PRIMARY}
                  />
                ) : (
                  <RiBookmarkLine
                    style={{ margin: 10 }}
                    size={25}
                    color={"#ffff"}
                  />
                )}
              </IconButton>
              <IconButton onClick={() => router.back()}>
                <BsChevronRight color="white" />
              </IconButton>
            </Stack>
          </Stack>
          {data.images && data.images.length > 0 ? (
            <Stack className="rowACJC">
              <ImageSliderFile data={data} images={data.images} />
            </Stack>
          ) : (
            <Stack className="rowACJC" style={styles.noImageHeader}>
              <img src="/images/noImage2.png" style={styles.noImage} alt="" />
            </Stack>
          )}
          <Stack className="row">
            <Typography m={2} fontSize={18} variant="h1">
              {data.title}
            </Typography>
          </Stack>
          <Stack className="row">
            <Typography m={2} color={COLOR_PRIMARY} fontSize={18} variant="h1">
              مشخصات
            </Typography>
          </Stack>
          <Stack width={"100%"} className="rowACJSB">
            <Typography mr={2} color="#aaa" variant="body1">
              {data.fullCategory}
            </Typography>
            <Typography ml={2} color="#aaa" variant="body1">
              {getTime(data.issuedDateTime)}
            </Typography>
          </Stack>
          <Divider style={styles.divider} />
          <Stack mt={2} width={"100%"} className="rowACJSB">
            <Typography mr={2} variant="body1">
              کدفایل
            </Typography>
            <Typography ml={2} color="#aaa" variant="body1">
              {data.id}
            </Typography>
          </Stack>
          <Divider style={styles.divider} />
          <Stack mt={2} width={"100%"} className="rowACJSB">
            <Typography mr={2} variant="body1">
              مژدگانی
            </Typography>
            <Typography ml={2} color="#aaa" variant="body1">
              {data.giftAmount
                ? `${persianNumber(data.giftAmount)} تومان`
                : "ندارد"}
            </Typography>
          </Stack>
          <Divider style={styles.divider} />
          {data.numberSpecifications &&
            data.numberSpecifications.map((item) => {
              return (
                <Stack key={item.id}>
                  <Stack mt={2} width={"100%"} className="rowACJSB">
                    <Typography mr={2} variant="body1">
                      {item.title}
                    </Typography>
                    <Typography ml={2} color="#aaa" variant="body1">
                      {item.value}
                    </Typography>
                  </Stack>
                  <Divider style={styles.divider} />
                </Stack>
              );
            })}
          {data.listSpecifications &&
            data.listSpecifications.map((item) => {
              return (
                <Stack key={item.id}>
                  <Stack mt={2} width={"100%"} className="rowACJSB">
                    <Typography mr={2} variant="body1">
                      {item.title}
                    </Typography>
                    <Typography ml={2} color="#aaa" variant="body1">
                      {item.value}
                    </Typography>
                  </Stack>
                  <Divider style={styles.divider} />
                </Stack>
              );
            })}
          {!isEmptyObject(data.description) && (
            <Stack className="col">
              <Typography
                m={2}
                color={COLOR_PRIMARY}
                fontSize={18}
                variant="h1"
              >
                توضیحات
              </Typography>
              <Typography sx={{ marginInline: 2 }} variant="body1">
                {data.description}
              </Typography>
            </Stack>
          )}
          <InfoContactBottomSheet
            open={isOpen}
            onClose={() => setIsOpen(false)}
            phone={data.advertiserPhoneNumber}
          />
        </Stack>
      )}
      <Stack style={{ height: 100 }} />
      <>
        {!isOpen && (
          <Stack>
            {router.query.edit ? (
              <Stack
                style={{ position: "fixed", bottom: 0, width: "100%" }}
                className="rowACJC"
              >
                <Stack style={styles.dividerBtn} />
                {data.status !== 250 && data.status !== 100 && (
                  <Button
                    onClick={() =>
                      router.push({
                        pathname: "/managmentFile",
                        query: {
                          status: data.status,
                          id: data.id,
                        },
                      })
                    }
                    style={styles.btn}
                    variant="contained"
                    color="primary"
                  >
                    <Typography variant="h1" color="white">
                      مدیریت آگهی
                    </Typography>
                  </Button>
                )}
              </Stack>
            ) : (
              <Btn onClick={() => setIsOpen(true)} text="اطلاعات تماس" />
            )}
          </Stack>
        )}
      </>
    </Stack>
  );
  function getTime(time) {
    let now = moment(new Date()); //todays date
    let end = moment(time); // another date
    let hours = now.diff(end, "hours");
    let days = now.diff(end, "days");
    let weeks = now.diff(end, "weeks");
    if (hours < 1) {
      return "دقایقی پیش";
    } else if (hours >= 1 && hours < 24) {
      return `${hours} ساعت پیش`;
    } else if (hours >= 24 && days < 30) {
      return `${days} روز پیش`;
    } else {
      return `${weeks} هفته پیش`;
    }
  }
  async function getDetails(id) {
    setIsLoading(true);
    let token = "";
    try {
      const value = await localStorage.getItem("token");
      token = value;
    } catch (e) {
      console.log("zze", e);
    }
    Axios.get(GET_FILES + "/" + id, {
      headers: { Authorization: "bearer " + token },
    })
      .then((res) => {
        setIsLoading(false);
        if (res.data.isSuccess) {
          setData(res.data.data);
        }
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err);
      });
  }
  async function favoriteToggle(type) {
    let token = "";
    try {
      const value = await localStorage.getItem("token");
      token = value;
    } catch (e) {
      console.log("zze", e);
    }
    Axios({
      url: FAVORITE,
      method: type === "add" ? "post" : "put",
      data: { id: data.id },
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => {
        console.log("ok");
        if (type === "add") {
          dispatch(addToFavorites(data.id));
        } else {
          dispatch(removeFromFavorites(data.id));
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  }
}
const styles = {
  noImageHeader: {
    width: "100%",
    height: 280,
    backgroundColor: "#eee",
  },
  noImage: {
    width: 100,
    height: 100,
    objectFit: "contain",
  },
  header: {
    position: "fixed",
    top: 0,
    zIndex: 99,
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row-reverse",
  },
  divider: {
    marginTop: 15,
  },
  btn: {
    padding: 15,
    width: "100%",
    borderRadius: 0,
  },
  btn2: {
    padding: 15,
    width: "100%",
    borderRadius: 0,
  },
  dividerBtn: {
    height: 45,
    width: 2,
    backgroundColor: "#ffff",
  },
};
