import { Button, Divider, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import MyHeader from "../../src/component/public/MyHeader";
import { BsChevronLeft } from "react-icons/bs";
import { AiOutlineEdit, AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import DeleteFileModal from "../../src/modals/DeleteFileModal";
import { useRouter } from "next/router";
import VerifyFileModal from "../../src/modals/VerifyFileModal";
export default function ManageFile() {
  const [deleteModal, setDeleteModal] = useState(false);
  const [verifyModal, setVerifyModal] = useState(false);

  const router = useRouter();
  let data = {};
  data.id = parseInt(router.query.id);
  data.status = parseInt(router.query.status);
  return (
    <Stack className="col">
      <MyHeader back title="مدیریت آگهی" />
      <Button
        onClick={() =>
          router.push({
            pathname: "edit",
            query: {
              id: router.query.id,
            },
          })
        }
        style={styles.itemBtn}
        variant="outlined"
        className="rowACJSB"
      >
        <Stack className="rowACJC">
          <AiOutlineEdit size={27} color="#000" />
          <Typography mr={1} variant="h1">
            ویرایش فایل
          </Typography>
        </Stack>
        <BsChevronLeft color={"#000"} />
      </Button>
      <Divider />
      <Button
        onClick={() => setVerifyModal(true)}
        style={styles.itemBtn}
        variant="outlined"
        className="rowACJSB"
      >
        <Stack className="rowACJC">
          <AiOutlineCheck size={27} color="#000" />
          <Typography mr={1} variant="h1">
            پیدا شد
          </Typography>
        </Stack>
        <BsChevronLeft color={"#000"} />
      </Button>
      <Divider />
      <Button
        onClick={() => setDeleteModal(true)}
        style={styles.itemBtn}
        variant="outlined"
        className="rowACJSB"
      >
        <Stack className="rowACJC">
          <AiOutlineClose size={27} color={"#000"} />
          <Typography mr={1} variant="h1">
            حذف فایل
          </Typography>
        </Stack>
        <BsChevronLeft color={"#000"} />
      </Button>
      <Divider />
      <DeleteFileModal
        open={deleteModal}
        onClose={() => setDeleteModal(false)}
        data={data}
      />
      <VerifyFileModal
        open={verifyModal}
        onClose={() => setVerifyModal(false)}
        id={parseInt(router.query.id)}
      />
    </Stack>
  );
}
const styles = {
  itemBtn: {
    padding: 20,
    border: "none",
  },
};
