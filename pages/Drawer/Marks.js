import { CircularProgress, Stack, Typography, Divider } from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import AdverItem from "../../src/component/index/AdverItem";
import MyHeader from "../../src/component/public/MyHeader";
import { COLOR_PRIMARY } from "../../src/theme/theme";
import { FAVORITE } from "../../src/utils/Api";

function Marks() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  React.useEffect(() => {
    getData();
  }, []);
  console.log("fav", data);
  return (
    <Stack>
      <MyHeader title="نشان شده ها" back />
      {loading ? (
        <Stack className="loading">
          <CircularProgress color="primary" />
        </Stack>
      ) : (
        <Stack>
          {data.length > 0 ? (
            <Stack className={"colJCAC"}>
              {data.map((item) => {
                return (
                  <>
                    <AdverItem item={item} />
                    <Divider />
                  </>
                );
              })}
            </Stack>
          ) : (
            <Stack className="loading">
              <Typography fontSize={18} variant="h1" color={COLOR_PRIMARY}>
                هیج موردی وجود ندارد
              </Typography>
            </Stack>
          )}
        </Stack>
      )}
    </Stack>
  );
  function getData() {
    setLoading(true);
    let token = "";
    try {
      const value = localStorage.getItem("token");
      token = value;
    } catch (e) {
      console.log("zze", e);
    }
    axios
      .get(FAVORITE + `?page=${page}`, {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
      .then((res) => {
        setLoading(false);
        if (res.data.isSuccess) {
          setData(res.data.data.files);
          setCount(res.data.data.totalItems);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log("favListErr", err);
        console.log(err);
      });
  }
}
export default Marks;
