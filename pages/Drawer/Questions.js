import { Divider, Stack, Typography } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";
import { COLOR_PRIMARY } from "../../src/theme/theme";
function Questions() {
  const data = useSelector((ss) => ss.splash.data.questions);

  return (
    <Stack>
      <MyHeader title="سوالات متداول" back />
      {data.map((item) => {
        return (
          <Stack key={item.id}>
            <Stack className="row">
              <Typography m={2} variant="h1" color={COLOR_PRIMARY}>
                {item.title}
              </Typography>
            </Stack>
            <Stack>
              <Typography m={2} variant="body">
                {item.content}
              </Typography>
            </Stack>
            <Divider sx={{ backgroundColor: "#fefefe" }} />
          </Stack>
        );
      })}
    </Stack>
  );
}

export default Questions;
