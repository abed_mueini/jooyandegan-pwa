import {
  Button,
  Card,
  CircularProgress,
  Divider,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";
import MySnackBar from "../../src/component/public/MySnackBar";
import { ADMIN_MESSAGE } from "../../src/utils/Api";
import axios from "axios";
function ContactUs() {
  const data = useSelector((state) => state.splash.data.contactUs);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  return (
    <Stack>
      <MyHeader title="تماس با ما" back />
      <Stack p={4} className="rowACJC">
        <Typography variant="body1" color="#ccc">
          از طریق یکی از راه های زیر با ما تماس بگیرید
        </Typography>
      </Stack>
      <Stack className={"rowACJC"}>
        <Card style={styles.card} className="colJCAC">
          <Stack p={2} className="rowACJC">
            <img src="/images/contact us.png" style={styles.image} alt="" />
          </Stack>
          <Divider style={styles.diviedr} />
          <Stack p={2} width={"100%"} className="rowACJSB">
            <Typography variant="h1">واحد پشتیبانی</Typography>
            <Typography variant="h1">{data.supportPhone}</Typography>
          </Stack>
          <Stack p={2} width={"100%"} className="rowACJSB">
            <Typography variant="h1">مدیریت</Typography>
            <Typography variant="h1">{data.phone}</Typography>
          </Stack>
        </Card>
      </Stack>
      <Stack mt={3} className={"rowACJC"}>
        <Card style={styles.card} className="colJCAC">
          <Stack p={2} className="rowACJC">
            <img src="/images/email.png" style={styles.image} alt="" />
          </Stack>
          <Divider style={styles.diviedr} />
          <Stack p={2} width={"100%"} className="rowACJSB">
            <Typography variant="h1">آدرس ایمیل</Typography>
            <Typography variant="h1">{data.email}</Typography>
          </Stack>
        </Card>
      </Stack>
      <Stack mt={3} className={"rowACJC"}>
        <Card style={styles.card} className="colJCAC">
          <Stack p={2} className="rowACJC">
            <Typography variant="body1">
              با استفاده از فرم زیر برای ما پیام ارسال کنید
            </Typography>
          </Stack>
          <Divider style={styles.diviedr} />
          <Stack p={2} width={"100%"} className="rowACJC">
            <TextField
              variant="outlined"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              style={styles.input}
              placeholder="عنوان"
            />
          </Stack>
          <Stack p={2} mt={-2} width={"100%"} className="rowACJC">
            <TextField
              variant="outlined"
              value={content}
              onChange={(e) => setContent(e.target.value)}
              style={styles.input}
              placeholder="توضیحات..."
              multiline
            />
          </Stack>
          <Stack p={2} width={"100%"} className="rowACJC">
            <Button
              style={{ width: "70%", padding: 10 }}
              variant="contained"
              color="primary"
              onClick={submitTicket}
            >
              {isLoading ? (
                <CircularProgress color="secondary" size={20} />
              ) : (
                <Typography variant="h1" color="white">
                  ارسال
                </Typography>
              )}
            </Button>
          </Stack>
        </Card>
      </Stack>
      <Stack p={3} className="colJCAC">
        <Typography variant="h1">ساعات پاسخگویی :</Typography>
        <Typography variant="body1">
          {`روز های غیر تعطیل از ساعات ${data.startWorkTime} الی ${data.endWorkTime}`}
        </Typography>
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  async function submitTicket() {
    setIsLoading(true);
    if (title.length !== 0) {
      if (content.length !== 0) {
        let token = "";
        try {
          const value = await localStorage.getItem("token");
          token = value;
        } catch (e) {
          console.log("zze", e);
        }
        let temp = {
          Title: title,
          Content: content,
        };
        axios
          .post(ADMIN_MESSAGE, temp, {
            headers: {
              Authorization: "Bearer " + token,
            },
          })
          .then((res) => {
            console.log("res", res.data);
            setIsLoading(false);
            if (res.data.isSuccess) {
              setSnackBar({ open: true, message: res.data.data });
              setContent("");
              setTitle("");
            }
          })
          .catch((err) => {
            console.log("err", err);
            setIsLoading(false);
            if (err.response === 401) {
              setSnackBar({
                open: true,
                message: "ابتدا وارد حساب کاربری خود شوید",
              });
              router.push("auth");
            }
          });
      } else {
        setSnackBar({ open: true, message: "توضیحات نباید خالی باشد" });
        setIsLoading(false);
      }
    } else {
      setSnackBar({ open: true, message: "عنوان نباید خالی باشد" });
      setIsLoading(false);
    }
  }
}
const styles = {
  card: {
    width: "95%",
    boxShadow: "1px 1px 13px #ccc",
  },
  image: {
    width: 25,
    height: 25,
    objectFit: "contain",
  },
  diviedr: {
    width: "90%",
    marginBottom: 5,
    marginTop: 10,
  },
  input: {
    width: "80%",
    padding: 5,
  },
};
export default ContactUs;
