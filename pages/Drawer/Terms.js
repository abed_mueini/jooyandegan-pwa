import { Divider, Stack, Typography } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import MyHeader from "../../src/component/public/MyHeader";

function Terms() {
  const data = useSelector((st) => st.splash.data.rule);
  return (
    <Stack>
      <MyHeader title="قوانین و مقررات" back />
      <Stack style={styles.header} className="rowACJC">
        <img src="/images/logo.png" style={styles.image} alt="" />
      </Stack>
      <Divider />
      <Typography lineHeight={2} margin={3} variant="body1">
        {data.content}
      </Typography>
    </Stack>
  );
}
const styles = {
  header: {
    height: 170,
  },
  image: {
    height: 80,
    width: 80,
    objectFit: "contain",
  },
};
export default Terms;
