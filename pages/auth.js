import React, { useState } from "react";
import {
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { MdPhoneEnabled } from "react-icons/md";
import { Fa2En } from "../src/utils/Persian";
import MyButton from "../src/component/public/MyButton";
import MySnackBar from "../src/component/public/MySnackBar";
import axios from "axios";
import { AUTH_SEND_CODE } from "../src/utils/Api";
import { useRouter } from "next/router";
import { isEmptyObject } from "../src/utils/Tools";
import { COLOR_SECOUNDRY } from "../src/theme/theme";
import { MdArrowBack } from "react-icons/md";

export default function Auth() {
  const [phone, setPhone] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const router = useRouter();
  return (
    <Stack className="col" style={styles.container}>
      <Stack className="rowACJC" style={styles.header}>
        <img src="/images/logo.png" alt="" style={styles.image} />
      </Stack>
      <Stack className="rowAC" style={styles.backContainer}>
        <IconButton>
          <MdArrowBack color={"#111"} size={21} />
        </IconButton>
      </Stack>
      <Stack style={styles.inputContainer} className="colJCAC">
        <Typography color="#ccc" variant="body1">
          یک کد برای این شماره ارسال خواهد شد
        </Typography>
        <TextField
          value={phone}
          onChange={(e) => setPhone(Fa2En(e.target.value))}
          style={{ width: "90%", marginTop: 15, backgroundColor: "white" }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <MdPhoneEnabled
                  style={{ fontSize: 25, color: COLOR_SECOUNDRY }}
                />
              </InputAdornment>
            ),
          }}
          placeholder="شماره تلفن همراه خود را وارد کنید"
        />
      </Stack>
      <Stack className="rowACJC">
        <MyButton
          onClick={() => requestCode()}
          isLoading={isLoading}
          style={{ padding: 10 }}
          title="ارسال"
        />
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
  function requestCode() {
    setIsLoading(true);
    if (!isEmptyObject(phone)) {
      let reg = /^(\+98|0098|98|0)?9\d{9}$/;
      if (reg.test(phone)) {
        let data = {
          PhoneNumber: phone,
        };
        axios
          .post(AUTH_SEND_CODE, data)
          .then((res) => {
            setIsLoading(false);
            if (res.data.isSuccess) {
              router.push({
                pathname: "auth-verification",
                query: {
                  Phone: phone,
                },
              });
            } else {
              setIsLoading(false);
              setSnackBar({ open: true, message: res.data.data });
            }
          })
          .catch((err) => {
            setIsLoading(false);
            console.log("error", err);
          });
      } else {
        setIsLoading(false);
        setSnackBar({ open: true, message: "فرمت شماره همراه اشتباه است" });
      }
    } else {
      setIsLoading(false);
      setSnackBar({ open: true, message: "شماره همراه نباید خالی باشد!" });
    }
  }
}
const styles = {
  container: {
    display: "flex",
    width: "100%",
    height: "100vh",
    backgroundColor: "#fafafa",
  },
  image: {
    width: 100,
    height: 100,
    objectFit: "contain",
  },
  header: {
    height: 180,
    backgroundColor: "white",
  },
  inputContainer: {
    padding: 25,
  },
  backContainer: {
    width: "100%",
    backgroundColor: "white",
    borderBottom: "1px solid #eee",
    justifyContent: "flex-end",
  },
};
