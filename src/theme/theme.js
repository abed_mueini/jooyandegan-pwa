import { createTheme, responsiveFontSizes } from "@mui/material/styles";
import { FONT_NAME, FONT_NAME_BOLD } from "../utils/Constant";

export const COLOR_PRIMARY = "#2fc391";
export const COLOR_SECOUNDRY = "#44425A";

// Create a theme instance.
let theme = createTheme({
  palette: {
    primary: {
      main: COLOR_PRIMARY,
    },
    secondary: {
      main: COLOR_SECOUNDRY,
    },
  },
  typography: {
    body1: {
      fontFamily: FONT_NAME,
      fontSize: 15,
      color: "#000",
    },
    h1: {
      fontFamily: FONT_NAME_BOLD,
      fontSize: 16,
      color: "#000",
    },
  },
});

theme = responsiveFontSizes(theme);

export default theme;
