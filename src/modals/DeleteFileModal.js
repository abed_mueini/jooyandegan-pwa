import {
  Button,
  CircularProgress,
  Dialog,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import MySnackBar from "../component/public/MySnackBar";
import { DELETE_FILE } from "../utils/Api";
export default function DeleteFileModal({ open, onClose, data }) {
  const [amount, setAmount] = useState("");
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  console.log("data", data);
  return (
    <Dialog
      open={open}
      onClose={() => onClose(false)}
      BackdropProps={() => onClose()}
    >
      <Stack className={"rowACJC"} p={1}>
        <Typography padding={1} variant="body1">
          {"کاربر گرامی در صورت امکان دلیل حذف فایل خودرا بنویسید"}
        </Typography>
      </Stack>
      <Stack className="colJCAC">
        <TextField
          value={amount}
          onChange={(e) => setAmount(e.target.value)}
          variant="outlined"
          placeholder={"دلیل خودرا اینجا بنویسید"}
          style={styles.input}
          multiline
        />
      </Stack>
      <Stack className="colJCAC">
        <Button
          onClick={() => {
            submit();
          }}
          style={styles.btnSubmit}
          variant="contained"
          color="secondary"
          className="rowACJC"
        >
          {loading ? (
            <CircularProgress size={22} color="inherit" />
          ) : (
            <Typography color="white" variant="h1">
              تایید
            </Typography>
          )}
        </Button>
        <Button
          onClick={() => onClose()}
          style={styles.btnBack}
          variant="contained"
          color="secondary"
        >
          <Typography color="white" variant="h1">
            بازگشت
          </Typography>
        </Button>
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Dialog>
  );
  async function submit() {
    if (data.status === 250) {
      setLoading(true);
      let token = "";
      try {
        const value = await localStorage.getItem("token");
        token = value;
      } catch (e) {
        console.log("zze", e);
      }
      axios
        .post(
          DELETE_FILE,
          { id: data.id, comment: "حذف فایل حذف شده‌ از طرف سیستم" },
          {
            headers: {
              Authorization: "Bearer " + token,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          setSnackBar({ open: true, message: res.data.message });
          onClose(true);
        })
        .catch((err) => {
          setLoading(false);
          setSnackBar({
            open: true,
            message:
              "در برقراری ارتباط با سرور مشکلی به وجود آمده است لطفا مجددا تلاش کنید",
          });
          console.log(err.response.data);
        });
    } else {
      setLoading(true);
      let token = "";
      try {
        const value = await localStorage.getItem("token");
        token = value;
      } catch (e) {
        console.log("zze", e);
      }
      axios
        .post(
          DELETE_FILE,
          { id: data.id, comment: amount },
          {
            headers: {
              Authorization: "Bearer " + token,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          setSnackBar({ open: true, message: res.data.message });
          onClose(true);
          router.back();
        })
        .catch((err) => {
          setLoading(false);
          setSnackBar({
            open: true,
            message:
              "در برقراری ارتباط با سرور مشکلی به وجود آمده است لطفا مجددا تلاش کنید",
          });
          console.log("err", err);
        });
    }
  }
}
const styles = {
  input: { width: 360, paddingInline: 30 },
  btnSubmit: {
    width: "90%",
    paddingBottom: 15,
    marginTop: 10,
    paddingTop: 15,
  },
  btnBack: {
    width: "90%",
    paddingBottom: 15,
    paddingTop: 15,
    marginTop: 5,
    marginBottom: 5,
  },
};
