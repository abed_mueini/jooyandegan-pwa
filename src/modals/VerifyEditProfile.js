import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import MyButton from "../component/public/MyButton";
import { COLOR_PRIMARY } from "../theme/theme";

import { FONT_NAME, FONT_NAME_BOLD } from "../utils/Constant";

export default function VerifyEditProfileModal({ open, onClose }) {
  return (
    <Dialog
      open={open}
      onClose={() => onClose(false)}
      BackdropProps={() => onClose()}
    >
      <div className={"colJCAC"}>
        <DialogContent>
          <DialogContentText className={styles.textModal}>
            ویرایش مشخصات با موفقیت انجام شد
          </DialogContentText>
        </DialogContent>
        <Stack className="rowACJC">
          <img src="/images/Confirmation.png" alt="" style={styles.image} />
        </Stack>
      </div>
      <Stack className="rowACJC">
        <Button
          onClick={() => onClose()}
          style={{ width: "90%", padding: 10, marginBottom: 10 }}
          variant="contained"
          color="secondary"
        >
          <Typography color="white" variant="h1">
            تایید
          </Typography>
        </Button>
      </Stack>
    </Dialog>
  );
}
const styles = {
  textModal: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 13,
    textAlign: "center",
  },
  btn1: {
    width: "45%",
    borderRadius: 10,
    marginInline: 5,
    backgroundColor: COLOR_PRIMARY,
  },
  btn2: {
    width: "45%",
    borderRadius: 10,
    marginInline: 5,
    backgroundColor: COLOR_PRIMARY,
  },
  btnContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  textBtn: {
    color: "white",
    fontFamily: FONT_NAME,
    fontSize: 10,
  },
  image: {
    width: 80,
    height: 100,
    objectFit: "contain",
  },
};
