import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import MyButton from "../component/public/MyButton";
import MySnackBar from "../component/public/MySnackBar";
import { saveNumberSpec } from "../redux/actions/AddFileActions";
import { saveNumberSpecEf } from "../redux/actions/EditFileActions";

export default function NumberPickerModal({ open, onClose, data, edit }) {
  const [amount, setAmount] = useState("");
  const dispatch = useDispatch();
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });

  return (
    <Dialog
      open={open}
      onClose={() => onClose(false)}
      BackdropProps={() => onClose()}
    >
      <Stack className={"rowAC"} p={1}>
        <Typography variant="body1">
          {`${data && data.title} را وارد نمایید`}
        </Typography>
      </Stack>
      <Stack className="colJCAC">
        <TextField
          value={amount}
          onChange={(e) => setAmount(e.target.value)}
          variant="outlined"
          placeholder={`${data && data.title}`}
          style={styles.input}
        />
      </Stack>
      <Stack className="colJCAC">
        <Button
          onClick={() => {
            if (amount.length > 0) {
              Submit();
            } else {
              setSnackBar({ open: true, message: "مقدار نباید خالی باشد" });
            }
          }}
          style={styles.btnSubmit}
          variant="contained"
          color="secondary"
          className="rowACJC"
        >
          <Typography color="white" variant="h1">
            تایید
          </Typography>
        </Button>
        <Button
          onClick={() => onClose()}
          style={styles.btnBack}
          variant="contained"
          color="secondary"
        >
          <Typography color="white" variant="h1">
            بازگشت
          </Typography>
        </Button>
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Dialog>
  );
  function Submit() {
    if (edit) {
      dispatch(
        saveNumberSpecEf({
          id: data.id,
          value: parseInt(amount.split(",").join("")),
          isOther: data.isOther,
        })
      );
      onClose();
    } else {
      dispatch(
        saveNumberSpec({
          id: data.id,
          value: parseInt(amount.split(",").join("")),
          isOther: data.isOther,
        })
      );
      onClose();
    }
  }
}
const styles = {
  input: { width: 360, paddingInline: 30 },
  btnSubmit: {
    width: "90%",
    paddingBottom: 15,
    marginTop: 10,
    paddingTop: 15,
  },
  btnBack: {
    width: "90%",
    paddingBottom: 15,
    paddingTop: 15,
    marginTop: 5,
    marginBottom: 5,
  },
};
