import {
  Button,
  CircularProgress,
  Dialog,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import MySnackBar from "../component/public/MySnackBar";
import { DELETE_FILE, VERIFY_FILE } from "../utils/Api";
export default function VerifyFileModal({ open, onClose, id }) {
  const [amount, setAmount] = useState("");
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const Id = id;
  return (
    <Dialog
      open={open}
      onClose={() => onClose(false)}
      BackdropProps={() => onClose()}
    >
      <Stack className={"rowACJC"} p={1}>
        <Typography padding={1} variant="body1">
          {"از انجام این عملیات اطمینان دارید"}{" "}
        </Typography>
      </Stack>
      <Stack width={"100%"} className="colJCAC">
        <Button
          onClick={() => {
            submit();
          }}
          style={styles.btnSubmit}
          variant="contained"
          color="secondary"
          className="rowACJC"
        >
          {loading ? (
            <CircularProgress size={22} color="inherit" />
          ) : (
            <Typography color="white" variant="h1">
              تایید
            </Typography>
          )}
        </Button>
        <Button
          onClick={() => onClose()}
          style={styles.btnBack}
          variant="contained"
          color="secondary"
        >
          <Typography color="white" variant="h1">
            بازگشت
          </Typography>
        </Button>
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Dialog>
  );
  async function submit() {
    setLoading(true);
    let token = "";
    try {
      const value = await localStorage.getItem("token");
      token = value;
    } catch (e) {
      console.log("zze", e);
    }
    axios
      .post(
        VERIFY_FILE,
        { id: Id },
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      )
      .then((res) => {
        setLoading(false);
        if (res.data.isSuccess) {
          setSnackBar({ open: true, message: res.data.message });
        }
        onClose();
        router.back();
      })
      .catch((err) => {
        console.log("err", err);
        setLoading(false);
        if (err.response.status === 400) {
          setSnackBar({
            open: true,
            message: "خطایی رخ داده است مجدد تلاش کنید",
          });
        }
      });
  }
}
const styles = {
  btnSubmit: {
    width: "90%",
    paddingBottom: 15,
    marginTop: 10,
    paddingTop: 15,
  },
  btnBack: {
    width: "90%",
    paddingBottom: 15,
    paddingTop: 15,
    marginTop: 5,
    marginBottom: 5,
  },
};
