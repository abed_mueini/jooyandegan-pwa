import { isEmptyObject } from "./Tools";
const latinToPersianMap = ["۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"];
const latinNumbers = [
  /1/g,
  /2/g,
  /3/g,
  /4/g,
  /5/g,
  /6/g,
  /7/g,
  /8/g,
  /9/g,
  /0/g,
];

export function prepareNumber(input) {
  let string;
  if (typeof input === "number") {
    string = input.toString();
  } else if (typeof input === "undefined") {
    string = "";
  } else {
    string = input;
  }

  return string;
}

function latinToPersian(string) {
  let result = string;

  for (let index = 0; index < 10; index++) {
    result = result.replace(latinNumbers[index], latinToPersianMap[index]);
  }

  return result;
}

export function persianNumber(input, sep = true) {
  if (!isEmptyObject(input)) {
    if (sep) {
      return latinToPersian(
        prepareNumber(
          parseInt(input)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, "،")
        )
      );
    } else {
      return latinToPersian(prepareNumber(input));
    }
  }
}
export function toFarsiNumber(number) {
  const farsiDigits = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

  return number && number.toString().replace(/\d/g, (x) => farsiDigits[x]);
}
export function Fa2En(str) {
  return (
    str
      .replace("۰", "0")
      .replace("۱", "1")
      .replace("۲", "2")
      .replace("۳", "3")
      .replace("۴", "4")
      .replace("۵", "5")
      .replace("۶", "6")
      .replace("۷", "7")
      .replace("۸", "8")
      .replace("۹", "9")
      //iphone numeric
      .replace("٠", "0")
      .replace("١", "1")
      .replace("٢", "2")
      .replace("٣", "3")
      .replace("٤", "4")
      .replace("٥", "5")
      .replace("٦", "6")
      .replace("٧", "7")
      .replace("٨", "8")
      .replace("٩", "9")
  );
}
