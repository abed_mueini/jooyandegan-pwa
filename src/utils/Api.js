export const HOST_V1 = 'https://joyandegan.ir/api/v1/';
export const GET_INITIAL_INFOS = HOST_V1 + 'main/getallinfo';
export const AUTH_SEND_CODE = HOST_V1 + 'account/sendcode';
export const AUTH_VERIFY = HOST_V1 + 'account/Verification';
export const EDIT_PROFILE = HOST_V1 + 'account/SubmitProfile';
export const GET_CATEGORIES = HOST_V1 + 'category';
export const GET_CATEGORIES_SPECIFICATIONS =
  HOST_V1 + 'category/Specifications';
export const GET_MY_FILES = HOST_V1 + 'account/myfiles';
export const SUBMIT_FILE = HOST_V1 + 'File/Add';
export const GET_FILES = HOST_V1 + 'File';
export const FAVORITE = HOST_V1 + 'Favorite';
export const GET_FILTER_SPECIFICATIONS =
  HOST_V1 + 'category/FilterSpecifications';
export const EDIT = HOST_V1 + 'File/Edit';
export const DELETE_FILE = HOST_V1 + 'file/deletefile';
export const VERIFY_FILE = HOST_V1 + 'file/verifyFile';
export const ADMIN_MESSAGE = HOST_V1 + 'main/adminMessage';
export const PAYMENT_GATEWAY = HOST_V1 + 'File/Payment';
