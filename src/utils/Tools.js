export const isEmptyObject = (object) => {
  return (
    object === '' ||
    object === null ||
    object === undefined ||
    object === 'null' ||
    object === '<p><br></p>'
  );
};
export const isEmptyObject2 = (object) => {
  return (
      object === null ||
      object === undefined
  );
};
export const snackBarOptions = {
  position: 'bottom-right',
  style: {
    backgroundColor: 'midnightblue',
    border: '2px solid lightgreen',
    color: 'lightblue',
    zIndex:999999999,
    fontFamily: 'Menlo, monospace',
    fontSize: '20px',
    textAlign: 'center',
  },
  closeStyle: {
    color: 'lightcoral',
    fontSize: '16px',
  },
}
export const htmlExample='<article>\n' +
    '                   \n' +
    '\t\t\t\t\t\t\n' +
    '\t\t\t\t\t\t        <figure class="post-attachment">\n' +
    '\t\t\t<img src="https://www.digikala.com/mag/wp-content/uploads/2020/07/Nearby-Share-1.jpg" width="822" height="522" class="post-module__img wp-post-image" alt="گوگل" data-lazy-loaded="true">            <figcaption class="hidden-seo">\n' +
    '\t\t\t\t            </figcaption>\n' +
    '        </figure>\n' +
    '\t\t\t\n' +
    '                        <div class="post-module__content">\n' +
    '\t\t\t\t\t\t\t<p>بعد از ماه&zwnj;ها شایعات، گوگل بالاخره ارائه&zwnj;ی قابلیت Nearby Share را تایید کرد. در حال حاضر نسخه بتا این مشخصه برای تعدادی از کاربران گوشی&zwnj;های <a href="https://www.digikala.com/mag/tag/%D8%A7%D9%86%D8%AF%D8%B1%D9%88%DB%8C%D8%AF/" target="_blank" rel="noopener noreferrer">اندرویدی </a>ارائه شده است. بر اساس گزارش&zwnj;ها، افرادی که از نسخه بتا سرویس&zwnj;های گوگل پلی استفاده می&zwnj;کنند، می&zwnj;توانند از این قابلیت بهره ببرند.</p>\n' +
    '<p>سخنگوی گوگل به سایت Android Police گفته: «ما در حال حاضر مشغول آزمایش نسخه بتا این مشخصه هستیم و قصد داریم در آینده نزدیک اطلاعات بیشتری در مورد آن به اشتراک بگذاریم. هدف نهایی ما پشتیبانی این قابلیت از گوشی&zwnj;های مبتنی بر اندروید ۶ و جدیدتر و همچنین دیگر پلتفرم&zwnj;ها است.»</p>\n' +
    '<p>همچنین این مشخصه مدتی قبل در نسخه بتا chrome OS هم پدیدار شده و این یعنی کروم&zwnj;بوک&zwnj;ها هم می&zwnj;توانند از این قابلیت بهره ببرند. کارکرد Nearby Share اندروید اساسا مشابه ایردراپ گجت&zwnj;های اپل است. این یعنی با استفاده از آن می&zwnj;توانید به راحتی فایل&zwnj;های موردنظر خود را بین گوشی&zwnj;های اندرویدی نزدیک خود به اشتراک بگذارید.</p>\n' +
    '<p><img src="https://www.digikala.com/mag/wp-content/uploads/2020/07/Nearby-Share-2.jpg" class="aligncenter size-full wp-image-685294" alt="گوگل" width="822" height="530" data-lazy-loaded="true"></p>\n' +
    '<p>در گزارش سایت Android Police آمده که در نسخه بتا این مشخصه، علاوه بر اشتراک عکس&zwnj;ها و ویدیوها از این قابلیت می&zwnj;توان برای اشتراک&zwnj;گذاری لینک&zwnj;ها و توییت&zwnj;ها هم استفاده کرد. در ضمن باید بگوییم از این قابلیت نمی&zwnj;توان برای ارسال فایل به غریبه&zwnj;ها بهره برد.</p>\n' +
    '<p>اولا کاربران موردنظر برای دریافت فایل در بخش Quick Settings باید این قابلیت را فعال کنند و به صورت دستی هم باید دریافت فایل&zwnj;های ارسال شده را بپذیرند. سامسونگ هم مدت&zwnj;ها قبل قابلیت مشابهی به نام Quick Share را معرفی کرده که به افراد اجازه می&zwnj;دهد به&zwnj;طور همزمان به حداکثر ۵ کاربر فایل ارسال کنند.</p>\n' +
    '<p>اما در هر صورت مهم&zwnj;ترین مزیت Nearby Share این است که از تمام گوشی&zwnj;های اندرویدی پشتیبانی می&zwnj;کند ولی قابلیت Quick Share مختص گوشی&zwnj;های سامسونگ است. در این زمینه شرکت&zwnj;های دیگری هم وارد عمل شده&zwnj;اند که برای آن&zwnj;ها هم چنین محدودیتی دیده می&zwnj;شود.</p>\n' +
    '\n' +
    '\n' +
    '                        </div>\n' +
    '                        \n' +
    '\n' +
    '\t\t\t\t\t\t                            \n' +
    '                            \n' +
    '\t\t\t\t\t\t                    </article>';
