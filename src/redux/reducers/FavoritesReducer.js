import {
  ADD_TO_FAVORITES,
  REMOVE_FROM_FAVORITES,
  STORE_FAVORITES,
} from '../Types';

export const favoritesReducer = (state = {data: []}, action) => {
  switch (action.type) {
    case ADD_TO_FAVORITES: {
      return {
        ...state,
        data: state.data.concat(action.payload),
      };
    }
    case STORE_FAVORITES: {
      return {
        ...state,
        data: action.payload,
      };
    }
    case REMOVE_FROM_FAVORITES: {
      return {
        ...state,
        data: state.data.filter((item) => item !== action.payload),
      };
    }
    default:
      return state;
  }
};
