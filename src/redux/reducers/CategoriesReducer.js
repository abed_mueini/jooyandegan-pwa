import {
  ADD_FILES_CHANGE_LEVEL_CAT,
  ADD_HOME_FILES_CHANGE_LEVEL_CAT,
  CLEAR_HISTORY_NAVIGATION_CAT,
  CLEAR_HOME_HISTORY_NAVIGATION_CAT,
  POP_HISTORY_NAVIGATION_CAT,
  POP_HOME_HISTORY_NAVIGATION_CAT,
  SAVE_HISTORY_NAVIGATION_CAT,
  SAVE_HOME_HISTORY_NAVIGATION_CAT,
  SAVE_HOME_MAIN_CATEGORIES_CAT,
  SET_CATEGORY_LIST_CAT,
  SET_HOME_CATEGORY_LIST_CAT,
} from '../Types';

export const categoriesReducer = (
  state = {
    categories: [],
    level: 0,
    homeLevel: 0,
    homeCategoryList: [],
    homeHistory: [],
    mainCategories: [],
    homeMainCategories: [],
    categoryList: [],
    history: [],
  },
  action,
) => {
  switch (action.type) {
    case SAVE_HOME_MAIN_CATEGORIES_CAT: {
      return {
        ...state,
        mainCategories: action.payload,
      };
    }
    case SAVE_HOME_MAIN_CATEGORIES_CAT: {
      return {
        ...state,
        homeMainCategories: action.payload,
      };
    }
    case ADD_FILES_CHANGE_LEVEL_CAT: {
      return {
        ...state,
        level: action.payload,
      };
    }
    case ADD_HOME_FILES_CHANGE_LEVEL_CAT: {
      return {
        ...state,
        homeLevel: action.payload,
      };
    }
    case SET_CATEGORY_LIST_CAT: {
      return {
        ...state,
        categoryList: action.payload,
      };
    }
    case SET_HOME_CATEGORY_LIST_CAT: {
      return {
        ...state,
        homeCategoryList: action.payload,
      };
    }
    case SAVE_HISTORY_NAVIGATION_CAT: {
      return {
        ...state,
        history: state.history.concat(action.payload),
      };
    }
    case SAVE_HOME_HISTORY_NAVIGATION_CAT: {
      return {
        ...state,
        homeHistory: state.homeHistory.concat(action.payload),
      };
    }
    case POP_HOME_HISTORY_NAVIGATION_CAT: {
      return {
        ...state,
        homeHistory: state.homeHistory.filter(
          (item, index) => index !== state.homeHistory.length - 1,
        ),
      };
    }
    case CLEAR_HISTORY_NAVIGATION_CAT: {
      return {
        ...state,
        history: [],
      };
    }
    case CLEAR_HOME_HISTORY_NAVIGATION_CAT: {
      return {
        ...state,
        homeHistory: [],
      };
    }
    case POP_HISTORY_NAVIGATION_CAT: {
      return {
        ...state,
        history: state.history.filter(
          (item, index) => index !== state.history.length - 1,
        ),
      };
    }
    default:
      return state;
  }
};
