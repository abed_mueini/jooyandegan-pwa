import {
  FETCH_CATEGORIES_EF,
  ADD_FILES_CHANGE_LEVEL_EF,
  SAVE_HISTORY_NAVIGATION_EF,
  POP_HISTORY_NAVIGATION_EF,
  CLEAR_HISTORY_NAVIGATION_EF,
  SAVE_CATEGORY_EF,
  SAVE_LIST_SPEC_EF,
  SAVE_LOCATION_EF,
  SAVE_NUMBER_SPEC_EF,
  SAVE_CITY_EF,
  SAVE_ADAPTIVE_EF,
  CLEAR_FILE_INFO_EF,
  ADD_PLAN_EF,
  REMOVE_PLAN_EF,
  ORIGINAL_PLANS,
  SAVE_TITLE_EF,
  SAVE_DESC_EF,
  SAVE_URL_EF,
  SAVE_IMAGES,
  EDIT_IMAGE_IDS_TYPE,
  EDIT_IMAGE_TYPE,
  ADD_IMAGE_TYPE,
  DELETE_IMAGE_TYPE,
  SAVE_ID_EF,
  CLEAR_NUMBER_SPEC_EF,
  CANCEL_DELETE_IMAGE_TYPE,
  CANCEL_EDIT_IMAGE_TYPE,
  CANCEL_ADD_IMAGE_TYPE,
  PLAN_DELETE,
  PLAN_ADD_IMAGE,
  PLAN_ADD_TITLE,
  EDIT_PLAN_EF,
  PLAN_DELETE_CANCEL,
  PLAN_ADD_IMAGE_CANCEL,
  PLAN_ADD_TITLE_CANCEL,
  CLEAR_LIST_SPEC_EF,
  SAVE_ADDRESS_EF,
  SAVE_OWNER_PHONE_EF,
  SAVE_CELL_PHONE_EF,
  ADVER_TISER_PHONE_NUMBER_EF,
  SAVE_GIFT_FILE_EF,
  SET_ADVER_TISER_PHONE_EF,
  SET_BOTTOM_SHEET_EF,
} from "../Types";

export const editFileReducer = (
  state = {
    categories: [],
    level: null,
    id: null,
    history: [],
    category: null,
    listSpecs: [],
    cityId: null,
    longitude: null,
    latitude: null,
    numberSpecs: [],
    adaptive: false,
    plans: [],
    title: "",
    desc: "",
    url: "",
    images: [],
    address: "",
    addImages: [],
    editFileImages: [],
    editFileImagesId: [],
    deleteImages: [],
    planAddImages: [],
    planDeleteImages: [],
    planAddTitle: [],
    adverTiserPhoneNumber: "",
    gift: "",
    bottomSheet: null,
  },
  action
) => {
  switch (action.type) {
    case FETCH_CATEGORIES_EF: {
      return {
        ...state,
        categories: action.payload,
      };
    }
    case ADD_FILES_CHANGE_LEVEL_EF: {
      return {
        ...state,
        level: action.payload,
      };
    }
    case SAVE_ID_EF: {
      return {
        ...state,
        id: action.payload,
      };
    }
    case SAVE_HISTORY_NAVIGATION_EF: {
      return {
        ...state,
        history: state.history.concat(action.payload),
      };
    }
    case POP_HISTORY_NAVIGATION_EF: {
      return {
        ...state,
        history: state.history.filter(
          (item, index) => index !== state.history.length - 1
        ),
      };
    }
    case CLEAR_HISTORY_NAVIGATION_EF: {
      return {
        ...state,
        history: [],
      };
    }
    case SAVE_CATEGORY_EF: {
      return {
        ...state,
        category: action.payload,
      };
    }
    case SAVE_ADDRESS_EF: {
      return {
        ...state,
        address: action.payload,
      };
    }
    case SAVE_LIST_SPEC_EF: {
      const elementsIndex = state.listSpecs.findIndex(
        (element) => element.id === action.payload.id
      );
      console.log("here!", elementsIndex);

      return {
        ...state,
        listSpecs:
          elementsIndex === -1
            ? state.listSpecs.concat(action.payload)
            : state.listSpecs.map((lsItem, index) => {
                if (index === elementsIndex) {
                  return { id: lsItem.id, value: action.payload.value };
                } else {
                  return lsItem;
                }
              }),
      };
    }
    case SAVE_NUMBER_SPEC_EF: {
      const elementsIndex = state.numberSpecs.findIndex(
        (element) => element.id === action.payload.id
      );
      return {
        ...state,
        numberSpecs:
          elementsIndex === -1
            ? state.numberSpecs.concat(action.payload)
            : state.numberSpecs.map((lsItem, index) => {
                if (index === elementsIndex) {
                  return { id: lsItem.id, value: action.payload.value };
                } else {
                  return lsItem;
                }
              }),
      };
    }
    case CLEAR_NUMBER_SPEC_EF: {
      return {
        ...state,
        numberSpecs: [],
      };
    }
    case SAVE_CITY_EF: {
      return {
        ...state,
        cityId: action.payload,
      };
    }
    case SAVE_LOCATION_EF: {
      return {
        ...state,
        latitude: action.payload.latitude,
        longitude: action.payload.longitude,
      };
    }
    case SAVE_ADAPTIVE_EF: {
      return {
        ...state,
        adaptive: action.payload,
      };
    }
    case SAVE_TITLE_EF: {
      return {
        ...state,
        title: action.payload,
      };
    }
    case SAVE_DESC_EF: {
      return {
        ...state,
        desc: action.payload,
      };
    }
    case SAVE_IMAGES: {
      return {
        ...state,
        images: action.payload,
      };
    }
    case ADD_IMAGE_TYPE: {
      return {
        ...state,
        addImages: state.addImages.concat(action.payload),
      };
    }
    case EDIT_IMAGE_IDS_TYPE: {
      return {
        ...state,
        editFileImagesId: state.editFileImagesId.concat(action.payload),
      };
    }
    case EDIT_IMAGE_TYPE: {
      return {
        ...state,
        editFileImages: state.editFileImages.concat(action.payload),
      };
    }
    case DELETE_IMAGE_TYPE: {
      return {
        ...state,
        deleteImages: state.deleteImages.concat(action.payload),
      };
    }
    case CANCEL_DELETE_IMAGE_TYPE: {
      return {
        ...state,
        deleteImages: state.deleteImages.filter((di) => di !== action.payload),
      };
    }
    case CANCEL_ADD_IMAGE_TYPE: {
      return {
        ...state,
        addImages: state.addImages.filter((di) => di.idx !== action.payload),
      };
    }
    case CANCEL_EDIT_IMAGE_TYPE: {
      const elementsIndex = state.editFileImagesId.findIndex(
        (element) => element === action.payload
      );
      return {
        ...state,
        editFileImages: state.editFileImages.filter(
          (_, index) => index !== elementsIndex
        ),
        editFileImagesId: state.editFileImagesId.filter(
          (item) => item !== action.payload
        ),
      };
    }
    case SAVE_URL_EF: {
      return {
        ...state,
        url: action.payload,
      };
    }
    case ORIGINAL_PLANS: {
      return {
        ...state,
        originPlans: action.payload,
      };
    }
    case ADD_PLAN_EF: {
      return {
        ...state,
        plans: state.plans.concat(action.payload),
      };
    }
    case EDIT_PLAN_EF: {
      return {
        ...state,
        plans: state.plans.map((mp) => {
          if (mp.id === action.payload.id) {
            return { ...mp, type: action.payload.type };
          } else {
            return mp;
          }
        }),
      };
    }
    case REMOVE_PLAN_EF: {
      return {
        ...state,
        plans: state.plans.filter((pln) => pln.id !== action.payload),
      };
    }
    case PLAN_ADD_IMAGE: {
      return {
        ...state,
        planAddImages: state.planAddImages.concat(action.payload),
      };
    }
    case PLAN_ADD_TITLE: {
      return {
        ...state,
        planAddTitle: state.planAddTitle.concat(action.payload),
      };
    }
    case PLAN_DELETE: {
      return {
        ...state,
        planDeleteImages: state.planDeleteImages.concat(action.payload),
      };
    }
    case PLAN_DELETE_CANCEL: {
      return {
        ...state,
        planDeleteImages: state.planDeleteImages.filter(
          (iy) => iy !== action.payload
        ),
      };
    }
    case PLAN_ADD_IMAGE_CANCEL: {
      return {
        ...state,
        planAddImages: state.planAddImages.filter(
          (it) => it.name !== action.payload
        ),
      };
    }
    case PLAN_ADD_TITLE_CANCEL: {
      return {
        ...state,
        planAddTitle: state.planAddTitle.filter((ir) => ir !== action.payload),
      };
    }
    case CLEAR_LIST_SPEC_EF: {
      return {
        ...state,
        listSpecs: [],
      };
    }
    case SET_ADVER_TISER_PHONE_EF: {
      return {
        ...state,
        adverTiserPhoneNumber: action.payload,
      };
    }
    case SAVE_GIFT_FILE_EF: {
      return {
        ...state,
        gift: action.payload,
      };
    }
    case SET_BOTTOM_SHEET_EF: {
      return {
        ...state,
        bottomSheet: action.payload,
      };
    }
    case CLEAR_FILE_INFO_EF: {
      return {
        ...state,
        categories: [],
        level: null,
        id: null,
        history: [],
        category: null,
        listSpecs: [],
        cityId: null,
        longitude: null,
        latitude: null,
        numberSpecs: [],
        adaptive: false,
        plans: [],
        title: "",
        desc: "",
        url: "",
        images: [],
        addImages: [],
        editFileImages: [],
        editFileImagesId: [],
        deleteImages: [],
        planAddImages: [],
        planDeleteImages: [],
        planAddTitle: [],
        adverTiserPhoneNumber: "",
        gift: "",
        bottomSheet: null,
      };
    }
    default:
      return state;
  }
};
