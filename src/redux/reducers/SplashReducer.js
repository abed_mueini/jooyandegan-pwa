import {
  CLEAR_NOTIF_BADGE,
  FETCH_SPLASH_DATA,
  SET_INFO_GATE_WAY,
  STORE_PROFILE,
  UPDATE_USER_WALLET,
} from "../Types";

export const splashReducer = (
  state = { data: null, gateWay: null, wallet: null },
  action
) => {
  switch (action.type) {
    case FETCH_SPLASH_DATA: {
      return {
        ...state,
        data: action.payload,
      };
    }
    case CLEAR_NOTIF_BADGE: {
      return {
        ...state,
        data: { ...state.data, countNotification: 0 },
      };
    }
    case STORE_PROFILE: {
      return {
        ...state,
        data: { ...state.data, user: action.payload },
      };
    }
    case SET_INFO_GATE_WAY: {
      return {
        ...state,
        gateWay: action.payload,
      };
    }
    case UPDATE_USER_WALLET: {
      return {
        ...state,
        wallet: action.payload,
      };
    }
    default:
      return state;
  }
};
