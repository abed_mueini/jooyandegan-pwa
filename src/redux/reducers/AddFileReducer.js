import {
  ADD_FILES_CHANGE_LEVEL,
  CLEAR_HISTORY_NAVIGATION,
  FETCH_CATEGORIES,
  POP_HISTORY_NAVIGATION,
  SAVE_CATEGORY,
  SAVE_CITY,
  SAVE_LOCATION,
  SAVE_ADAPTIVE,
  SAVE_HISTORY_NAVIGATION,
  SAVE_LIST_SPEC,
  SAVE_NUMBER_SPEC,
  CLEAR_FILE_INFO,
  SAVE_ADDRESS,
  ADD_PLAN,
  REMOVE_PLAN,
  SAVE_TITLE,
  SAVE_DESC,
  SAVE_URL,
  SAVE_IMAGE1,
  SAVE_IMAGE6,
  SAVE_IMAGE5,
  SAVE_IMAGE4,
  SAVE_IMAGE3,
  SAVE_IMAGE2,
  SAVE_OWNER_PHONE,
  SAVE_CELL_PHONE,
  SAVE_GIFT_FILE,
  ADVER_TISER_PHONE_NUMBER,
  SAVE_ADVER_PRICE,
  SET_BOTTOM_SHEET,
} from "../Types";

export const addFileReducer = (
  state = {
    categories: [],
    level: null,
    history: [],
    category: null,
    listSpecs: [],
    cityId: null,
    numberSpecs: [],
    adaptive: false,
    title: "",
    desc: "",
    image1: null,
    image2: null,
    image3: null,
    image4: null,
    image5: null,
    image6: null,
    advertiserPhoneNumber: "",
    gift: null,
    adverPrice: null,
    bottomSheet: null,
  },
  action
) => {
  switch (action.type) {
    case FETCH_CATEGORIES: {
      return {
        ...state,
        categories: action.payload,
      };
    }
    case ADD_FILES_CHANGE_LEVEL: {
      return {
        ...state,
        level: action.payload,
      };
    }
    case SAVE_HISTORY_NAVIGATION: {
      return {
        ...state,
        history: state.history.concat(action.payload),
      };
    }
    case POP_HISTORY_NAVIGATION: {
      return {
        ...state,
        history: state.history.filter(
          (item, index) => index !== state.history.length - 1
        ),
      };
    }
    case CLEAR_HISTORY_NAVIGATION: {
      return {
        ...state,
        history: [],
      };
    }

    case SAVE_CATEGORY: {
      return {
        ...state,
        category: action.payload,
        numberSpecs: [],
        listSpecs: [],
      };
    }
    case SAVE_LIST_SPEC: {
      const elementsIndex = state.listSpecs.findIndex(
        (element) => element.id === action.payload.id
      );
      return {
        ...state,
        listSpecs:
          elementsIndex === -1
            ? state.listSpecs.concat(action.payload)
            : state.listSpecs.map((lsItem, index) => {
                if (index === elementsIndex) {
                  return { id: lsItem.id, value: action.payload.value };
                } else {
                  return lsItem;
                }
              }),
      };
    }
    case SAVE_NUMBER_SPEC: {
      const elementsIndex = state.numberSpecs.findIndex(
        (element) => element.id === action.payload.id
      );
      return {
        ...state,
        numberSpecs:
          elementsIndex === -1
            ? state.numberSpecs.concat(action.payload)
            : state.numberSpecs.map((lsItem, index) => {
                if (index === elementsIndex) {
                  return { id: lsItem.id, value: action.payload.value };
                } else {
                  return lsItem;
                }
              }),
      };
    }
    case SAVE_CITY: {
      return {
        ...state,
        cityId: action.payload,
      };
    }

    case SAVE_ADAPTIVE: {
      return {
        ...state,
        adaptive: action.payload,
      };
    }
    case SAVE_TITLE: {
      return {
        ...state,
        title: action.payload,
      };
    }
    case SAVE_DESC: {
      return {
        ...state,
        desc: action.payload,
      };
    }
    case SAVE_IMAGE1: {
      return {
        ...state,
        image1: action.payload,
      };
    }
    case SAVE_IMAGE2: {
      return {
        ...state,
        image2: action.payload,
      };
    }
    case SAVE_IMAGE3: {
      return {
        ...state,
        image3: action.payload,
      };
    }
    case SAVE_IMAGE4: {
      return {
        ...state,
        image4: action.payload,
      };
    }
    case SAVE_IMAGE5: {
      return {
        ...state,
        image5: action.payload,
      };
    }
    case SAVE_IMAGE6: {
      return {
        ...state,
        image6: action.payload,
      };
    }

    case ADVER_TISER_PHONE_NUMBER: {
      return {
        ...state,
        advertiserPhoneNumber: action.payload,
      };
    }

    case SAVE_GIFT_FILE: {
      return {
        ...state,
        gift: action.payload,
      };
    }
    case SAVE_ADVER_PRICE: {
      return {
        ...state,
        priceAdver: action.payload,
      };
    }
    case SET_BOTTOM_SHEET: {
      return {
        ...state,
        bottomSheet: action.payload,
      };
    }
    case CLEAR_FILE_INFO: {
      return {
        ...state,
        level: null,
        history: [],
        category: null,
        listSpecs: [],
        address: "",
        cityId: null,
        longitude: null,
        latitude: null,
        numberSpecs: [],
        adaptive: false,
        plans: [],
        title: "",
        desc: "",
        url: "",
        image1: null,
        image2: null,
        image3: null,
        image4: null,
        image5: null,
        image6: null,
        advertiserPhoneNumber: "",
        gift: null,
        priceAdver: null,
        bottomSheet: null,
      };
    }
    default:
      return state;
  }
};
