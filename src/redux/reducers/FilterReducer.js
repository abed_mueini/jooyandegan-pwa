import {
  Add_FILTER,
  CHANGE_LEVEL_FILTER,
  FETCH_FILTERS,
  PREPARE_DATA_FOR_FILTER,
  REMOVE_FILTER,
  REMOVE_FILTER_RANGE,
  SAVE_FILTER_NAVIGATION_HISTORY,
  SET_FILTER_CATEGORY,
  POP_FILTER_NAVIGATION_HISTORY,
  CLEAR_FILTER_NAVIGATION_HISTORY,
  SET_QUERY,
  SET_PARENT_CAT_ID,
  NEED_REFRESH,
  SET_CITY,
  CLEAR_FILTER,
  SET_HAS_IMAGE,
  SET_HAS_PLAN,
  SET_HAS_TOUR,
  SET_CAT_ID,
} from "../Types";

export const filterReducer = (
  state = {
    data: [],
    params: [],
    query: "",
    history: [],
    parentCatId: null,
    needRefresh: false,
    level: null,
    category: null,
    city: null,
    hasImage: false,
    hasPlan: false,
    hasTour: false,
    ListSpecifications: [],
    NumberSpecifications: [],
    catId: -1,
  },
  action
) => {
  switch (action.type) {
    case FETCH_FILTERS: {
      return {
        ...state,
        data: action.payload,
      };
    }
    case Add_FILTER: {
      return {
        ...state,
        params: state.params.concat(action.payload),
      };
    }
    case CLEAR_FILTER: {
      return {
        ...state,
        params: [],
        hasImage: false,
        hasPlan: false,
        hasTour: false,
      };
    }
    case SET_CITY: {
      return {
        ...state,
        city: action.payload,
      };
    }
    case NEED_REFRESH: {
      return {
        ...state,
        needRefresh: action.payload,
      };
    }
    case SET_QUERY: {
      return {
        ...state,
        query: action.payload,
      };
    }
    case SET_CAT_ID: {
      return {
        ...state,
        catId: action.payload,
      };
    }
    case REMOVE_FILTER: {
      return {
        ...state,
        params: state.params.filter((item) => item.id !== action.payload),
        ListSpecifications: state.ListSpecifications.filter(
          (ls) => ls !== action.payload2
        ),
      };
    }
    case REMOVE_FILTER_RANGE: {
      return {
        ...state,
        params: state.params.filter(
          (item) =>
            item.id !== action.payload.id ||
            item.rangeType !== action.payload.type
        ),
      };
    }
    case SET_FILTER_CATEGORY: {
      return {
        ...state,
        category: action.payload,
      };
    }
    case PREPARE_DATA_FOR_FILTER: {
      return {
        ...state,
        ListSpecifications: action.payload.ListSpecifications
          ? action.payload.ListSpecifications
          : [],
        NumberSpecifications: action.payload.NumberSpecifications
          ? action.payload.NumberSpecifications
          : [],
      };
    }
    case CHANGE_LEVEL_FILTER: {
      return {
        ...state,
        level: action.payload,
      };
    }
    case SAVE_FILTER_NAVIGATION_HISTORY: {
      return {
        ...state,
        history: state.history.concat(action.payload),
      };
    }
    case POP_FILTER_NAVIGATION_HISTORY: {
      return {
        ...state,
        history: state.history.filter(
          (item, index) => index !== state.history.length - 1
        ),
      };
    }
    case CLEAR_FILTER_NAVIGATION_HISTORY: {
      return {
        ...state,
        history: [],
      };
    }
    case SET_PARENT_CAT_ID: {
      return {
        ...state,
        parentCatId: action.payload,
      };
    }
    case SET_HAS_IMAGE: {
      return {
        ...state,
        hasImage: action.payload,
      };
    }
    case SET_HAS_PLAN: {
      return {
        ...state,
        hasPlan: action.payload,
      };
    }
    case SET_HAS_TOUR: {
      return {
        ...state,
        hasTour: action.payload,
      };
    }
    default:
      return state;
  }
};
