import {
  FETCH_SPLASH_DATA,
  STORE_PROFILE,
  CLEAR_NOTIF_BADGE,
  SET_INFO_GATE_WAY,
  UPDATE_USER_WALLET,
} from '../Types';

export const fetchSplashData = payload => {
  return dispatch => {
    dispatch({
      type: FETCH_SPLASH_DATA,
      payload: payload,
    });
  };
};

export const clearNotifBadge = () => {
  return dispatch => {
    dispatch({
      type: CLEAR_NOTIF_BADGE,
    });
  };
};

export const storeProfile = payload => {
  return dispatch => {
    dispatch({
      type: STORE_PROFILE,
      payload: payload,
    });
  };
};
export const setInfoGateWay = payload => {
  return dispatch => {
    dispatch({
      type: SET_INFO_GATE_WAY,
      payload: payload,
    });
  };
};
export const saveUserWallet = payload => {
  return dispatch => {
    dispatch({
      type: UPDATE_USER_WALLET,
      payload: payload,
    });
  };
};