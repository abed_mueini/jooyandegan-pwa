import {
  FETCH_CATEGORIES_EF,
  ADD_FILES_CHANGE_LEVEL_EF,
  SAVE_HISTORY_NAVIGATION_EF,
  POP_HISTORY_NAVIGATION_EF,
  CLEAR_HISTORY_NAVIGATION_EF,
  SAVE_CATEGORY_EF,
  SAVE_LIST_SPEC_EF,
  SAVE_LOCATION_EF,
  SAVE_NUMBER_SPEC_EF,
  SAVE_CITY_EF,
  SAVE_ADAPTIVE_EF,
  CLEAR_FILE_INFO_EF,
  ADD_PLAN_EF,
  REMOVE_PLAN_EF,
  SAVE_TITLE_EF,
  SAVE_ADDRESS_EF,
  SAVE_DESC_EF,
  SAVE_URL_EF,
  SAVE_IMAGES,
  EDIT_IMAGE_IDS_TYPE,
  EDIT_IMAGE_TYPE,
  ADD_IMAGE_TYPE,
  DELETE_IMAGE_TYPE,
  SAVE_ID_EF,
  PLAN_ADD_TITLE_CANCEL,
  PLAN_ADD_IMAGE_CANCEL,
  CLEAR_NUMBER_SPEC_EF,
  CANCEL_DELETE_IMAGE_TYPE,
  CANCEL_EDIT_IMAGE_TYPE,
  CANCEL_ADD_IMAGE_TYPE,
  PLAN_ADD_IMAGE,
  PLAN_ADD_TITLE,
  PLAN_DELETE,
  ORIGINAL_PLANS,
  EDIT_PLAN_EF,
  PLAN_DELETE_CANCEL,
  CLEAR_LIST_SPEC_EF,
  SAVE_OWNER_PHONE_EF,
  SAVE_CELL_PHONE_EF,
  ADVER_TISER_PHONE_NUMBER_EF,
  SAVE_GIFT_FILE_EF,
  SET_ADVER_TISER_PHONE_EF,
  SET_BOTTOM_SHEET_EF,
} from "../Types";

export const fetchCategoies = (payload) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_CATEGORIES_EF,
      payload: payload,
    });
  };
};
export const saveId = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_ID_EF,
      payload: payload,
    });
  };
};
export const changeLevel = (payload) => {
  return (dispatch) => {
    dispatch({
      type: ADD_FILES_CHANGE_LEVEL_EF,
      payload: payload,
    });
  };
};
export const pushNavigationHistory = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_HISTORY_NAVIGATION_EF,
      payload: payload,
    });
  };
};
export const popNavigationHistory = () => {
  return (dispatch) => {
    dispatch({
      type: POP_HISTORY_NAVIGATION_EF,
    });
  };
};
export const clearHistory = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_HISTORY_NAVIGATION_EF,
    });
  };
};
export const saveCategory = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_CATEGORY_EF,
      payload: payload,
    });
  };
};
export const saveListSpec = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_LIST_SPEC_EF,
      payload: payload,
    });
  };
};
export const saveNumberSpecEf = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_NUMBER_SPEC_EF,
      payload: payload,
    });
  };
};
export const clearNumberSpecEf = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_NUMBER_SPEC_EF,
    });
  };
};
export const clearListSpecEf = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_LIST_SPEC_EF,
    });
  };
};
export const saveCity = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_CITY_EF,
      payload: payload,
    });
  };
};
export const saveAddress = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_ADDRESS_EF,
      payload: payload,
    });
  };
};
export const saveLocationEf = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_LOCATION_EF,
      payload: payload,
    });
  };
};
export const saveAdaptive = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_ADAPTIVE_EF,
      payload: payload,
    });
  };
};
export const clearFileInfo = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_FILE_INFO_EF,
    });
  };
};
export const addPlan = (payload) => {
  return (dispatch) => {
    dispatch({
      type: ADD_PLAN_EF,
      payload: payload,
    });
  };
};
export const editPlan = (payload) => {
  return (dispatch) => {
    dispatch({
      type: EDIT_PLAN_EF,
      payload: payload,
    });
  };
};
export const originPlans = (payload) => {
  return (dispatch) => {
    dispatch({
      type: ORIGINAL_PLANS,
      payload: payload,
    });
  };
};
export const saveTitle = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_TITLE_EF,
      payload: payload,
    });
  };
};
export const saveDesc = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_DESC_EF,
      payload: payload,
    });
  };
};
export const saveUrl = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_URL_EF,
      payload: payload,
    });
  };
};
export const saveImages = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_IMAGES,
      payload: payload,
    });
  };
};
export const addImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: ADD_IMAGE_TYPE,
      payload: payload,
    });
  };
};
export const EditImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: EDIT_IMAGE_TYPE,
      payload: payload,
    });
  };
};
export const EditImageIds = (payload) => {
  return (dispatch) => {
    dispatch({
      type: EDIT_IMAGE_IDS_TYPE,
      payload: payload,
    });
  };
};
export const saveOwnerPhone = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_OWNER_PHONE_EF,
      payload: payload,
    });
  };
};
export const saveCellPhone = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_CELL_PHONE_EF,
      payload: payload,
    });
  };
};
export const deleteImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_IMAGE_TYPE,
      payload: payload,
    });
  };
};
export const cancelDeleteImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: CANCEL_DELETE_IMAGE_TYPE,
      payload: payload,
    });
  };
};
export const cancelEditImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: CANCEL_EDIT_IMAGE_TYPE,
      payload: payload,
    });
  };
};
export const cancelAddImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: CANCEL_ADD_IMAGE_TYPE,
      payload: payload,
    });
  };
};
export const planAddImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: PLAN_ADD_IMAGE,
      payload: payload,
    });
  };
};
export const planAddTitle = (payload) => {
  return (dispatch) => {
    dispatch({
      type: PLAN_ADD_TITLE,
      payload: payload,
    });
  };
};
export const planAddImageCancel = (payload) => {
  return (dispatch) => {
    dispatch({
      type: PLAN_ADD_IMAGE_CANCEL,
      payload: payload,
    });
  };
};
export const planAddTitleCancel = (payload) => {
  return (dispatch) => {
    dispatch({
      type: PLAN_ADD_TITLE_CANCEL,
      payload: payload,
    });
  };
};
export const planDelete = (payload) => {
  return (dispatch) => {
    dispatch({
      type: PLAN_DELETE,
      payload: payload,
    });
  };
};
export const planDeleteCancel = (payload) => {
  return (dispatch) => {
    dispatch({
      type: PLAN_DELETE_CANCEL,
      payload: payload,
    });
  };
};
export const removePlan = (payload) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_PLAN_EF,
      payload: payload,
    });
  };
};
export const saveGiftFileEf = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_GIFT_FILE_EF,
      payload: payload,
    });
  };
};
export const saveAdverTiserPhoneNumberEf = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_ADVER_TISER_PHONE_EF,
      payload: payload,
    });
  };
};
export const setBottomSheetEf = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_BOTTOM_SHEET_EF,
      payload: payload,
    });
  };
};
