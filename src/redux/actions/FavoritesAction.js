import {
  ADD_TO_FAVORITES,
  REMOVE_FROM_FAVORITES,
  STORE_FAVORITES,
} from '../Types';

export const storeFavorites = (payload) => {
  return (dispatch) => {
    dispatch({
      type: STORE_FAVORITES,
      payload: payload,
    });
  };
};
export const addToFavorites = (payload) => {
  return (dispatch) => {
    dispatch({
      type: ADD_TO_FAVORITES,
      payload: payload,
    });
  };
};
export const removeFromFavorites = (payload) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_FROM_FAVORITES,
      payload: payload,
    });
  };
};
