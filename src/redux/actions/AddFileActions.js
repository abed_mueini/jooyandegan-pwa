import {
  FETCH_CATEGORIES,
  ADD_FILES_CHANGE_LEVEL,
  SAVE_HISTORY_NAVIGATION,
  POP_HISTORY_NAVIGATION,
  CLEAR_HISTORY_NAVIGATION,
  SAVE_CATEGORY,
  SAVE_LIST_SPEC,
  SAVE_NUMBER_SPEC,
  SAVE_CITY,
  SAVE_ADAPTIVE,
  CLEAR_FILE_INFO,
  SAVE_TITLE,
  SAVE_DESC,
  SAVE_IMAGE1,
  SAVE_IMAGE2,
  SAVE_IMAGE3,
  SAVE_IMAGE4,
  SAVE_IMAGE5,
  SAVE_IMAGE6,
  SAVE_GIFT_FILE,
  ADVER_TISER_PHONE_NUMBER,
  SAVE_ADVER_PRICE,
  SET_BOTTOM_SHEET,
} from "../Types";

export const fetchCategoies = (payload) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_CATEGORIES,
      payload: payload,
    });
  };
};
export const changeLevel = (payload) => {
  return (dispatch) => {
    dispatch({
      type: ADD_FILES_CHANGE_LEVEL,
      payload: payload,
    });
  };
};
export const pushNavigationHistory = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_HISTORY_NAVIGATION,
      payload: payload,
    });
  };
};
export const popNavigationHistory = () => {
  return (dispatch) => {
    dispatch({
      type: POP_HISTORY_NAVIGATION,
    });
  };
};
export const clearHistory = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_HISTORY_NAVIGATION,
    });
  };
};
export const saveCategory = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_CATEGORY,
      payload: payload,
    });
  };
};
export const saveListSpec = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_LIST_SPEC,
      payload: payload,
    });
  };
};
export const saveNumberSpec = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_NUMBER_SPEC,
      payload: payload,
    });
  };
};
export const saveCity = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_CITY,
      payload: payload,
    });
  };
};

export const saveAdaptive = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_ADAPTIVE,
      payload: payload,
    });
  };
};
export const clearFileInfo = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_FILE_INFO,
    });
  };
};

export const saveTitle = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_TITLE,
      payload: payload,
    });
  };
};

export const saveDesc = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_DESC,
      payload: payload,
    });
  };
};

export const saveImage1 = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_IMAGE1,
      payload: payload,
    });
  };
};
export const saveImage2 = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_IMAGE2,
      payload: payload,
    });
  };
};
export const saveImage3 = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_IMAGE3,
      payload: payload,
    });
  };
};
export const saveImage4 = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_IMAGE4,
      payload: payload,
    });
  };
};
export const saveImage5 = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_IMAGE5,
      payload: payload,
    });
  };
};
export const saveImage6 = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_IMAGE6,
      payload: payload,
    });
  };
};
export const saveAdverTiserPhone = (payload) => {
  return (dispatch) => {
    dispatch({
      type: ADVER_TISER_PHONE_NUMBER,
      payload: payload,
    });
  };
};
export const saveGiftFile = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_GIFT_FILE,
      payload: payload,
    });
  };
};
export const saveAdverPrice = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_ADVER_PRICE,
      payload: payload,
    });
  };
};
export const setBottomSheet = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_BOTTOM_SHEET,
      payload: payload,
    });
  };
};
