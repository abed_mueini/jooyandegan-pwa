import {
  SAVE_MAIN_CATEGORIES,
  SAVE_HOME_MAIN_CATEGORIES_CAT,
  ADD_FILES_CHANGE_LEVEL_CAT,
  ADD_HOME_FILES_CHANGE_LEVEL_CAT,
  SAVE_HOME_HISTORY_NAVIGATION_CAT,
  POP_HOME_HISTORY_NAVIGATION_CAT,
  POP_HISTORY_NAVIGATION_CAT,
  CLEAR_HISTORY_NAVIGATION_CAT,
  CLEAR_HOME_HISTORY_NAVIGATION_CAT,
  SET_CATEGORY_LIST_CAT,
  SET_HOME_CATEGORY_LIST_CAT,
  SAVE_HISTORY_NAVIGATION_CAT,
} from '../Types';

export const saveMainCategories = payload => {
  return dispatch => {
    dispatch({
      type: SAVE_MAIN_CATEGORIES,
      payload: payload,
    });
  };
};
export const saveHomeMainCategories = payload => {
  return dispatch => {
    dispatch({
      type: SAVE_HOME_MAIN_CATEGORIES_CAT,
      payload: payload,
    });
  };
};
export const changeLevel = payload => {
  return dispatch => {
    dispatch({
      type: ADD_FILES_CHANGE_LEVEL_CAT,
      payload: payload,
    });
  };
};
export const changeHomeLevel = payload => {
  return dispatch => {
    dispatch({
      type: ADD_HOME_FILES_CHANGE_LEVEL_CAT,
      payload: payload,
    });
  };
};

export const pushHomeNavigationHistory = payload => {
  return dispatch => {
    dispatch({
      type: SAVE_HOME_HISTORY_NAVIGATION_CAT,
      payload: payload,
    });
  };
};
export const popHomeNavigationHistory = () => {
  return dispatch => {
    dispatch({
      type: POP_HOME_HISTORY_NAVIGATION_CAT,
    });
  };
};
export const popNavigationHistory = () => {
  return dispatch => {
    dispatch({
      type: POP_HISTORY_NAVIGATION_CAT,
    });
  };
};
export const clearHistory = () => {
  return dispatch => {
    dispatch({
      type: CLEAR_HISTORY_NAVIGATION_CAT,
    });
  };
};
export const clearHomeHistory = () => {
  return dispatch => {
    dispatch({
      type: CLEAR_HOME_HISTORY_NAVIGATION_CAT,
    });
  };
};
export const setCategoryList = payload => {
  return dispatch => {
    dispatch({
      type: SET_CATEGORY_LIST_CAT,
      payload: payload,
    });
  };
};
export const setHomeCategoryList = payload => {
  return dispatch => {
    dispatch({
      type: SET_HOME_CATEGORY_LIST_CAT,
      payload: payload,
    });
  };
};
export const pushNavigationHistory = payload => {
  return dispatch => {
    dispatch({
      type: SAVE_HISTORY_NAVIGATION_CAT,
      payload: payload,
    });
  };
};
