import {
  Add_FILTER,
  CHANGE_LEVEL_FILTER,
  CLEAR_FILTER_NAVIGATION_HISTORY,
  FETCH_FILTERS,
  NEED_REFRESH,
  POP_FILTER_NAVIGATION_HISTORY,
  PREPARE_DATA_FOR_FILTER,
  REMOVE_DATA_FOR_FILTER,
  REMOVE_FILTER,
  REMOVE_FILTER_RANGE,
  SAVE_FILTER_NAVIGATION_HISTORY,
  SET_FILTER_CATEGORY,
  SET_PARENT_CAT_ID,
  SET_QUERY,
  SET_CITY,
  CLEAR_FILTER,
  SET_HAS_IMAGE,
  SET_HAS_PLAN,
  SET_HAS_TOUR,
  SET_CAT_ID,
} from "../Types";

export const fetchFilters = (payload) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_FILTERS,
      payload: payload,
    });
  };
};
export const addFilter = (payload) => {
  return (dispatch) => {
    dispatch({
      type: Add_FILTER,
      payload: payload,
    });
  };
};
export const setCatId = (payload) => {
  return {
    type: SET_CAT_ID,
    payload: payload,
  };
};
export const clearFilter = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_FILTER,
    });
  };
};
export const setCity = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_CITY,
      payload: payload,
    });
  };
};
export const setQuery = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_QUERY,
      payload: payload,
    });
  };
};
export const removeFilter = (payload, payload2) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_FILTER,
      payload: payload,
      payload2: payload2,
    });
  };
};
export const removeFilterRange = (payload) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_FILTER_RANGE,
      payload: payload,
    });
  };
};
export const prepareFilterForData = (payload) => {
  return (dispatch) => {
    dispatch({
      type: PREPARE_DATA_FOR_FILTER,
      payload: payload,
    });
  };
};
export const removeFilterForData = (payload) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_DATA_FOR_FILTER,
      payload: payload,
    });
  };
};
export const needRefresh = (payload) => {
  return (dispatch) => {
    dispatch({
      type: NEED_REFRESH,
      payload: payload,
    });
  };
};
export const setFilterCategory = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_FILTER_CATEGORY,
      payload: payload,
    });
  };
};
export const setParentCatId = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_PARENT_CAT_ID,
      payload: payload,
    });
  };
};
export const pushFilterNavigationHistory = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_FILTER_NAVIGATION_HISTORY,
      payload: payload,
    });
  };
};
export const popFilterNavigationHistory = () => {
  return (dispatch) => {
    dispatch({
      type: POP_FILTER_NAVIGATION_HISTORY,
    });
  };
};
export const clearFilterNavigationHistory = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_FILTER_NAVIGATION_HISTORY,
    });
  };
};
export const filterChangeLevel = (payload) => {
  return (dispatch) => {
    dispatch({
      type: CHANGE_LEVEL_FILTER,
      payload: payload,
    });
  };
};
export const setHasImage = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_HAS_IMAGE,
      payload: payload,
    });
  };
};
export const setHasTour = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_HAS_TOUR,
      payload: payload,
    });
  };
};
export const setHasPlan = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_HAS_PLAN,
      payload: payload,
    });
  };
};
