import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import {splashReducer} from '../reducers/SplashReducer';
import {filterReducer} from '../reducers/FilterReducer';
import {addFileReducer} from '../reducers/AddFileReducer';
import {favoritesReducer} from '../reducers/FavoritesReducer';
import {editFileReducer} from '../reducers/EditFileReducer';
import {categoriesReducer} from '../reducers/CategoriesReducer';

export const reducers = combineReducers({
  splash: splashReducer,
  filter: filterReducer,
  files: addFileReducer,
  editFile: editFileReducer,
  favorites: favoritesReducer,
  categories: categoriesReducer,
});

export function configureStore(initialState = {}) {
  return compose(applyMiddleware(thunk))(createStore)(reducers, initialState);
}

export const store = configureStore();
