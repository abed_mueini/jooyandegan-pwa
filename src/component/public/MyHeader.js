import { IconButton } from "@mui/material";
import { useRouter } from "next/router";
import React from "react";
import { MdArrowBack } from "react-icons/md";
import { FONT_NAME_BOLD } from "../../utils/Constant";
const MyHeader = (props) => {
  const router = useRouter();
  return (
    <div style={props.back ? styles.header1 : styles.header2}>
      {props.back && <div style={{ width: "0%" }}></div>}
      <span style={props.back ? styles.title : styles.titleBold}>
        {props.title}
      </span>
      {props.back && (
        <div style={{ width: "0%" }}>
          <IconButton onClick={() => router.back()}>
            <MdArrowBack color={"#111"} size={21} />
          </IconButton>
        </div>
      )}
    </div>
  );
};
const styles = {
  title: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 18,
    color: "#111",
    textAlign: "center",
    marginRight: 8,
  },
  titleBold: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 18,
    color: "#111",
    textAlign: "center",
  },
  header1: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    width: "100%",
    backgroundColor: "white",
    padding: 15,
    position: "sticky",
    zIndex: 99,
    top: 0,
    boxShadow: "0px 1px 2px #ccc",
    right: 0,
  },
  header2: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    backgroundColor: "white",
    padding: 17,
    position: "sticky",
    zIndex: 99,
    top: 0,
    boxShadow: "0px 1px 2px #ccc",
    right: 0,
  },
};
export default MyHeader;
