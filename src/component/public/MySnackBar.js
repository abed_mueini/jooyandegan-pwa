import { Snackbar } from "@mui/material";
import React from "react";
import { FONT_NAME_BOLD } from "../../utils/Constant";

const styles = {
  anchorOriginTopRight: {
    zIndex: 9999999999999,
  },
  close: {
    padding: 15,
  },
  message: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 13,
  },
};

export default function MySnackBar(props) {
  return (
    <Snackbar
      anchorOrigin={
        props.inModa
          ? {
              vertical: "center",
              horizontal: "center",
            }
          : { vertical: "top", horizontal: "center" }
      }
      open={props.open}
      autoHideDuration={3000}
      action={props.action}
      onClose={props.onClose}
      ContentProps={{
        "aria-describedby": "message-id",
      }}
      message={<span style={styles.message}>{props.message}</span>}
    />
  );
}
