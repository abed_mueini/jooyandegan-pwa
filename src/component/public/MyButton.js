import { Button, CircularProgress } from "@mui/material";
import React from "react";
import { FONT_NAME_BOLD } from "../../utils/Constant";

export default function MyButton(props) {
  return (
    <Button
      variant="contained"
      color="primary"
      style={styles.button}
      onClick={props.onClick}
    >
      {props.isLoading ? (
        <CircularProgress style={styles.loading} size={22} color={"inherit"} />
      ) : (
        <span style={styles.title}>{props.title}</span>
      )}
    </Button>
  );
}
const styles = {
  button: {
    marginTop: 50,
    width: "50%",
  },
  loading: {
    display: "flex",
    color: "white",
  },
  title: {
    fontSize: 14,
    color: "white",
    fontFamily: FONT_NAME_BOLD,
  },
};
