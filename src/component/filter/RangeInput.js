import React from "react";
import { isEmptyObject } from "../../utils/Tools";
import { Button, Stack, Typography } from "@mui/material";
import { AiFillCloseCircle } from "react-icons/ai";
import { FONT_NAME, FONT_NAME_BOLD } from "../../utils/Constant";

const RangeInput = (props) => {
  return (
    <Stack style={styles.container}>
      <Typography style={styles.titleText}>{props.title}</Typography>
      <Button
        onClick={() => {
          if (props.hasDownDelete.flag) {
            props.onDownDelete();
          } else {
            props.downOnPress();
          }
        }}
        style={styles.touchable}
      >
        <Typography style={styles.placeholder}>
          {isEmptyObject(props.hasDownDelete.title)
            ? "از"
            : props.hasDownDelete.title}
        </Typography>
        {props.hasDownDelete.flag && (
          <AiFillCloseCircle name="close" style={styles.deleteIcon} />
        )}
      </Button>
      <Button
        onClick={() => {
          if (props.hasUpDelete.flag) {
            props.onUpDelete();
          } else {
            props.upOnPress();
          }
        }}
        style={styles.touchable}
      >
        <Typography style={styles.placeholder}>
          {isEmptyObject(props.hasUpDelete.title)
            ? "تا"
            : props.hasUpDelete.title}
        </Typography>
        {props.hasUpDelete.flag && (
          <AiFillCloseCircle name="close" style={styles.deleteIcon} />
        )}
      </Button>
    </Stack>
  );
};
const styles = {
  container: {
    flexDirection: "column",
    width: "100%",
    display: "flex",
  },
  titleText: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 13,
    color: "black",
    marginRight: 10,
  },
  touchable: {
    backgroundColor: "white",
    borderRadius: 5,
    width: "100%",
    paddingInline: 5,
    paddingBottom: 3.5,
    paddingTop: 3.5,
    marginTop: 5,
    borderColor: "#eee",
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    border: "1px solid #000",
    display: "flex",
  },
  deleteIcon: {
    fontSize: 20,
    color: "#555",
  },
  placeholder: {
    fontFamily: FONT_NAME,
    fontSize: 13,
    color: "#aaa",
  },
};
export default RangeInput;
