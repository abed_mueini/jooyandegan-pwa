import React from "react";
import { FONT_NAME, FONT_NAME_BOLD } from "../../utils/Constant";
import { Button, Stack, Typography } from "@mui/material";
import { AiFillCloseCircle } from "react-icons/ai";
import { isEmptyObject } from "../../utils/Tools";
const ListInput = (props) => {
  return (
    <Stack style={styles.container}>
      <Typography  m={2} variant="h1" style={styles.titleText}>{props.title}</Typography>
      <Stack className="rowACJC">
      <Button
        onClick={() => {
          if (props.hasDelete.flag) {
            props.onDelete();
          } else {
            props.onClick();
          }
        }}
        style={styles.touchable}
      >
        <Typography style={styles.placeholder}>
          {isEmptyObject(props.hasDelete.title)
            ? "انتخاب کنید"
            : props.hasDelete.title}
        </Typography>
        {props.hasDelete.flag && (
          <AiFillCloseCircle style={styles.deleteIcon} />
        )}
      </Button>
      </Stack>
    </Stack>
  );
};
const styles = {
  container: {
    flexDirection: "column",
    width: "100%",
    display: "flex",
  },
  titleText: {
    fontFamily: FONT_NAME_BOLD,
    fontSize: 15,
    color: "black",
    marginRight: 10,
  },
  deleteIcon: {
    fontSize: 20,
    color: "#555",
  },
  touchable: {
    backgroundColor: "white",
    borderRadius: 5,
    width: "95%",
    paddingInline: 5,
    paddingBottom: 7,
    paddingTop: 7,
    marginTop: 5,
    borderColor: "#eee",
    flexDirection: "row",
    justifyContent: "space-between",
    border: "1px solid #ccc",
    display: "flex",
  },
  placeholder: {
    fontFamily: FONT_NAME,
    fontSize: 13,
    color: "#aaa",
  },
};
export default ListInput;
