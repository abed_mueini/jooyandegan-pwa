/* eslint-disable @next/next/no-img-element */
import React, { memo } from "react";
import classNames from "classnames";
import SwipeableBottomSheet from "@sergeymyssak/swipeable-bottom-sheet";
import "@sergeymyssak/swipeable-bottom-sheet/lib/min.css";
import { useRouter } from "next/router";
import { Stack, Typography, Divider, Button } from "@mui/material";
import { useDispatch } from "react-redux";
import { storeProfile } from "../../redux/actions/SplashActions";

export function HeaderSheet() {
  return (
    <Stack mt={1} className="rowACJC">
      <Stack style={styles.headerSheet} />
    </Stack>
  );
}

const InfoContactBottomSheet = ({
  open,
  disableSwipe = false,
  onClose,
  containerClassName,
  bodyClassName,
}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  return (
    <SwipeableBottomSheet
      isOpen={open}
      onChange={() => onClose(false)}
      swipeableViewsProps={{ disabled: disableSwipe }}
      containerClassName={classNames("custom-bottom-sheet", containerClassName)}
      bodyClassName={classNames("custom-bottom-sheet__body", bodyClassName)}
    >
      {HeaderSheet()}
      <Stack style={styles.addToBasketContainer}>
        <Stack className={"colJCAC"}>
          <img src="/images/Logout.png" style={styles.image} />
          <Typography variant="h1" marginTop={2}>
            خروج از حساب کاربری
          </Typography>
        </Stack>
        <Divider sx={{ marginTop: 3 }} />
        <Stack className="rowACJC">
          <Typography marginTop={1.5} color="#555" variant="body1">
            شما در حال خروج از حسابتان هستید ! آیا مایل به خروج هستید؟
          </Typography>
        </Stack>
        <Stack className={"rowACJSB"} mt={3}>
          <Button
            style={{ width: "45%", marginInline: 10, padding: 10 }}
            variant="contained"
            color={"primary"}
            className="rowACJC"
            onClick={() => {
              dispatch(storeProfile(null));
              localStorage.removeItem("token");
              localStorag.removeItem("user");
              onClose();
            }}
          >
            <Typography color={"white"} variant="h1">
              بله
            </Typography>
          </Button>
          <Button
            style={{ width: "45%", marginInline: 10, padding: 10 }}
            variant="contained"
            color={"secondary"}
            className="rowACJC"
            onClick={() => onClose()}
          >
            <Typography color={"white"} variant="h1">
              خیر
            </Typography>
          </Button>
        </Stack>
      </Stack>
    </SwipeableBottomSheet>
  );
};
const styles = {
  addToBasketContainer: {
    width: "100%",
    height: 260,
    backgroundColor: "white",
  },
  headerSheet: {
    width: 50,
    height: 3,
    backgroundColor: "#ccc",
    borderRadius: 10,
    padding: 2,
  },
  image: {
    width: 25,
    height: 25,
    objectFit: "contain",
    marginTop: 15,
  },
};
export default memo(InfoContactBottomSheet);
