/* eslint-disable @next/next/no-img-element */
import React, { memo } from "react";
import classNames from "classnames";
import SwipeableBottomSheet from "@sergeymyssak/swipeable-bottom-sheet";
import "@sergeymyssak/swipeable-bottom-sheet/lib/min.css";
import { useRouter } from "next/router";
import { Stack, Typography, Divider, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { storeProfile } from "../../redux/actions/SplashActions";
import { COLOR_PRIMARY } from "../../theme/theme";
import { FONT_NAME } from "../../utils/Constant";
import {
  saveListSpec,
  setBottomSheet,
} from "../../redux/actions/AddFileActions";

export function HeaderSheet() {
  return (
    <Stack mt={1} className="rowACJC">
      <Stack style={styles.headerSheet} />
    </Stack>
  );
}

const ListSpicificationSheet = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const state = useSelector((ss) => ss.files?.bottomSheet);
  console.log("state", state);
  return (
    <SwipeableBottomSheet
      isOpen={props.open}
      onChange={() => props.onClose(false)}
      swipeableViewsProps={{ disabled: props.disableSwipe }}
      containerClassName={classNames(
        "custom-bottom-sheet",
        props.containerClassName
      )}
      bodyClassName={classNames(
        "custom-bottom-sheet__body",
        props.bodyClassName
      )}
    >
      {HeaderSheet()}
      <Stack style={styles.addToBasketContainer}>
        <Divider sx={{ marginTop: 3 }} />
        <Stack className="rowACJC">
          <Typography
            marginTop={1.5}
            fontSize={20}
            color={COLOR_PRIMARY}
            variant="body1"
          >
            {state && state.title}
          </Typography>
        </Stack>
        {state &&
          state.data.map((item, index) => {
            return (
              <Button
                variant="text"
                key={index}
                onClick={() => {
                  dispatch(saveListSpec({ id: state.id, value: item.id }));
                  dispatch(setBottomSheet(null));
                  props.onClose();
                }}
                style={styles.itemSpecContainer}
              >
                <Typography variant="body1" style={styles.textSpecItem}>
                  {item.value}
                </Typography>
              </Button>
            );
          })}
      </Stack>
    </SwipeableBottomSheet>
  );
};
const styles = {
  addToBasketContainer: {
    width: "100%",
    height: 500,
    backgroundColor: "white",
  },
  headerSheet: {
    width: 50,
    height: 3,
    backgroundColor: "#ccc",
    borderRadius: 10,
    padding: 2,
  },
  image: {
    width: 25,
    height: 25,
    objectFit: "contain",
    marginTop: 15,
  },
  itemSpecContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    padding: 8,
  },
  textSpecItem: {
    fontFamily: FONT_NAME,
    fontSize: 14,
    color: "#111",
  },
};
export default memo(ListSpicificationSheet);
