import { Button, Divider, Drawer, Stack, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { COLOR_PRIMARY } from "../../theme/theme";
import { toFarsiNumber } from "../../utils/Persian";
import MySnackBar from "../public/MySnackBar";

export default function DrawerNavigation({ open, onClose, data }) {
  const drawerWidth = 240;
  const router = useRouter();
  function handleDrawerClose() {
    onClose();
  }
  const userInfo =
    typeof window !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : null;
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });

  return (
    <Drawer anchor={"right"} open={open} onClose={() => onClose()}>
      <Stack className="col" style={{ width: drawerWidth }}>
        <Stack style={styles.header} className="colJCAC">
          <img alt="" src="/images/user.png" style={styles.userIcon} />
          {data === null ? (
            <Button
              onClick={() => router.push("auth")}
              style={styles.btnLogin}
              variant="outlined"
              color="inherit"
            >
              <Typography color="#fff" variant="body1">
                ورود حساب
              </Typography>
            </Button>
          ) : (
            <>
              {data.fullName === null ? (
                <>
                  <Button
                    style={styles.btnLogin}
                    variant="outlined"
                    color="inherit"
                    onClick={() => router.push("Profile/EditProfile")}
                  >
                    <Typography color="#fff" variant="body1">
                      تکمیل پروفایل
                    </Typography>
                  </Button>
                </>
              ) : (
                <Stack className="colJCAC">
                  <Typography color="#fff" mt={1} variant="h1">
                    {data.fullName}
                  </Typography>
                  <Typography color="#fff" mt={1} variant="h1">
                    {toFarsiNumber(data.phoneNumber)}
                  </Typography>
                </Stack>
              )}
            </>
          )}
        </Stack>
        <Button
          onClick={() => {
            if (userInfo === null) {
              setSnackBar({
                open: true,
                message: "ابتدا وارد حساب کاربری خود شوید",
              });
              router.push("/auth");
            } else {
              router.push("Drawer/Marks");
              handleDrawerClose();
            }
          }}
          style={styles.itemDrawer}
        >
          <img src="images/marks.png" alt="" style={styles.imageItemDrawer} />
          <Typography mr={1.5} variant={"h1"}>
            نشان شده ها
          </Typography>
        </Button>
        <Divider />
        <Button
          onClick={() => {
            router.push("Drawer/Questions");
            handleDrawerClose();
          }}
          style={styles.itemDrawer}
        >
          <img
            src="images/questions.png"
            alt=""
            style={styles.imageItemDrawer}
          />
          <Typography mr={1.5} variant={"h1"}>
            سوالات متداول
          </Typography>
        </Button>
        <Divider />
        <Button
          onClick={() => {
            router.push("Drawer/AboutUs");
            handleDrawerClose();
          }}
          style={styles.itemDrawer}
        >
          <img src="images/aboutUs.png" alt="" style={styles.imageItemDrawer} />
          <Typography mr={1.5} variant={"h1"}>
            درباره ما
          </Typography>
        </Button>
        <Divider />
        <Button
          onClick={() => {
            router.push("Drawer/ContactUs");
            handleDrawerClose();
          }}
          style={styles.itemDrawer}
        >
          <img
            src="images/contact us.png"
            alt=""
            style={styles.imageItemDrawer}
          />
          <Typography mr={1.5} variant={"h1"}>
            تماس با ما
          </Typography>
        </Button>
        <Divider />
        <Button
          onClick={() => {
            router.push("Drawer/Terms");
            handleDrawerClose();
          }}
          style={styles.itemDrawer}
        >
          <img
            src="images/Terms and Conditions.png"
            alt=""
            style={styles.imageItemDrawer}
          />
          <Typography mr={1.5} variant={"h1"}>
            قوانین و مقررات
          </Typography>
        </Button>
        <Divider />
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Drawer>
  );
}
const styles = {
  userIcon: {
    width: 70,
    height: 70,
    objectFit: "contain",
  },
  header: {
    height: 200,
    backgroundColor: COLOR_PRIMARY,
  },
  btnLogin: {
    marginTop: 5,
    width: "50%",
    border: "1px solid #ffff",
  },
  imageItemDrawer: {
    width: 25,
    height: 25,
    objectFit: "contain",
  },
  itemDrawer: {
    padding: 13,
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-start",
  },
};
