/* eslint-disable @next/next/no-img-element */
import { IconButton, Stack } from "@mui/material";
import React, { useRef } from "react";
import Carousel from "react-elastic-carousel";
import { useSelector } from "react-redux";
import { isEmptyObject } from "../../../utils/Tools";
const ImageSliderFile = ({ images }) => {
  const imageBaseUrl = useSelector((st) => st.splash.data.imageBaseUrl);

  return (
    <Carousel autoPlaySpeed={5000} enableAutoPlay={true}>
      {images &&
        images.map((item) => (
          <div key={item.id}>
            <img
              src={imageBaseUrl + item.source}
              style={styles.imageSlider}
              alt=""
            />
          </div>
        ))}
    </Carousel>
  );
};
const styles = {
  imageSlider: {
    width: "100%",
    height: 280,
    borderRadius: 10,
    objectFit: "contain",
  },
  row: {
    position: "absulote",
    top: 0,
  },
};
export default ImageSliderFile;
