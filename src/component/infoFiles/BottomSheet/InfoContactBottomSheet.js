/* eslint-disable @next/next/no-img-element */
import React, { memo } from "react";
import classNames from "classnames";
import SwipeableBottomSheet from "@sergeymyssak/swipeable-bottom-sheet";
import "@sergeymyssak/swipeable-bottom-sheet/lib/min.css";
import { useRouter } from "next/router";
import { Stack, Typography, Divider, Button } from "@mui/material";
import { useDispatch } from "react-redux";
import { FiUser } from "react-icons/fi";
export function HeaderSheet() {
  return (
    <Stack mt={1} className="rowACJC">
      <Stack style={styles.headerSheet} />
    </Stack>
  );
}

const InfoContactBottomSheet = ({
  open,
  disableSwipe = false,
  onClose,
  containerClassName,
  bodyClassName,
  phone,
}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  return (
    <SwipeableBottomSheet
      isOpen={open}
      onChange={() => onClose(false)}
      swipeableViewsProps={{ disabled: disableSwipe }}
      containerClassName={classNames("custom-bottom-sheet", containerClassName)}
      bodyClassName={classNames("custom-bottom-sheet__body", bodyClassName)}
    >
      {HeaderSheet()}
      <Stack style={styles.addToBasketContainer}>
        <Stack className={"colJCAC"}>
          <FiUser color="#aaa" size={25} style={{ marginTop: 10 }} />
          <Typography variant="h1" marginTop={2}>
            اطلاعات تماس
          </Typography>
        </Stack>
        <Divider sx={{ marginTop: 3 }} />
        <Stack className={"rowACJC"} mt={3}>
          <Button
            style={styles.btn}
            variant="outlined"
            color={"primary"}
            className="rowACJC"
            onClick={() => {
              window.open(`tel:${phone}`);
            }}
          >
            <Typography variant="h1">{phone}</Typography>
            <img src="/images/call.png" style={styles.image} />
          </Button>
          <Button
            style={Object.assign({ backgroundColor: "#aaa" }, styles.btn)}
            variant="outlined"
            color={"secondary"}
            className="rowACJC"
            onClick={() => window.open(`sms:${phone}`)}
          >
            <Typography color="white" variant="h1">
              ارسال پیام
            </Typography>
            <img src={"/images/message.png"} style={styles.image} />
          </Button>
        </Stack>
      </Stack>
    </SwipeableBottomSheet>
  );
};
const styles = {
  addToBasketContainer: {
    width: "100%",
    height: 260,
    backgroundColor: "white",
  },
  headerSheet: {
    width: 50,
    height: 3,
    backgroundColor: "#ccc",
    borderRadius: 10,
    padding: 2,
  },
  image: {
    width: 35,
    height: 35,
    objectFit: "contain",
    marginLeft: 10,
  },
  btn: {
    width: "35%",
    marginInline: 15,
    padding: 6,
  },
};
export default memo(InfoContactBottomSheet);
