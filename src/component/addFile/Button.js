import { Button, CircularProgress, Typography } from "@mui/material";
import React from "react";

const Btn = (props) => {
  return (
    <Button
      onClick={props.onClick}
      disabled={props.disabled}
      style={styles.btn}
      variant="contained"
      color="primary"
    >
      {props.isLoading ? (
        <CircularProgress size={25} style={{ color: "white" }} />
      ) : (
        <Typography variant="h1" fontSize={18} color="white">
          {props.text}
        </Typography>
      )}
    </Button>
  );
};
const styles = {
  btn: {
    width: "100%",
    position: "fixed",
    bottom: 0,
    padding: 20,
    borderRaduis: 0,
  },
};
export default Btn;
