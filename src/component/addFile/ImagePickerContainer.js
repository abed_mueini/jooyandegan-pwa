import { Stack } from "@mui/material";
import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  saveImage1,
  saveImage2,
  saveImage3,
  saveImage4,
  saveImage5,
  saveImage6,
} from "../../redux/actions/AddFileActions";
import { COLOR_PRIMARY } from "../../theme/theme";
import { FONT_NAME, FONT_NAME_BOLD } from "../../utils/Constant";
import { isEmptyObject } from "../../utils/Tools";
import MySnackBar from "../public/MySnackBar";

export default function ImagePickerContainer() {
  const dispatch = useDispatch();
  const imageRef1 = useRef(null);
  const imageRef2 = useRef(null);
  const imageRef3 = useRef(null);
  const imageRef4 = useRef(null);
  const imageRef5 = useRef(null);
  const imageRef6 = useRef(null);
  const files = useSelector((state) => state.files);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const noImage = "/images/add image.png";
  const onFileChange = (event, index) => {
    event.stopPropagation();
    event.preventDefault();
    let file = event.target.files[0];
    switch (index) {
      case 0: {
        dispatch(saveImage1(file));
        break;
      }
      case 1: {
        dispatch(saveImage2(file));
        break;
      }
      case 2: {
        dispatch(saveImage3(file));
        break;
      }
      case 3: {
        dispatch(saveImage4(file));
        break;
      }
      case 4: {
        dispatch(saveImage5(file));
        break;
      }
      case 5: {
        dispatch(saveImage6(file));
        break;
      }
    }
  };
  return (
    <Stack className="colJCAC">
      <Stack style={styles.rowContainer} className="row">
        <Stack
          onClick={() => {
            if (!isEmptyObject(files.image1)) {
              dispatch(saveImage1(null));
            } else {
              imageRef1.current.click();
            }
          }}
          style={styles.imageContainer}
        >
          <input
            type="file"
            accept={"image/*"}
            id="file"
            onChange={(e) => onFileChange(e, 0)}
            style={styles.fileUpload}
            ref={imageRef1}
          />
          <img
            style={styles.image}
            alt={"image1"}
            src={
              !isEmptyObject(files.image1)
                ? URL.createObjectURL(files.image1)
                : noImage
            }
          />
          <Stack className={"rowACJC"} style={styles.imageInfo1}>
            <span style={styles.imageInfoText2}>تصویر پیش فرض</span>
          </Stack>
          <Stack className={"rowACJC"} style={styles.imageInfo2}>
            <span style={styles.imageInfoText}>
              {isEmptyObject(files.image2) ? "انتخاب عکس" : "حذف عکس"}
            </span>
          </Stack>
        </Stack>
        <Stack
          onClick={() => {
            if (!isEmptyObject(files.image1)) {
              if (!isEmptyObject(files.image2)) {
                dispatch(saveImage2(null));
              } else {
                imageRef2.current.click();
              }
            } else {
              setSnackBar({
                open: true,
                message: "تصاویر را به ترتیب پر کنید",
              });
            }
          }}
          style={styles.imageContainer}
        >
          <input
            type="file"
            id="file"
            accept={"image/*"}
            onChange={(e) => onFileChange(e, 1)}
            style={styles.fileUpload}
            ref={imageRef2}
          />
          <img
            style={styles.image}
            alt={"image2"}
            src={
              !isEmptyObject(files.image2)
                ? URL.createObjectURL(files.image2)
                : noImage
            }
          />
          <Stack className={"rowACJC"} style={styles.imageInfo1}>
            <span style={styles.imageInfoText2}>تصویر دوم</span>
          </Stack>
          <Stack className={"rowACJC"} style={styles.imageInfo2}>
            <span style={styles.imageInfoText}>
              {isEmptyObject(files.image2) ? "انتخاب عکس" : "حذف عکس"}
            </span>
          </Stack>
        </Stack>
        <Stack
          onClick={() => {
            if (!isEmptyObject(files.image2)) {
              if (!isEmptyObject(files.image3)) {
                dispatch(saveImage3(null));
              } else {
                imageRef3.current.click();
              }
            } else {
              setSnackBar({
                open: true,
                message: "تصاویر را به ترتیب پر کنید",
              });
            }
          }}
          style={styles.imageContainer}
        >
          <input
            type="file"
            id="file"
            accept={"image/*"}
            onChange={(e) => onFileChange(e, 2)}
            style={styles.fileUpload}
            ref={imageRef3}
          />
          <img
            style={styles.image}
            alt={"image3"}
            src={
              !isEmptyObject(files.image3)
                ? URL.createObjectURL(files.image3)
                : noImage
            }
          />
          <Stack className={"rowACJC"} style={styles.imageInfo1}>
            <span style={styles.imageInfoText2}>تصویر سوم</span>
          </Stack>
          <Stack className={"rowACJC"} style={styles.imageInfo2}>
            <span style={styles.imageInfoText}>
              {isEmptyObject(files.image2) ? "انتخاب عکس" : "حذف عکس"}
            </span>
          </Stack>
        </Stack>
      </Stack>
      <Stack style={styles.rowContainer} className="row">
        <Stack
          onClick={() => {
            if (!isEmptyObject(files.image3)) {
              if (!isEmptyObject(files.image4)) {
                dispatch(saveImage4(null));
              } else {
                imageRef4.current.click();
              }
            } else {
              setSnackBar({
                open: true,
                message: "تصاویر را به ترتیب پر کنید",
              });
            }
          }}
          style={styles.imageContainer}
        >
          <input
            type="file"
            id="file"
            accept={"image/*"}
            onChange={(e) => onFileChange(e, 3)}
            style={styles.fileUpload}
            ref={imageRef4}
          />
          <img
            style={styles.image}
            alt={"image4"}
            src={
              !isEmptyObject(files.image4)
                ? URL.createObjectURL(files.image4)
                : noImage
            }
          />
          <Stack className={"rowACJC"} style={styles.imageInfo1}>
            <span style={styles.imageInfoText2}>تصویر چهارم</span>
          </Stack>
          <Stack className={"rowACJC"} style={styles.imageInfo2}>
            <span style={styles.imageInfoText}>
              {isEmptyObject(files.image2) ? "انتخاب عکس" : "حذف عکس"}
            </span>
          </Stack>
        </Stack>
        <Stack
          onClick={() => {
            if (!isEmptyObject(files.image4)) {
              if (!isEmptyObject(files.image5)) {
                dispatch(saveImage5(null));
              } else {
                imageRef5.current.click();
              }
            } else {
              setSnackBar({
                open: true,
                message: "تصاویر را به ترتیب پر کنید",
              });
            }
          }}
          style={styles.imageContainer}
        >
          <input
            type="file"
            id="file"
            accept={"image/*"}
            onChange={(e) => onFileChange(e, 4)}
            style={styles.fileUpload}
            ref={imageRef5}
          />
          <img
            style={styles.image}
            alt={"image5"}
            src={
              !isEmptyObject(files.image5)
                ? URL.createObjectURL(files.image5)
                : noImage
            }
          />
          <Stack className={"rowACJC"} style={styles.imageInfo1}>
            <span style={styles.imageInfoText2}>تصویر پنجم</span>
          </Stack>
          <Stack className={"rowACJC"} style={styles.imageInfo2}>
            <span style={styles.imageInfoText}>
              {isEmptyObject(files.image2) ? "انتخاب عکس" : "حذف عکس"}
            </span>
          </Stack>
        </Stack>
        <Stack
          onClick={() => {
            if (!isEmptyObject(files.image5)) {
              if (!isEmptyObject(files.image6)) {
                dispatch(saveImage6(null));
              } else {
                imageRef6.current.click();
              }
            } else {
              setSnackBar({
                open: true,
                message: "تصاویر را به ترتیب پر کنید",
              });
            }
          }}
          style={styles.imageContainer}
        >
          <input
            type="file"
            id="file"
            accept={"image/*"}
            onChange={(e) => onFileChange(e, 5)}
            style={styles.fileUpload}
            ref={imageRef6}
          />
          <img
            style={styles.image}
            alt={"image6"}
            src={
              !isEmptyObject(files.image6)
                ? URL.createObjectURL(files.image6)
                : noImage
            }
          />
          <Stack className={"rowACJC"} style={styles.imageInfo1}>
            <span style={styles.imageInfoText2}>تصویر ششم</span>
          </Stack>
          <Stack className={"rowACJC"} style={styles.imageInfo2}>
            <span style={styles.imageInfoText}>
              {isEmptyObject(files.image2) ? "انتخاب عکس" : "حذف عکس"}
            </span>
          </Stack>
        </Stack>
      </Stack>
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );
}
const styles = {
  imageContainer: {
    position: "relative",
    width: "100%",
    height: 100,
    cursor: "pointer",
    border: "1px #aaa solid",
    marginInline: 20,
    borderRadius: 5,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  fileUpload: {
    display: "none",
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  rowContainer: {
    width: "100%",
    marginTop: 10,
  },
  image: {
    width: "60%",
    height: "60%",
    objectFit: "contain",
    borderRadius: 12,
  },
  imageInfo1: {
    position: "absolute",
    top: 0,
    right: 0,
    left: 0,
    padding: 5,
    backgroundColor: COLOR_PRIMARY.concat("99"),
  },
  imageInfo2: {
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    padding: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  imageInfoText: {
    color: "#aaa",
    fontSize: 12,
    fontFamily: FONT_NAME,
  },
  imageInfoText2: {
    color: "white",
    fontSize: 14,
    fontFamily: FONT_NAME_BOLD,
    textAlign: "center",
  },
};
