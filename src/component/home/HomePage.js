import {
  Container,
  Typography,
  Stack,
  TextField,
  InputAdornment,
  IconButton,
  Input,
  Divider,
  CircularProgress,
  Button,
} from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { storeFavorites } from "../../redux/actions/FavoritesAction";
import {
  fetchFilters,
  needRefresh,
  setCity,
  setFilterCategory,
} from "../../redux/actions/FilterActions";
import {
  fetchSplashData,
  saveUserWallet,
} from "../../redux/actions/SplashActions";
import { GET_FILES, GET_INITIAL_INFOS } from "../../utils/Api";
import { isEmptyObject } from "../../utils/Tools";
import TabNavigation from "../navigation/TabNavigation";
import { BsSearch } from "react-icons/bs";
import { useRouter } from "next/router";
import AdverItem from "../index/AdverItem";
import InfiniteScroll from "react-infinite-scroll-component";
import { COLOR_PRIMARY } from "../../theme/theme";

export default function HomePage() {
  const filterData = useSelector((cat) => cat.filter);
  const searchQuery = useSelector((cat) => cat.filter.query);
  const [moreLoading, setMoreLoading] = useState(false);
  const [data, setData] = useState([]);
  const [search, setSearch] = useState(searchQuery);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const router = useRouter();
  const dispatch = useDispatch();
  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (filterData.needRefresh) {
      getList(true);
    }
  }, [filterData]);

  useEffect(() => {
    if (!filterData.needRefresh) {
      getList(false);
    }
  }, [page]);
  return (
    <Stack className="col">
      <TabNavigation name="home" />
      <Stack style={styles.topHeader}>
        <Stack mt={1} className="rowACJC">
          <Stack
            p={0.5}
            style={styles.inputContainer}
            width={"95%"}
            className="rowACJSB"
          >
            <Input
              value={search}
              disableUnderline={true}
              placeholder="جستجو کنید..."
              onChange={(e) => setSearch(e.target.value)}
              style={styles.input}
              type="search"
              startAdornment={
                <InputAdornment position="start">
                  <IconButton
                    onClick={() => router.push("/")}
                    className="rowACJC"
                  >
                    <BsSearch color="#aaa" size={22} />
                  </IconButton>
                </InputAdornment>
              }
            />
            <IconButton
              onClick={() => router.push("/Profile/SetCity")}
              className="rowACJC"
            >
              <Typography
                marginLeft={1}
                color={"#aaa"}
                fontSize={15}
                variant="body1"
              >
                {filterData.city && filterData.city.label}
              </Typography>
              <img src={"/images/location.png"} style={styles.image} alt="" />
            </IconButton>
          </Stack>
        </Stack>
        <Divider style={styles.divider} />
        <Stack className="rowACJC">
          <Stack className={"horizontal"}>
            <Button
              onClick={() => {
                if (
                  filterData.data.filter(
                    (sd) => sd.id === filterData.category.id
                  )[0].parentId !== null
                ) {
                  router.push("filters");
                } else {
                  if (filterData.category.id !== -1) {
                    router.push({
                      pathname: "Categories",
                      query: {
                        id: filterData.category.id,
                        title: filterData.category.title,
                        parentId: filterData.category.id,
                        parentTitle: filterData.category.title,
                      },
                    });
                  } else {
                    router.push("filters");
                  }
                }
              }}
              className="rowACJSB"
              style={{ padding: 10 }}
              variant="outlined"
            >
              <img src="/images/Filter.png" style={styles.filterImage} alt="" />
              <Typography fontSize={17} variant="h1">
                {/* {filterData.data.filter(
                (sd) => sd.id ===  filterData.category.id
              )[0].parentId !== null && getFilterCount() !== 0
                ? `فیلتر‌های ${
                    filterData.category.title
                  } ${getFilterCount()} مورد انتخاب شده`
                : "فیلتر"} */}
                فیلتر
              </Typography>
            </Button>
          </Stack>
        </Stack>
        <Divider style={styles.divider} />
      </Stack>
      {loading ? (
        <Stack className="loading">
          <CircularProgress />
        </Stack>
      ) : (
        <Stack>
          {data.length > 0 ? (
            <>
              {data.map((item) => {
                return (
                  <Stack key={item.id}>
                    <AdverItem item={item} />
                    <Divider />
                  </Stack>
                );
              })}
            </>
          ) : (
            <Stack className="loading">
              <Typography variant="h1" color={COLOR_PRIMARY}>
                فایلی یافت نشد
              </Typography>
            </Stack>
          )}
        </Stack>
      )}
      <Stack style={{ height: 70 }} />
    </Stack>
  );
  function getFilterCount() {
    let ct = 0;
    ct += filterData.params.length;
    if (filterData.hasImage) {
      ct += 1;
    }
    if (filterData.hasPlan) {
      ct += 1;
    }
    if (filterData.hasTour) {
      ct += 1;
    }
    return ct;
  }
  async function getData() {
    let token = "";
    let city = {};
    try {
      const value = localStorage.getItem("token");
      const cityTemp = localStorage.getItem("city");
      token = value;
      city = cityTemp;
    } catch (e) {
      console.log("zze", e);
    }
    axios
      .get(GET_INITIAL_INFOS, {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
      .then((res) => {
        if (res.data.isSuccess) {
          console.log("result", res.data.data);
          let categories = res.data.data.categories;
          let favorites = res.data.data.favorites;
          delete res.data.data.categories;
          delete res.data.data.favorites;
          let myCity = JSON.parse(city);
          console.log("city", city);
          if (!isEmptyObject(city)) {
            dispatch(setCity(myCity));
          } else {
            dispatch(setCity(res.data.data.cities[0]));
          }
          dispatch(saveUserWallet(res.data.data.wallet));
          dispatch(fetchSplashData(res.data.data));
          dispatch(fetchFilters(categories));
          dispatch(
            setFilterCategory({
              parentId: categories[categories.length - 1].id,
              parentTitle: categories[categories.length - 1].title,
              title: categories[categories.length - 1].title,
              id: categories[categories.length - 1].id,
            })
          );
          dispatch(storeFavorites(favorites !== null ? favorites : []));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  function getList(first) {
    if (first) {
      setLoading(true);
    } else {
      setMoreLoading(true);
    }
    let oth = {};
    if (filterData.needRefresh) {
      setData([]);
      setPage(1);
      setCount(0);
    }
    if (filterData.params.length > 0) {
      oth = prepareFilter();
    }
    let dt = {};
    dt.page = filterData.needRefresh ? 1 : page;
    dt.cityId = filterData.city !== null && filterData.city.value;

    if (filterData.hasImage) {
      dt.hasImage = filterData.hasImage;
    }
    if (filterData.hasPlan) {
      dt.hasPlan = filterData.hasPlan;
    }
    if (filterData.hasTour) {
      dt.hasTour = filterData.hasTour;
    }
    if (router.query) {
      dt.categoryId = router.query.id;
    } else {
      dt.categoryId = filterData.catId.id;
    }
    if (searchQuery !== "") {
      dt.q = searchQuery;
    }
    if (oth.ListSpecifications) {
      dt.ListSpecifications = oth.ListSpecifications;
    }
    if (oth.NumberSpecifications) {
      dt.NumberSpecifications = oth.NumberSpecifications;
    }
    axios
      .post(GET_FILES, dt)
      .then((res) => {
        setData((prevState) => prevState.concat(res.data.data.files));
        setCount(res.data.data.totalItems);
        console.log("dt", dt);
        if (filterData.needRefresh) {
          dispatch(setRefresh(false));
        }
        if (first) {
          setLoading(false);
        } else {
          setMoreLoading(false);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log("err", err);
      });
  }
  function prepareFilter() {
    let listSpecs = [];
    let numberSpecsResult = [];
    filterData.params.forEach((item) => {
      if (item.type === "list") {
        listSpecs.push(item.value);
      } else {
        let values = Array(2).fill(0);
        if (item.rangeType === "min") {
          values[0] = item.value;
        }
        if (item.rangeType === "max") {
          values[1] = item.value;
        }
        if (numberSpecsResult.findIndex((ite) => ite.id === item._id) === -1) {
          numberSpecsResult.push({ id: item._id, value: values });
        } else {
          let tmp = numberSpecsResult.filter((ite) => ite.id === item._id)[0];
          tmp.value[tmp.value.indexOf(0)] = item.value;
        }
      }
    });
    let result = {};
    if (listSpecs.length > 0) {
      result.ListSpecifications = listSpecs;
    }
    if (numberSpecsResult.length > 0) {
      result.NumberSpecifications = numberSpecsResult;
    }
    return result;
  }
}
const styles = {
  input: {
    width: "80%",
  },
  inputContainer: { border: "1px solid #aaa", borderRadius: 6 },
  image: {
    width: 20,
    height: 25,
    objectFit: "contain",
  },
  divider: {
    marginTop: 10,
  },
  filterImage: {
    width: 25,
    height: 25,
    objectFit: "contain",
    marginLeft: 5,
  },
  topHeader: {
    position: "sticky",
    top: 0,
    backgroundColor: "white",
  },
};
