import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import { Button, Container } from "@mui/material";
import { COLOR_PRIMARY, COLOR_SECOUNDRY } from "../../theme/theme";
import { FONT_NAME_BOLD } from "../../utils/Constant";
import { Drawer } from "@mui/material";
import DrawerNavigation from "../drawer/DrawerNavigation";
const TabNavigation = (props) => {
  const router = useRouter();
  const [activeTabs, setActiveTabs] = useState(props.name);
  const [openDrawer, setOpenDrawer] = useState(false);
  const userInfo =
    typeof window !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : null;
  return (
    <div style={styles.tabContainer}>
      <div className="bnTab" onClick={() => router.push("/Profile")}>
        {activeTabs === "Profile" ? (
          <Image
            style={styles.iconTabs}
            src={"/images/Profile2.png"}
            width={23}
            height={22}
            alt=""
          />
        ) : (
          <Image
            src={"/images/Profile.png"}
            style={styles.iconTabs}
            width={23}
            height={22}
            alt=""
          />
        )}
        <span
          style={
            activeTabs === "Profile" ? styles.titleTabSelected : styles.titleTab
          }
        >
          پروفایل
        </span>
      </div>
      <div className="bnTab" onClick={() => router.push("/Categories")}>
        {activeTabs === "categories" ? (
          <Image
            src={"/images/Categories2.png"}
            width={24}
            height={22}
            alt=""
          />
        ) : (
          <Image src={"/images/Categories.png"} width={24} height={22} alt="" />
        )}
        <span
          style={
            activeTabs === "categories"
              ? styles.titleTabSelected
              : styles.titleTab
          }
        >
          دسته بندی
        </span>
      </div>
      <Button className="bnTab" onClick={() => router.push("/add")}>
        {activeTabs === "AddFile" ? (
          <Image src={"/images/Add2.png"} width={35} height={37} alt="" />
        ) : (
          <Image src={"/images/Add.png"} width={35} height={37} alt="" />
        )}
      </Button>
      <div className="bnTab" onClick={() => router.push("/")}>
        {activeTabs === "home" ? (
          <Image
            src={"/images/Advertising2.png"}
            width={23}
            height={22}
            alt=""
          />
        ) : (
          <Image
            src={"/images/Advertising.png"}
            width={23}
            height={22}
            alt=""
          />
        )}
        <span
          style={
            activeTabs === "home" ? styles.titleTabSelected : styles.titleTab
          }
        >
          آگهی ها
        </span>
      </div>
      <div
        className="bnTab"
        onClick={() => {
          setOpenDrawer(true);
        }}
      >
        <Image src={"/images/More.png"} width={22} height={20} alt="" />
        <span
          style={
            activeTabs === "More2" ? styles.titleTabSelected : styles.titleTab
          }
        >
          بیشتر
        </span>
      </div>
      <DrawerNavigation
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        data={userInfo}
      />
    </div>
  );
};

const styles = {
  tabContainer: {
    width: "100%",
    height: 65,
    flexDirection: "row",
    display: "flex",
    position: "fixed",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 9999999999999999999,
    backgroundColor: "white",
    boxShadow: "0px 1px 2px 1px rgba(0, 0, 0, 0.2)",
    right: 0,
  },
  tabSelected: {
    backgroundColor: "red",
  },
  titleTab: {
    fontSize: 12,
    fontFamily: FONT_NAME_BOLD,
    color: COLOR_SECOUNDRY,
    marginTop: 4,
    textAlign: "center",
    marginBottom: -2,
    width: "100%",
  },
  titleTabSelected: {
    fontSize: 12,
    fontFamily: FONT_NAME_BOLD,
    color: COLOR_PRIMARY,
    marginTop: 4,
    textAlign: "center",
    marginBottom: -2,
    width: "100%",
  },
  bottomTabsSelected: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: COLOR_PRIMARY,
    padding: 2,
    borderRadius: 3,
  },
  iconTabs: {
    marginTop: 15,
  },
};

export default TabNavigation;
