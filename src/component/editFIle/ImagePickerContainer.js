import { isEmptyObject } from "../../utils/Tools";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Stack, Grid, Typography } from "@mui/material";
import {
  addImage,
  cancelAddImage,
  deleteImage,
  cancelDeleteImage,
  cancelEditImage,
  EditImage,
  EditImageIds,
} from "../../redux/actions/EditFileActions";
import { createRef } from "react";
import { COLOR_PRIMARY } from "../../theme/theme";
import MySnackBar from "../public/MySnackBar";
import { FONT_NAME_BOLD } from "../../utils/Constant";
const noImage = "/images/noImage2.png";
export default function ImagePickerContainer() {
  const dispatch = useDispatch();
  const imageBaseUrl = useSelector((state) => state.splash.data.imageBaseUrl);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const arrLength = 6;
  const arrLengthEdit = 6;
  const [elRefs, setElRefs] = React.useState([]);
  const [elRefsEdit, setElRefsEdit] = React.useState([]);
  const files = useSelector((state) => state.editFile);
  React.useEffect(() => {
    // add or remove refs
    setElRefs((elRefs) =>
      Array(arrLength)
        .fill()
        .map((_, i) => elRefs[i] || createRef())
    );
  }, [arrLength]);
  React.useEffect(() => {
    // add or remove refs
    setElRefsEdit((elRefsEdit) =>
      Array(arrLengthEdit)
        .fill()
        .map((_, i) => elRefsEdit[i] || createRef())
    );
  }, [arrLengthEdit]);
  const onFileChange = (event, dt, index) => {
    event.stopPropagation();
    event.preventDefault();
    let file = event.target.files[0];
    if (dt.isNew) {
      file.idx = index;
      dispatch(addImage(file));
    } else {
      dispatch(EditImage(file));
      dispatch(EditImageIds(dt.id));
    }
  };
  return (
    <Stack style={styles.gridContainer}>
      {files.images.map((item2, index) => {
        return (
          <Grid item style={styles.item} sm={4} xs={6}>
            <img style={styles.image} src={getImageSource(item2, index)} />
            <div className="rowAC">
              {getImageSource(item2, index) !== noImage && (
                <div style={{ width: 10 }} />
              )}
              {isEmptyObject(item2) ? (
                files.addImages.filter((ai) => ai.idx === index).length > 0 ? (
                  <Button
                    fullWidth
                    variant="contained"
                    size="small"
                    color="primary"
                    onClick={() => dispatch(cancelAddImage(index))}
                  >
                    <span style={styles.descImageSelect}>لغو عکس</span>
                  </Button>
                ) : (
                  <Button
                    fullWidth
                    variant="contained"
                    size="small"
                    color="primary"
                    onClick={() => elRefs[index].current.click()}
                  >
                    <span style={styles.descImageDelete}>انتخاب تصویر</span>
                    <input
                      type="file"
                      accept={"image/*"}
                      id="file"
                      onChange={(e) => {
                        if (isEmptyObject(item2)) {
                          if (index === 0) {
                            onFileChange(e, { isNew: true }, index);
                          } else {
                            if (
                              getImageSource(
                                files.images[index - 1],
                                index - 1
                              ) !== noImage
                            ) {
                              onFileChange(e, { isNew: true }, index);
                            } else {
                              setSnackBar({
                                open: true,
                                message: "لطفا تصاویر را به ترتیب پر کنید",
                              });
                            }
                          }
                        } else {
                          onFileChange(
                            e,
                            {
                              isNew: false,
                              id: item2.id,
                            },
                            2
                          );
                        }
                      }}
                      style={styles.fileUpload}
                      ref={elRefs[index]}
                    />
                  </Button>
                )
              ) : files.editFileImagesId.includes(item2.id) ? (
                <Button
                  fullWidth
                  variant="contained"
                  size="small"
                  color="primary"
                  onClick={() => dispatch(cancelEditImage(item2.id))}
                >
                  <span style={styles.descImageDelete}>لغو ویرایش</span>
                </Button>
              ) : files.deleteImages.includes(item2.id) ? (
                <Button
                  fullWidth
                  variant="contained"
                  size="small"
                  color="primary"
                  onClick={() => dispatch(cancelDeleteImage(item2.id))}
                >
                  <span style={styles.descImageDelete}>لغو حذف</span>
                </Button>
              ) : (
                <div className="rowAC" style={{ width: "100%" }}>
                  <Button
                    fullWidth
                    variant="contained"
                    size="small"
                    color="primary"
                    onClick={() => dispatch(deleteImage(item2.id))}
                  >
                    <span style={styles.descImageDelete}>حذف</span>
                  </Button>
                  <div style={{ width: 10 }} />
                  <Button
                    fullWidth
                    variant="contained"
                    size="small"
                    color="primary"
                    onClick={() => elRefsEdit[index].current.click()}
                  >
                    <span style={styles.descImageDelete}>ویرایش</span>
                    <input
                      type="file"
                      accept={"image/*"}
                      id="file"
                      onChange={(e) => {
                        onFileChange(
                          e,
                          {
                            isNew: false,
                            id: item2.id,
                          },
                          -999
                        );
                      }}
                      style={styles.fileUpload}
                      ref={elRefsEdit[index]}
                    />
                  </Button>
                </div>
              )}
            </div>
          </Grid>
        );
      })}
      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
    </Stack>
  );

  function getImageSource(item, ind) {
    if (!isEmptyObject(item)) {
      let idx = files.editFileImagesId.indexOf(item.id);
      if (idx === -1) {
        return imageBaseUrl + item.source;
      } else {
        return URL.createObjectURL(
          files.editFileImages.filter((_, index) => index === idx)[0]
        );
      }
    } else {
      if (files.addImages.filter((fi) => fi.idx === ind).length === 0) {
        return noImage;
      } else {
        return URL.createObjectURL(
          files.addImages.filter((fi) => fi.idx === ind)[0]
        );
      }
    }
  }
  function getImageTitle(ind) {
    let result = "پیش فرض";
    switch (ind) {
      case 0: {
        result = "پیش فرض";
        break;
      }
      case 1: {
        result = "دوم";
        break;
      }
      case 2: {
        result = "سوم";
        break;
      }
      case 3: {
        result = "چهارم";
        break;
      }
      case 4: {
        result = "پنجم";
        break;
      }
      case 5: {
        result = "ششم";
        break;
      }
    }
    return `تصویر ${result}`;
  }
}

const styles = {
  imageContainer: {
    borderWidth: 1,
    borderColor: "#aaa",
    width: "100%",
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    height: 200,
  },
  descImage2: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    color: "white",
    fontSize: 14,
    textAlign: "center",
    backgroundColor: COLOR_PRIMARY.concat("77"),
  },
  fileUpload: {
    display: "none",
    position: "absolute",
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
  image: {
    width: "50%",
    objectFit: "contain",
    height: 140,
  },
  descImageContainer: {
    color: "white",
    fontSize: 14,
    textAlign: "center",
    backgroundColor: "#00000088",
  },
  descImageRow: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  descImageEdit: {
    color: "white",
    fontSize: 14,
    textAlign: "center",
    fontFamily: FONT_NAME_BOLD,
  },
  descImageDelete: {
    color: "white",
    fontSize: 14,
    textAlign: "center",
    fontFamily: FONT_NAME_BOLD,
  },
  descImageSelect: {
    color: "white",
    fontSize: 14,
    textAlign: "center",
    fontFamily: FONT_NAME_BOLD,
  },
  imageInfo1: {
    position: "absolute",
    top: 0,
    right: 0,
    left: 0,
    padding: 5,
    backgroundColor: COLOR_PRIMARY.concat("77"),
  },
  imageInfo2: {
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    padding: 5,
    backgroundColor: "#00000077",
  },
  imageInfoText: {
    color: "white",
    fontSize: 12,
    fontFamily: FONT_NAME_BOLD,
  },
  controlIcon: {
    position: "absolute",
    alignSelf: "center",
    color: "red",
    fontSize: 50,
  },
  controlIcon2: {
    position: "absolute",
  },
  gridContainer: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  item: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  },
};
