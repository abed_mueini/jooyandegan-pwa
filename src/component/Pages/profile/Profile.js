import { Button, Container, Divider, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { BsChevronLeft } from "react-icons/bs";
import { useRouter } from "next/router";
import TabNavigation from "../../navigation/TabNavigation";
import MyHeader from "../../public/MyHeader";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { COLOR_PRIMARY } from "../../../theme/theme";
import MySnackBar from "../../public/MySnackBar";
import LogoutBottomSheet from "../../bottomSheet/LogoutBottomSheet";
const Profile = () => {
  const userInfo =
    typeof window !== "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : null;
  const dispatch = useDispatch();
  const router = useRouter();
  const state = useSelector((st) => st.filter);
  const [snackBar, setSnackBar] = useState({ open: false, message: "" });
  const [openSheet, setOpenSheet] = useState(false);
  return (
    <Stack>
      {!openSheet && <TabNavigation name="Profile" />}
      <MyHeader title="پروفایل" />
      <Stack padding={2} className={"colJCAC"}>
        <img alt="" src="/images/user.png" style={styles.userIcon} />
        {userInfo && userInfo.fullName !== null && (
          <Typography marginTop={1} varinat="body1">
            {userInfo && userInfo.fullName}
          </Typography>
        )}
        {userInfo === null ? (
          <Button
            onClick={() => router.push("auth")}
            variant="text"
            style={styles.btnLogin}
          >
            <Typography variant="h1">ورود به حساب</Typography>
          </Button>
        ) : (
          <Stack className="rowACJC" style={styles.userPhone}>
            <Typography variant="h1">
              {userInfo && userInfo.phoneNumber}
            </Typography>
          </Stack>
        )}
        <Typography fontSize={12} color="#aaa" marginTop={2}>
          کاربر گرامی شما با این شماره وارد سیستم جویندگان شده اید
        </Typography>
      </Stack>
      <Divider />
      <Button
        onClick={() => router.push({ pathname: "Profile/SetCity" })}
        style={styles.rowACJSB}
      >
        <Stack className="rowACJC">
          <img src="/images/Set the city.png" style={styles.imageItem} />
          <Typography marginRight={1} variant="h1">
            تنظیم شهر
          </Typography>
        </Stack>
        <div className="rowACJC">
          <Typography marginLeft={2} color="#ccc">
            {state.city.label}
          </Typography>
          <BsChevronLeft color="#000" />
        </div>
      </Button>
      <Divider />
      <Button
        onClick={() => {
          if (userInfo === null) {
            setSnackBar({
              open: true,
              message: "ابتدا وارد حساب کاربری خود شوید",
            });
            router.push("auth");
          } else {
            router.push("/Files");
          }
        }}
        style={styles.rowACJSB}
      >
        <div className="rowACJC">
          <img src="/images/my Files.png" style={styles.imageItem} />
          <Typography marginRight={1} variant="h1">
            فایل های من
          </Typography>
        </div>
        <div className="rowACJC">
          <BsChevronLeft color="#000" />
        </div>
      </Button>
      <Divider />
      {userInfo !== null && (
        <>
          <Button
            onClick={() => router.push("Profile/EditProfile")}
            style={styles.rowACJSB}
          >
            <div className="rowACJC">
              <img src="/images/Edit Profile.png" style={styles.imageItem} />
              <Typography marginRight={1} variant="h1">
                ویرایش پروفایل
              </Typography>
            </div>
            <div className="rowACJC">
              <BsChevronLeft color="#000" />
            </div>
          </Button>
          <Divider />
        </>
      )}
      {userInfo !== null && (
        <>
          <Button onClick={() => setOpenSheet(true)} style={styles.rowACJSB}>
            <div className="rowACJC">
              <img src="/images/Logout.png" style={styles.imageItem} />
              <Typography marginRight={1} variant="h1">
                خروج از حساب
              </Typography>
            </div>
            <div className="rowACJC">
              <BsChevronLeft color="#000" />
            </div>
          </Button>
          <Divider />
        </>
      )}
            <Stack style={{ height: 150 }} />

      {snackBar.open && (
        <MySnackBar
          time={1000}
          open={snackBar.open}
          onClose={() => setSnackBar({ open: false, message: "" })}
          message={snackBar.message}
        />
      )}
      <LogoutBottomSheet open={openSheet} onClose={() => setOpenSheet(false)} />
    </Stack>
  );
};
const styles = {
  userInfo: {
    padding: 10,
  },
  userIcon: {
    width: 70,
    height: 70,
    objectFit: "contain",
  },
  btnLogin: {
    width: "80%",
    padding: 9,
    border: "1px solid #2fc391",
    marginTop: 5,
  },
  imageItem: {
    width: 25,
    height: 25,
    objectFit: "contain",
  },
  rowACJSB: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    padding: 20,
  },
  userPhone: {
    width: "80%",
    padding: 9,
    marginTop: 5,
    backgroundColor: "white",
    borderRadius: 10,
    border: `1px solid ${COLOR_PRIMARY}`,
  },
};
export default Profile;
