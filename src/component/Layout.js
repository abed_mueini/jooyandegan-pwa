import React from "react";

import { Container, Stack } from "@mui/material";

export default function Layout({ children }) {
  return (
    <Container className="padding0" sx={styles.container} maxWidth="md">
      {children}
    </Container>
  );
}
const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
  },
};
