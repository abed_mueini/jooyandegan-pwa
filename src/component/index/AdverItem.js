import { Card, Stack, Typography } from "@mui/material";
import React from "react";
import { COLOR_PRIMARY } from "../../theme/theme";
import moment from "moment-jalaali";
import { persianNumber } from "../../utils/Persian";
import { useSelector } from "react-redux";
import router from "next/router";

export default function AdverItem({ item, edit }) {
  const imageBaseUrl = useSelector((st) => st.splash.data?.imageBaseUrl);

  return (
    <Card
      onClick={() => {
        if (edit) {
          router.push("/[id]?edit=true", `/${item.id}?edit=true`);
        } else {
          router.push("/[id]", `/${item.id}`);
        }
      }}
      className="rowACJSB"
      sx={styles.itemContainer}
    >
      <Stack style={{ width: "70%" }} className={"col"}>
        <Stack marginBottom={2.5}>
          <Typography m={1} padding={1} variant={"h1"}>
            {item.title}
          </Typography>
          <Typography marginRight={2} color="#aaa" variant="body1">
            {item.fullCategory}
          </Typography>
        </Stack>
        <Stack sx={styles.rowACJSB} className="rowACJSB">
          <Typography marginRight={1} variant="body1">
            کد فایل :
          </Typography>
          <Typography>{item.id}</Typography>
        </Stack>
        <Stack sx={styles.rowACJSB} className="rowACJSB">
          <Typography color={COLOR_PRIMARY} marginRight={1} variant="body1">
            مژدگانی :
          </Typography>
          <Typography>
            {item.giftAmount === 0
              ? "ندارد"
              : `${persianNumber(item.giftAmount)} تومان`}
          </Typography>
        </Stack>
        <Stack sx={styles.rowACJSB2}>
          <Typography>{getTime(item.issuedDateTime)}</Typography>
        </Stack>
      </Stack>
      <Stack style={{ width: "43%" }}>
        {item.image ? (
          <img src={imageBaseUrl + item.image} style={styles.imageAdver2} />
        ) : (
          <Stack style={styles.noImageContainer}>
            <img src={"/images/noImage2.png"} style={styles.imageAdver} />
          </Stack>
        )}
      </Stack>
    </Card>
  );
  function getTime(time) {
    let now = moment(new Date()); //todays date
    let end = moment(time); // another date
    let hours = now.diff(end, "hours");
    let days = now.diff(end, "days");
    let weeks = now.diff(end, "weeks");
    if (hours < 1) {
      return "دقایقی پیش";
    } else if (hours >= 1 && hours < 24) {
      return `${hours} ساعت پیش`;
    } else if (hours >= 24 && days < 30) {
      return `${days} روز پیش`;
    } else {
      return `${weeks} هفته پیش`;
    }
  }
}
const styles = {
  itemContainer: {
    width: "98%",
    borderRadius: 0,
    boxShadow:'none'
  },
  imageAdver: {
    width: 50,
    height: 50,

    borderRadius: 5,
    objectFit: "contain",
  },
  imageAdver2: {
    width: 120,
    height: 120,
    margin: 15,
    borderRadius: 5,
    objectFit: "cover",
  },
  rowACJSB: { width: "93%", marginTop: 0, marginBottom: 1, marginRight: 1 },
  rowACJSB2: {
    width: "93%",
    marginTop: 0,
    marginBottom: 1,
    marginRight: 1.5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    display: "flex",
  },
  noImageContainer: {
    width: 125,
    height: 125,
    border: "1px solid #eee",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    marginRight: 13,
  },
};
